import { Alert } from 'react-native'

export const showAlert = (title = '', message = '') => {
    setTimeout(() => Alert.alert(
        title,
        message,
        [{ text:'OK' }],
        //{cancelable: false},
    ), 100)
}

export const camelCase = (str) => {
    console.log("string received = ", str)
    let wordsArray = str.split(' ')
    return wordsArray.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
  }