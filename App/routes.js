import React from 'react'
import { createAppContainer, createSwitchNavigator, createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { TouchableOpacity, Image, View } from 'react-native';
import Metrics from './Themes/Metrics.js';
import colors from './Themes/Colors.js';

import Splash from './Components/Splash.js';
import IntroSlider from './Components/IntroSlider.js';
import Register from './Components/Register.js';
import Login from './Components/Login.js';
import ForgetPassword from './Components/ForgetPassword.js';
import PasswordOTP from './Components/PasswordOTP.js';
import ResetPassword from './Components/ResetPassword.js';
import Home from './Components/Home.js';
import Cards from './Components/Cards.js';
import Transfer from './Components/Transfer.js';
import Wallet from './Components/Wallet.js';
import More from './Components/More.js';
import EnterOtp from './Components/EnterOtp.js';
import RegistrationSuccesfull from './Components/RegistrationSuccesfull.js';
import UpdateKYC from './Components/UpdateKYC.js';
import EditProfile from './Components/EditProfile.js';
import AddMoney from './Components/AddMoney.js';
import AddFunds from './Components/AddFunds.js';
import TransactionsList from './Components/TransactionsList.js';
import SendMoney from './Components/SendMoney.js';
import ReviewSend from './Components/ReviewSend.js';
import sendMoneySuccessfull from './Components/sendMoneySuccessfull.js';
import RequestMoney from './Components/RequestMoney.js';
import ReviewRequest from './Components/ReviewRequest.js';
import RequestMoneySuccessfull from './Components/RequestMoneySuccessfull.js';
import ScanQR from './Components/ScanQR.js';
import MobileReload from './Components/MobileReload.js';
import Topup from './Components/Topup.js';
import AddMoneyStripe from './Components/AddMoneyStripe.js';
import TopupPayment from './Components/TopupPayment.js';
import TopupPayStripe from './Components/TopupPayStripe.js';
import SuccessFull from './Components/SuccessFull.js';
import SuccessFullMobile from './Components/SuccessFullMobile.js';
import Support from './Components/Support.js';
import Faqs from './Components/Faqs.js';
import ScanSuccessFull from './Components/ScanSuccessFull.js';
import Settings from './Components/Settings.js';
import BillsPayment from './Components/BillsPayment.js';
import BillsPayInfo from './Components/BillsPayInfo.js';
import TermsNConditions from './Components/TermsNConditions.js';
import Tickets from './Components/Tickets.js';
import CreateTickets from './Components/CreateTickets.js';
import SetPasscode from './Components/SetPasscode.js';
import LockScreen from './Components/LockScreen.js';
import ChangePasscode from './Components/ChangePasscode.js';
import CouponsVouchers from './Components/CouponsVouchers.js';
import TicketDetails from './Components/TicketDetails.js';
import AccountVerification from './Components/AccountVerification.js';
import Points from './Components/Points.js'
import metrics from './Themes/Metrics.js';


const Homestack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
})

const CardsStack = createStackNavigator({
    Cards: {
        screen: Cards,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }

});

const TransferStack = createStackNavigator({
    Transfer: {
        screen: Transfer,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
});


const WalletStack = createStackNavigator({
    Wallet: {
        screen: Wallet,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
});

const MoreStack = createStackNavigator({
    More: {
        screen: More,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
});





const HomeTabNavigator = createBottomTabNavigator({
    Home: {
        screen: Homestack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: () => <Image style={{ marginHorizontal: 10, height: Metrics.getW(20), width: Metrics.getW(20), resizeMode: 'contain', tintColor: 'white' }}
                source={require('./Images/home.png')} />,
            title: "Home",
            tabBarOptions: {
                labelStyle: {
                    fontSize: metrics.text_medium,
                    color: colors.white,
                    // margin: 0,
                    // padding: 0,
                },
                style: {
                    backgroundColor: colors.theme_caribpay,
                }
            }
        })
    },
    Cards: {
        screen: CardsStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: () => <Image style={{ marginHorizontal: 10, height: Metrics.getW(16), width: Metrics.getW(16), resizeMode: 'contain', tintColor: 'white' }}
                source={require('./Images/card.png')} />,
            title: "Cards",
            tabBarOptions: {
                labelStyle: {
                    fontSize: metrics.text_medium,
                    color: colors.white,
                    margin: 0,
                    padding: 0,
                },
                style: {
                    backgroundColor: colors.theme_caribpay,
                }
            }
        })
    },
    Transfer: {
        screen: TransferStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: () => <Image style={{ marginHorizontal: 10, height: Metrics.getW(16), width: Metrics.getW(16), resizeMode: 'contain', tintColor: 'white' }}
                source={require('./Images/plus.png')} />,
            title: "Transfer",
            tabBarOptions: {
                labelStyle: {
                    fontSize: metrics.text_medium,
                    color: colors.white,
                    margin: 0,
                    padding: 0,
                },
                style: {
                    backgroundColor: colors.theme_caribpay,
                }
            }
        })
    },
    Wallet: {
        screen: WalletStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: () => <Image style={{ marginHorizontal: 10, height: Metrics.getW(16), width: Metrics.getW(16), resizeMode: 'contain', tintColor: 'white' }}
                source={require('./Images/wallet.png')} />,
            title: "Wallet",
            tabBarOptions: {
                labelStyle: {
                    fontSize: metrics.text_medium,
                    color: colors.white,
                    margin: 0,
                    padding: 0,
                },
                style: {
                    backgroundColor: colors.theme_caribpay,
                }
            }

        })
    },
    More: {
        screen: MoreStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: () => <Image style={{ marginHorizontal: 10, height: Metrics.getW(16), width: Metrics.getW(16), resizeMode: 'contain', tintColor: 'white' }}
                source={require('./Images/menu.png')} />,
            title: "More",
            tabBarOptions: {
                labelStyle: {
                    fontSize: metrics.text_medium,
                    color: colors.white,
                    margin: 0,
                    padding: 0,
                },
                style: {
                    backgroundColor: colors.theme_caribpay,
                }
            }
        })
    }
}, {
    defaultNavigationOptions: ({ navigation }) => {
        return {
            header: null,
            tabBarOptions: {
                showLabel: true, activeTintColor: colors.theme_caribpay, inactiveTintColor: colors.darkgray,
                style: { height: metrics.dimen_55, backgroundColor: colors.theme_caribpay },
            }
        }
    }
});


const DashBoardStackNavigator = createStackNavigator({
    HomeTabNavigator: HomeTabNavigator
}, {
    defaultNavigationOptions: ({ navigation }) => {
        return {
            header: null
        }
    }
});

// const drawerStackNavigator = createDrawerNavigator({
//     DashBoard: {
//         screen: DashBoardStackNavigator,
//         navigationOptions: ({ navigation }) => ({
//             tabBarLabel: "Roshan",
//             headerStyle: { borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
//         }),
//     },
// }, {
//     initialRouteName: 'DashBoard',
//     contentComponent: NavigationDrawer,
//     drawerWidth: Metrics.width * .73,
//     drawerLockMode: 'locked-closed',
// })

const AuthStack = createStackNavigator({
    Splash: {
        screen: Splash,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    IntroSlider: {
        screen: IntroSlider,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    Register: {
        screen: Register,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    Login: {
        screen: Login,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    ForgetPassword: {
        screen: ForgetPassword,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    PasswordOTP: {
        screen: PasswordOTP,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    ResetPassword: {
        screen: ResetPassword,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    Home: {
        screen: DashBoardStackNavigator,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    EnterOtp: {
        screen: EnterOtp,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    RegistrationSuccesfull: {
        screen: RegistrationSuccesfull,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.white, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            title: "Done",
            headerBackTitle: null,
        })
    },
    UpdateKYC: {
        screen: UpdateKYC,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "KYC Verification",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            ),
        })
    },

    EditProfile: {
        screen: EditProfile,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    AddMoney: {
        screen: AddMoney,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    AddFunds: {
        screen: AddFunds,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    AddMoneyStripe: {
        screen: AddMoneyStripe,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },


    TransactionsList: {
        screen: TransactionsList,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "Transactions",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            )

        })
    },
    SendMoney: {
        screen: SendMoney,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    ReviewSend: {
        screen: ReviewSend,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "Review & Send",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            )

        })
    },

    sendMoneySuccessfull: {
        screen: sendMoneySuccessfull,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },



    RequestMoney: {
        screen: RequestMoney,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    ReviewRequest: {
        screen: ReviewRequest,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "Review & Request",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            )

        })
    },


    RequestMoneySuccessfull: {
        screen: RequestMoneySuccessfull,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },



    ScanQR: {
        screen: ScanQR,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    MobileReload: {
        screen: MobileReload,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    Topup: {
        screen: Topup,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    TopupPayment: {
        screen: TopupPayment,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    TopupPayStripe: {
        screen: TopupPayStripe,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },


    SuccessFull: {
        screen: SuccessFull,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    SuccessFullMobile: {
        screen: SuccessFullMobile,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },


    Support: {
        screen: Support,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },


    Faqs: {
        screen: Faqs,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    ScanSuccessFull: {
        screen: ScanSuccessFull,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },


    Settings: {
        screen: Settings,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    BillsPayment: {
        screen: BillsPayment,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    BillsPayInfo: {
        screen: BillsPayInfo,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    TermsNConditions: {
        screen: TermsNConditions,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },

    Tickets: {
        screen: Tickets,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    CreateTickets: {
        screen: CreateTickets,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    SetPasscode: {
        screen: SetPasscode,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "Enable Passcode",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            )

        })
    },

    LockScreen: {
        screen: LockScreen,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },

    ChangePasscode: {
        screen: ChangePasscode,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },


    CouponsVouchers: {
        screen: CouponsVouchers,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "eCoupons & eVouchers",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            ),
        })
    },

    TicketDetails: {
        screen: TicketDetails,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "View Message",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            ),
        })
    },

    Points: {
        screen: Points,
        navigationOptions: ({ navigationOptions }) => {
            return {
                header: null
            }
        }
    },
    AccountVerification: {
        screen: AccountVerification,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.theme_caribpay, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
            headerTitleStyle: { fontSize: Metrics.text_16, fontFamily: Metrics.quicksand_bold, color: colors.white },
            title: "Account Verification",
            headerBackTitle: null,
            headerLeft: (
                <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
                    style={{ paddingLeft: Metrics.dimen_5 }}
                    onPress={() => navigation.goBack()}>
                    <Image style={{ marginHorizontal: 10, height: Metrics.dimen_24, width: Metrics.dimen_24, resizeMode: 'contain' }}
                        source={require('./Images/leftarrow.png')} />
                </TouchableOpacity>
            ),
        })
    },








    // LetsBargain: {
    //     screen: LetsBargain,
    //     navigationOptions: ({ navigation }) => ({
    //         headerStyle: { backgroundColor: colors.white, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //         headerTitleStyle: {fontSize: Metrics.text_18 , fontFamily : Metrics.roboto_bold , color:colors.heading_black_text},
    //         headerTintColor: colors.black,
    //         title: "Bargain this product",
    //         headerBackTitle: null,
    //         headerLeft: (
    //             <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
    //                 style={{ paddingLeft: Metrics.dimen_5 }}
    //                 onPress={() => navigation.goBack()}>
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/leftarrowblack.png')} />
    //             </TouchableOpacity>
    //         ),
    //         headerRight: (
    //             <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginRight: 5 }}>
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/searchblack.png')} />
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/cart.png')} />
    //             </View>
    //         )
    //     })
    // },
    // ProductDetails: {
    //     screen: ProductDetails,
    //     navigationOptions: ({ navigation }) => {
    //         return {
    //             headerTitle: <AppHeader
    //                 title={navigation.getParam('title')}
    //                 backgroundColor={navigation.getParam('backgroundColor', 'transparent')}
    //                 color={navigation.getParam('color', colors.white)} />,
    //             headerStyle: { backgroundColor: "#9A9B9C", borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //             headerBackTitle: null,
    //             headerLeft: (
    //                 <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
    //                     style={{ paddingLeft: Metrics.dimen_5 }}
    //                     onPress={() => navigation.goBack()}>
    //                     <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                         source={require('./Images/leftarrow.png')} />
    //                 </TouchableOpacity>
    //             ),
    //             headerRight: (
    //                 <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginRight: 5 }}>
    //                     <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                         source={require('./Images/share.png')} />
    //                     <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                         source={require('./Images/msg.png')} />
    //                     <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                         source={require('./Images/cartwhite.png')} />
    //                 </View>
    //             ),
    //         }
    //     }
    // },
    // ShowAll: {
    //     screen: ShowAll,
    //     navigationOptions: ({ navigation }) => {
    //         return {
    //             headerTitle: <AppHeader
    //                 title={navigation.getParam('title')}
    //                 backgroundColor={navigation.getParam('backgroundColor', 'transparent')}
    //                 color={navigation.getParam('color', "#000000")} />,
    //             headerStyle: { backgroundColor: "white", borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //             headerBackTitle: null,
    //             // headerLeft: (
    //             //     <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
    //             //         style={{ paddingLeft: Metrics.dimen_5 }}
    //             //         onPress={() => navigation.goBack()}>
    //             //         <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //             //             source={require('./Images/leftarrowblack.png')} />
    //             //     </TouchableOpacity>
    //             // ),
    //             headerRight: (
    //                 <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginRight: 5 }}>
    //                     <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                         source={require('./Images/fav.png')} />
    //                     <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                         source={require('./Images/cart.png')} />
    //                 </View>
    //             )
    //         }
    //     }
    // },
    // SimmilarProductList : {
    //     screen: SimmilarProductList,
    //     navigationOptions: ({ navigation }) => ({
    //         headerStyle: { backgroundColor: colors.white, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //         headerTitleStyle: { fontSize: Metrics.text_18 , fontFamily : Metrics.roboto_bold , color:colors.heading_black_text},
    //         headerTintColor: colors.black,
    //         title: "Product Detail",
    //         headerBackTitle: null,
    //         headerLeft: (
    //             <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
    //                 style={{ paddingLeft: Metrics.dimen_5 }}
    //                 onPress={() => navigation.goBack()}>
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/leftarrowblack.png')} />
    //             </TouchableOpacity>
    //         ),
    //         headerRight: (
    //             <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginRight: 5 }}>
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/fav.png')} />
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/cart.png')} />
    //             </View>
    //         )
    //     })
    // },
    // ShowAllSimmilar : {
    //     screen: ShowAllSimmilar,
    //     navigationOptions: ({ navigation }) => ({
    //         headerStyle: { backgroundColor: colors.white, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //         headerTitleStyle: { fontSize: Metrics.text_18 , fontFamily : Metrics.roboto_bold , color:colors.heading_black_text},
    //         headerTintColor: colors.black,
    //         title: "More Products",
    //         headerBackTitle: null,
    //         headerLeft: (
    //             <TouchableOpacity style={{ paddingLeft: Metrics.dimen_5 }}
    //                 style={{ paddingLeft: Metrics.dimen_5 }}
    //                 onPress={() => navigation.goBack()}>
    //                 <Image style={{ marginHorizontal: 10, height: Metrics.getW(25), width: Metrics.getW(25), resizeMode: 'contain' }}
    //                     source={require('./Images/leftarrowblack.png')} />
    //             </TouchableOpacity>
    //         ),
    //     })
    // },
    // Favourites : {
    //     screen: Favourites,
    //     navigationOptions: ({ navigation }) => ({
    //         headerStyle: { backgroundColor: colors.white, borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //         headerTitleStyle: { fontSize: Metrics.text_18 , fontFamily : Metrics.roboto_bold , color:colors.heading_black_text},
    //         headerTintColor: colors.black,
    //         title: "Wishlist",
    //         headerBackTitle: null,
    //     })
    // },
    // ProductByCategory: {
    //     screen: ProductByCategory,
    //     navigationOptions: ({ navigation }) => {
    //         return {
    //             headerTitle: <AppHeader
    //                 title={navigation.getParam('title')}
    //                 backgroundColor={navigation.getParam('backgroundColor', 'transparent')}
    //                 color={navigation.getParam('color', "#000000")} />,
    //             headerStyle: { backgroundColor: "white", borderBottomWidth: 0, elevation: 0, shadowOpacity: 0 },
    //             headerBackTitle: null,
    //         }
    //     }
    // },




})


const switchStack = createSwitchNavigator({
    Splash: { screen: Splash /* Payment */ },
    auth: { screen: AuthStack },
}, { initialRouteName: 'Splash' })












const AppContainer = createAppContainer(switchStack);
export default AppContainer
