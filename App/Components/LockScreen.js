import React, { Component } from 'react';
import { SafeAreaView, View, Text, Image, TouchableOpacity, BackHandler, StyleSheet } from 'react-native';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import ProgressDialog from '../ProgressDialog.js';
const axios = require('axios');
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';


export default class SetPasscode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: "",
            userPasscode: '',
            visible: false,
            deviceID: '',
            token: '',
        }
    }

    async componentDidMount() {
        let userToken = await AsyncStorage.getItem('token');
        let deviceID = await AsyncStorage.getItem('DeviceId');
        let passcode = await AsyncStorage.getItem('passcode');
        this.setState({
            deviceID: deviceID,
            token: userToken,
            userPasscode: passcode,
            wrong: false
        })
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        BackHandler.exitApp()
        return true;
    }


    onSetPasscodeClick = () => {
        console.log(this.state.code, this.state.userPasscode)
        if (this.state.code != this.state.userPasscode) {
            this.setState({ wrong: true })
        } else {
            this.props.navigation.navigate("Home")
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ProgressDialog visible={this.state.visible} />
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.handleBackPress()}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Login</Text>
                </View>

                <Text style={{ fontSize: metrics.text_heading, color: colors.heading_black_text, fontWeight: '700', textAlign: 'center', marginTop: 10 }}>Enter your Passcode</Text>
                <Image style={{ height: metrics.dimen_100, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/logo.png')}></Image>
                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, textAlign: 'center', marginTop: 10 }}>Enter your 4 Digit Passcode PIN </Text>
                <View style={{ marginTop: 15, alignSelf: 'center' }}>
                    <SmoothPinCodeInput
                        placeholder=""
                        cellSize={65}
                        cellSpacing={7}
                        keyboardType='phone-pad'
                        cellStyle={{
                            borderWidth: 2,
                            borderRadius: 7,
                            borderColor: 'mediumturquoise',
                            backgroundColor: 'azure',
                        }}
                        cellStyleFocused={{
                            borderColor: 'lightseagreen',
                            backgroundColor: 'lightcyan',
                        }}
                        textStyle={{
                            fontSize: 24,
                            color: colors.black,
                            fontWeight: 'bold'
                        }}
                        textStyleFocused={{
                            color: 'crimson'
                        }}
                        value={this.state.code}
                        onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                        onTextChange={code => this.setState({ code })}
                    />
                </View>



                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.wrong}>
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontWeight: 'bold', fontSize: metrics.text_header, textAlign: 'center' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Passcode Invalid."}</Text>

                                <TouchableOpacity onPress={() => this.setState({ wrong: false })}>
                                    <View style={{ height: metrics.dimen_30, width: 100, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center', padding: 10 }}>{"Try Again"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>



                {/* <View style={{ bottom: 5, position: 'absolute', height: 40, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => this.onSetPasscodeClick()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Login</Text>
                    </TouchableOpacity>
                </View> */}

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_50,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    }
})

