import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Dimensions, BackHandler, TouchableOpacity, Image, FlatList } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import ProgressDialog from '../ProgressDialog.js';
import { Input } from 'react-native-elements';
import RadioForm from 'react-native-simple-radio-button';
const axios = require('axios');
import Modal from 'react-native-modal';
import Toast, { DURATION } from 'react-native-easy-toast';
let Facilities_data = [
    {
        id: 111,
        amount: "20",
    },
    {
        id: 222,
        amount: "50",
    },
    {
        id: 333,
        amount: "100",
    },
    {
        id: 444,
        amount: "150",
    },
    {
        id: 555,
        amount: "200",
    },
    {
        id: 666,
        amount: "300",
    }
]


export default class AddMoney extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            available_Balance: this.props.navigation.state.params.available_Balance,
            token: this.props.navigation.state.params.token,
            amount: this.props.navigation.state.params.amount,
            isVisible: false,
            value: 'Select One',
            paymentMethods: [],
            depositCurrency: [],
            pay_method: '', isPaymentVisible: false,
            amount: '',
            currency_id: 0, fromOnchage: false, maxLimit: '', minLimit: '', limitsAvailable: false,
            symbol: this.props.navigation.state.params.symbol,
            amountsuggestions: [],
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        console.log('token', this.props.navigation.state.params.amount)
        this.getDepositCurrencies();

        axios.get('https://sandbox.caribpayintl.com/api/get-payment-method-list', { headers: { 'Authorization': this.state.token } }).then((response) => {
            console.log("response status ---> ", response.data.success.data)
            if (response.data.status == 200) {
                let respponse = response.data.success.data
                let payment_Methods = [];
                let i;
                for (i = 0; i < respponse.length; i++) {
                    let id = respponse[i].id
                    let name = respponse[i].name
                    var add_methods = { value: name, label: name }
                    payment_Methods.push(add_methods)
                }
                // console.log(payment_Methods)
                this.setState({
                    paymentMethods: payment_Methods
                })
            }
        }).catch((error) => {
            console.log("roshan", error);
        })

        if (this.state.symbol === "USD") {
            this.setState({
                value: "USD"
            })
        } else {
            this.setState({
                value: "XCD"
            })
        }
        let data = Facilities_data.map((item, index) => {
            item.id = ++index
            return { ...item };
        })
        this.setState({
            amountsuggestions: data
        })
    }

    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    getDepositCurrencies = () => {
        axios.get('https://sandbox.caribpayintl.com/api/get-deposit-currency-list', { headers: { 'Authorization': this.state.token } }).then((response) => {
            console.log("response status ---> ", response.data.success.currencies)

            if (response.data.status == 200) {
                let respponse = response.data.success.currencies
                let deposit_crncy = [];
                let i;
                for (i = 0; i < respponse.length; i++) {
                    let id = respponse[i].id
                    let name = respponse[i].code
                    var add_crncy = { value: name, label: name }
                    deposit_crncy.push(add_crncy)
                }
                this.setState({
                    depositCurrency: deposit_crncy
                })
                // console.log("value coming --- ", this.state.depositCurrency)
            }
            // console.log(response.data)
        }).catch((error) => {
            console.log("roshan", error);
        })
    }


    onProceedToAddMoney = () => {
        if (this.state.value == "Select One") {
            this.refs.toast.show('Please Select Currency')
        }
        else if (this.state.pay_method == "") {
            this.refs.toast.show("Please Select Payment Method.");
        }
        else if (this.state.fromOnchage == true && this.state.amount == "") {
            this.refs.toast.show("Please enter amount to add in your wallet");
        }
        // else if (parseFloat(this.state.amount) < 1.00 && this.state.pay_method == "Debit/Credit Card") {
        //     this.refs.toast.show("Minimum amount is 1 for this payment method");
        // }
        else {
            this.props.navigation.navigate('AddFunds', { amount: this.state.fromOnchage ? this.state.amount : this.props.navigation.state.params.amount, token: this.state.token, paymentMethodId: this.state.pay_method == "Paypal" ? 3 : 2, currency_id: this.state.value == "USD" ? 1 : 5 })
        }
    }



    knowLimits = () => {
        let postData = { paymentMethodId: this.state.pay_method == 'Stripe' ? 2 : 3, currencyId: this.state.symbol == "USD" ? 1 : 5 }
        axios.get('https://sandbox.caribpayintl.com/api/get-fees-list-by-payment-method', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            let maxlimit = response.data.success.feesdetails[0].max_limit
            let minlimit = response.data.success.feesdetails[0].min_limit
            let parseFloatMaxlimit;
            let parseFloatMinlimit;
            if (maxlimit != null) {
                parseFloatMaxlimit = parseFloat(maxlimit).toFixed(2)
            } if (minlimit != null) {
                parseFloatMinlimit = parseFloat(minlimit).toFixed(2)
            }
            this.setState({
                maxLimit: maxlimit == null ? 'No Limit' : parseFloatMaxlimit,
                minLimit: minlimit == null ? 'No Limit' : parseFloatMinlimit,
                limitsAvailable: true
            })
        }).catch((error) => {
            console.log(error);
        })
    }


    handleSelection = (item, id) => {
        var selectedid = this.state.selectedID
        if (selectedid === id) {
            this.setState({
                selectedID: null,
                amount: item.amount,
                fromOnchage: true
            })
        } else {
            this.setState({
                selectedID: id,
                amount: item.amount,
                fromOnchage: true
            })
        }
    }




    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ProgressDialog visible={this.state.visible} />
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ margin: metrics.dimen_20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        {/* <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image> */}
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold' }}>Add Money</Text>
                    </View>
                </View>

                <View style={styles.curveview}>
                    <View style={{ flex: 1 }}>
                        <View style={{ margin: metrics.dimen_25, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ color: colors.theme_caribpay, fontWeight: '500', fontSize: metrics.text_normal, marginTop: metrics.dimen_10 }}>{"Available Balance " + this.state.symbol + " " + this.state.available_Balance}</Text>
                            </View>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, borderRadius: metrics.dimen_40 / 2, backgroundColor: "#f2f2f2", justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_22, width: metrics.dimen_22, alignSelf: 'center' }}
                                    source={require('../Images/balance_wallet.png')}></Image>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.setState({ isVisible: true })}>
                            <View style={styles.boxContainer}>
                                <Text style={{ color: colors.black, fontSize: metrics.text_header, padding: 10 }}>Select Currency</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: colors.app_gray, fontSize: metrics.text_description, padding: 10, alignSelf: 'center' }}>{this.state.value}</Text>
                                    <Image style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center', marginRight: 10, tintColor: colors.app_gray }}
                                        source={require('../Images/dropdown.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setState({ isPaymentVisible: true })}>
                            <View style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}>
                                <Text style={{ color: colors.black, fontSize: metrics.text_header, padding: 10 }}>Select Payment Method</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: colors.app_gray, fontSize: metrics.text_description, padding: 10, alignSelf: 'center' }}>{this.state.pay_method}</Text>
                                    <Image style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center', marginRight: 10, tintColor: colors.app_gray }}
                                        source={require('../Images/dropdown.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <Text style={{ fontSize: metrics.text_medium, color: colors.app_light_gray, marginHorizontal: metrics.dimen_20, marginTop: 10 }}>Suggestions</Text>

                        <View style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}>
                            <Text style={{ color: colors.black, fontSize: metrics.text_header, padding: 10 }}>Amount</Text>
                            <View style={{ flex: 1 }}>
                                <Input
                                    placeholder={'$ 10'}
                                    containerStyle={{ alignSelf: 'center' }}
                                    keyboardType="number-pad"
                                    value={this.state.fromOnchage ? this.state.amount : this.props.navigation.state.params.amount}
                                    multiline={false}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: this.state.fromOnchage ? metrics.text_large : metrics.text_normal, textAlign: 'right', color: this.state.fromOnchage ? colors.app_blueColor : null, fontWeight: this.state.fromOnchage ? 'bold' : "200" }}
                                    onChangeText={(text) => this.setState({ amount: text, fromOnchage: true })}>
                                </Input>
                            </View>
                        </View>

                        {this.state.limitsAvailable &&
                            <View style={{ margin: metrics.dimen_25, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: colors.app_gray, fontWeight: '500', fontSize: metrics.text_description }}>{"Min Limit : " + this.state.minLimit}</Text>
                                <Text style={{ color: colors.app_gray, fontWeight: '500', fontSize: metrics.text_description }}>{"Max Limit : " + this.state.maxLimit}</Text>
                            </View>}




                        <View style={{ justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '90%', borderRadius: metrics.dimen_15, marginTop: metrics.dimen_7 }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.amountsuggestions}
                                extraData={this.state.selectedID}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{ backgroundColor: item.id == this.state.selectedID ? colors.theme_caribpay : colors.light_grey_backgroud, margin: metrics.dimen_5, justifyContent: 'center', width: 70, height: metrics.dimen_35, borderRadius: metrics.dimen_20, borderColor: colors.app_gray, borderWidth: 0.5 }}>
                                        <TouchableOpacity onPress={() => this.handleSelection(item, item.id)}>
                                            <Text style={{ fontSize: metrics.text_normal, color: item.id == this.state.selectedID ? "white" : colors.heading_black_text, textAlign: 'center', marginTop: 2, fontWeight: 'bold' }}>{"USD " + item.amount}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View>
                    </View>

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isVisible}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, flexDirection: 'column', height: 230 }}>
                                <Text style={{ fontSize: metrics.text_heading, color: colors.theme_caribpay, fontWeight: 'bold', textAlign: 'center', margin: 10 }}>Select Currency</Text>
                                <View style={{ flexDirection: 'column', margin: 10 }}>
                                    <View style={{ marginLeft: metrics.dimen_30 }}>
                                        <RadioForm
                                            radio_props={this.state.depositCurrency}
                                            initial={this.state.symbol == "USD" ? 0 : 1}
                                            buttonColor={colors.theme_caribpay}
                                            buttonSize={metrics.dimen_20}
                                            onPress={(value) => { this.setState({ value: value }) }} /></View>

                                    <View style={styles.horizontalLine}></View>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                                    <TouchableOpacity onPress={() => this.setState({ isVisible: false, symbol: this.state.value })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.theme_caribpay, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Next</Text>
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.theme_caribpay, borderRadius: metrics.dimen_7, marginLeft: 10, height: metrics.dimen_40, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>

                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isPaymentVisible}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, flexDirection: 'column' }}>
                                <Text style={{ fontSize: metrics.text_heading, color: colors.theme_caribpay, margin: 15, fontWeight: 'bold', textAlign: 'center' }}>Select Payment Method</Text>
                                <View style={{ flexDirection: 'column' }}>
                                    <View style={{ marginTop: 10, marginLeft: metrics.dimen_30 }}>
                                        <RadioForm
                                            radio_props={this.state.paymentMethods}
                                            initial={-1}
                                            buttonColor={colors.theme_caribpay}
                                            buttonSize={metrics.dimen_20}
                                            onPress={(value) => { this.setState({ pay_method: value }, () => this.knowLimits()) }} /></View>
                                    <View style={styles.horizontalLine}></View>
                                </View>

                                <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.setState({ isPaymentVisible: false })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.theme_caribpay, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.setState({ isPaymentVisible: false })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.theme_caribpay, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>











                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.onProceedToAddMoney()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed to Add Money</Text>
                    </TouchableOpacity>
                </View>





            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: '#D3D3D3', fontWeight: '800' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', marginBottom: 10, borderBottomWidth: 1, width: '100%', alignSelf: 'center', marginTop: 20 },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: "#f2f2f2", borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    buttonStyle: {
        flexDirection: 'row',
        width: '80%',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 30,
        height: 40,
        backgroundColor: colors.theme_caribpay,
        shadowColor: "#f2f2f2",
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    imageStyle: {
        height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain', marginTop: 5
    },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop - metrics.dimen_60, width: "100%", backgroundColor: colors.white, borderRadius: 40 },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

})
