import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, Dimensions, FlatList, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { Input } from 'react-native-elements';
import Modal from 'react-native-modal';
var Spinner = require('react-native-spinkit');
import Toast, { DURATION } from 'react-native-easy-toast';
import DocumentPicker from 'react-native-document-picker';
import ImgToBase64 from 'react-native-image-base64';
import ImagePicker from 'react-native-image-crop-picker';
var radio_props = [
    { label: 'Driving License', value: 'Driving License' },
    { label: 'Passport', value: 'Passport' },
    { label: 'National ID', value: 'National ID' },
];
import RadioForm from 'react-native-simple-radio-button';
import base64 from 'react-native-base64'


const axios = require('axios');
const pickeroptions = {
    title: "Select",
    mediaType: 'photo',
    maxWidth: 600,
    maxHeight: 600,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

export default class UpdateKYC extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            uploadPhotoview: true,
            uploadId: false,
            uploadAddress: false,
            isVisible: false,
            selectedDL: false,
            selectedPP: false,
            selectedNID: false,
            selectedIdentity: 'Select One',
            passport_image_uri: '',
            ids_documents_uri: '',
            address_documets_uri: '',
            user_id: this.props.navigation.state.params.user_id,
            fromSignup: this.props.navigation.state.params.fromSignup,
            id_number: '',
            user_token: '',
            photosource: '',
            idsources: '',
            addressSource: '',
            photo_uri: "",
            id_url: "",
            address_url: '',
            isVisibleAlert: false,
            singleFile: '',
            fileName: '',
            fileName2: '',
            fileName3: '',
            invalid: false



        }
    }


    componentDidMount() {
        console.log("user id ---- ", this.props.navigation.state.params.user_id)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack();
        return true;
    }


    async showDocumentPicker() {
        //Opening Document Picker for selection of one file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            console.log('URI : ' + res.uri);
            console.log("response ----> ", res)
            ImgToBase64.getBase64String(res.uri)
                .then(base64String => this.setState({
                    passport_image_uri: base64String,
                    photo_uri: res.uri,
                    fileName: res.name
                }))
                .catch(err => console.log(err));
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                alert('Canceled from single doc picker');
            } else {
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }
    async showDocumentPicker2() {
        //Opening Document Picker for selection of one file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            console.log('URI : ' + res.uri);
            ImgToBase64.getBase64String(res.uri)
                .then(base64String => this.setState({
                    ids_documents_uri: base64String,
                    id_url: res.uri,
                    fileName2: res.name
                }))
                .catch(err => console.log(err));
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // alert('Canceled from single doc picker');
            } else {
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }
    async showDocumentPicker3() {
        //Opening Document Picker for selection of one file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
            });
            console.log('URI : ' + res.uri);
            let base_64 = base64.encode(res.uri)
            this.setState({
                address_documets_uri: base_64,
                fileName3: res.name
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // alert('Canceled from single doc picker');
            } else {
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }


    showImagePickerOpenCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            useFrontCamera: true
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    passport_image_uri: base64String,
                    photo_uri: image.path,
                })).catch(err => console.log(err));
        });
    }

    showImagePickerGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    passport_image_uri: base64String,
                    photo_uri: image.path,
                })).catch(err => console.log(err));
        });
    }


    showImagePicker2OpenCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    ids_documents_uri: base64String,
                    id_url: image.path
                })).catch(err => console.log(err));
        });
    }

    showImagePicker2Gallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    ids_documents_uri: base64String,
                    id_url: image.path
                })).catch(err => console.log(err));
        });
    }



    showImagePicker3OpenCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    address_documets_uri: base64String,
                    address_url: image.path
                })).catch(err => console.log(err));
        });
    }

    showImagePicker3Gallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    address_documets_uri: base64String,
                    address_url: image.path
                })).catch(err => console.log(err));
        });
    }

    onNext = () => {
        if (this.state.passport_image_uri == "") {
            this.setState({
                uploadPhotoview: true
            });
            this.refs.toast.show("please uplaod passport size photo here.")
        } else if (this.state.ids_documents_uri == "") {
            this.setState({
                uploadId: true,
                uploadPhotoview: false,
                uploadAddress: false
            });
            this.refs.toast.show("Please Upload Identity Document")
        }
        else if (this.state.ids_documents_uri != "" && this.state.selectedIdentity == 'Select One') {
            this.setState({
                uploadId: true,
                uploadPhotoview: false,
                uploadAddress: false
            });
            this.refs.toast.show("Please Select Identity Type")
        }
        else if (this.state.ids_documents_uri != "" && this.state.selectedIdentity != 'Select One' && this.state.id_number == "") {
            this.setState({
                uploadId: true,
                uploadPhotoview: false,
                uploadAddress: false
            });
            this.setState({
                invalid: true
            })
        }
        else if (this.state.address_documets_uri == "") {
            this.setState({
                uploadAddress: true,
                uploadId: false,
                uploadPhotoview: false,
            });
            this.refs.toast.show("Please Upload Address Proof")
        }
        else {
            this.setState({
                visible: true
            });
            let postData = {
                user_id: this.state.user_id,
                photo: this.state.passport_image_uri,
                id_proof: this.state.ids_documents_uri,
                address_proof: this.state.address_documets_uri,
            }


            console.log()
            axios({
                url: 'https://sandbox.caribpayintl.com/api/upload-kyc-document-react',
                method: 'POST',
                data: postData,
            }).then((response) => {
                console.log("response upload :", response.data);
                if (response.data.status == 200) {
                    this.setState({
                        isVisibleAlert: true
                    })
                }
            }).catch((error) => {
                console.log("error from image :", error);
                this.setState({
                    visible: false
                });
            })

        }

    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1, flexDirection: 'column', backgroundColor: colors.white }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', margin: metrics.dimen_15 }}>

                    <TouchableOpacity onPress={() =>
                        this.setState({
                            uploadPhotoview: !this.state.uploadPhotoview,
                            uploadId: false, uploadAddress: false
                        })
                    }>
                        <View style={{
                            height: metrics.dimen_40, borderColor: colors.white, borderWidth: 1, justifyContent: 'center'
                            , flexDirection: 'row', borderRadius: metrics.dimen_20, width: metrics.dimen_100, backgroundColor: this.state.uploadPhotoview ? colors.theme_caribpay : colors.lightgrey
                        }}>
                            <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, alignSelf: 'center', resizeMode: 'contain', tintColor: this.state.uploadPhotoview ? colors.white : colors.theme_caribpay, padding: 10 }}
                                source={require('../Images/camera.png')}>
                            </Image>
                            <Text style={{ textAlign: 'center', fontSize: metrics.text_small, color: this.state.uploadPhotoview ? colors.white : colors.black, fontFamily: metrics.quicksand_regular, alignSelf: 'center', padding: 10, fontWeight: 'bold' }}>Photo</Text>
                        </View>
                    </TouchableOpacity>

                    <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, alignSelf: 'center', justifyContent: 'center', tintColor: colors.theme_caribpay }}
                        source={require('../Images/arrow.png')}></Image>

                    <TouchableOpacity onPress={() =>
                        this.setState({ uploadId: this.state.passport_image_uri != "" ? true : false, uploadPhotoview: this.state.passport_image_uri != "" ? false : true, uploadAddress: false })
                    }>
                        <View style={{
                            height: metrics.dimen_40, borderColor: colors.white, borderWidth: 1, justifyContent: 'center', flexDirection: 'row', borderRadius: metrics.dimen_20, width: metrics.dimen_100, backgroundColor: this.state.uploadId ? colors.theme_caribpay : colors.lightgrey
                        }}>
                            <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, alignSelf: 'center', resizeMode: 'contain', tintColor: this.state.uploadId ? colors.white : colors.theme_caribpay, padding: 10 }}
                                source={require('../Images/idcard.png')}>
                            </Image>
                            <Text style={{ textAlign: 'center', fontSize: metrics.text_small, color: this.state.uploadId ? colors.white : colors.black, fontFamily: metrics.quicksand_regular, alignSelf: 'center', padding: 10, fontWeight: 'bold' }}>Identity</Text>
                        </View>
                    </TouchableOpacity>


                    <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, alignSelf: 'center', justifyContent: 'center', tintColor: colors.theme_caribpay }}
                        source={require('../Images/arrow.png')}></Image>

                    <TouchableOpacity onPress={() =>
                        this.setState({ uploadAddress: this.state.passport_image_uri != "" && this.state.ids_documents_uri != "" ? true : false, uploadPhotoview: this.state.passport_image_uri == "" && this.state.ids_documents_uri == "" ? true : false, uploadId: this.state.ids_documents_uri == "" && this.state.passport_image_uri != "" ? true : false })
                    }>
                        <View style={{
                            height: metrics.dimen_40, borderColor: colors.white, borderWidth: 1, justifyContent: 'center', flexDirection: 'row', borderRadius: metrics.dimen_20, width: metrics.dimen_100, backgroundColor: this.state.uploadAddress ? colors.theme_caribpay : colors.lightgrey
                        }}>
                            <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, alignSelf: 'center', resizeMode: 'contain', tintColor: this.state.uploadAddress ? colors.white : colors.theme_caribpay, padding: 10 }}
                                source={require('../Images/address.png')}>
                            </Image>
                            <Text style={{ textAlign: 'center', fontSize: metrics.text_small, color: this.state.uploadAddress ? colors.white : colors.black, fontFamily: metrics.quicksand_regular, alignSelf: 'center', padding: 10, fontWeight: 'bold' }}>Address</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={{ margin: metrics.dimen_15, flex: 1 }}>

                    {this.state.uploadPhotoview &&
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'column', marginLeft: 7 }}>
                                <Text style={styles.headingg}>We need your Selfie</Text>
                                <Text style={styles.descriiption}>Please take a selfie for us to ensure the owner of the submitted ID and the Carib Pay owner are the same.</Text>
                            </View>

                            <View style={{ width: "100%", alignSelf: 'center', borderRadius: 10, marginTop: metrics.dimen_60 }}>
                                <View>
                                    {this.state.passport_image_uri == '' && <Image style={{ height: 150, width: 150, borderRadius: 75, resizeMode: 'contain', alignSelf: 'center' }} source={require("../Images/user.png")}></Image>}
                                    {this.state.passport_image_uri != '' && <Image style={{ height: 150, width: 150, borderRadius: 75, alignSelf: 'center' }} source={{ uri: this.state.photo_uri }}></Image>}
                                </View>

                                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300', marginTop: 7, textAlign: 'center' }}>Make sure it is clear and bright.</Text>

                            </View>

                            <View style={{ flexDirection: 'column', position: 'absolute', bottom: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Image style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center' }} source={require('../Images/shield.png')}></Image>
                                    </View>
                                    <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                                        <Text style={{ fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', }}>Feel Safe</Text>
                                        <Text style={{ ...styles.descriiption, width: 300, alignSelf: 'center', marginTop: 0 }}>Rest assured that your transactions and data are kept secure and confidential</Text>
                                    </View>
                                </View>

                                {this.state.passport_image_uri == "" && <TouchableOpacity onPress={() => this.showImagePickerOpenCamera()}>
                                    <View style={{ backgroundColor: colors.theme_caribpay, width: "90%", height: 40, borderRadius: 5, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Take Photo</Text>
                                    </View>
                                </TouchableOpacity>}

                                {this.state.passport_image_uri != "" && this.state.uploadPhotoview && <TouchableOpacity onPress={() => this.onNext()}>
                                    <View style={{ backgroundColor: colors.theme_caribpay, width: "90%", height: 40, borderRadius: 5, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Next</Text>
                                    </View>
                                </TouchableOpacity>}

                            </View>

                        </View>
                    }

                    {this.state.uploadId &&

                        <View style={{ width: "100%", alignSelf: 'center', borderRadius: 10, marginTop: metrics.dimen_20, backgroundColor: colors.white }}>

                            <TouchableOpacity onPress={() => this.setState({ isVisible: true })}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ color: colors.black, fontSize: metrics.text_header, padding: 10 }}>Identity Type</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginRight: 10 }}>
                                        <Text style={{ color: colors.app_gray, fontSize: metrics.text_description, padding: 5, alignSelf: 'center' }}>{this.state.selectedIdentity}</Text>
                                        <Image source={require('../Images/dropdown.png')} style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center', marginTop: 1 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>

                            <View style={{ borderBottomColor: 'black', borderBottomWidth: 0.5, marginTop: metrics.dimen_10 }} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: colors.black, fontSize: metrics.text_header, padding: 10 }}>Identity Number</Text>
                                <Input
                                    placeholder={'123xx4x.'}
                                    containerStyle={{ width: "70%", height: 30, alignSelf: 'center' }}
                                    keyboardType="number-pad"
                                    value={this.state.id_number}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ id_number: text })}>
                                </Input>
                            </View>

                            <View style={{ borderBottomColor: 'black', borderBottomWidth: 0.5, marginTop: metrics.dimen_10 }} />

                            <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_16, fontWeight: 'bold', padding: 10 }}>Upload ID</Text>
                            <TouchableOpacity onPress={() => this.setState({
                                openModal2: true
                            })}>
                                <View style={{ width: '90%', backgroundColor: colors.app_light_gray, height: metrics.dimen_150, borderRadius: metrics.dimen_10, justifyContent: 'center', margin: metrics.dimen_10, alignSelf: 'center' }}>
                                    {this.state.ids_documents_uri == '' && <Image style={{ height: metrics.dimen_70, width: metrics.dimen_70, alignSelf: 'center', tintColor: colors.white }}
                                        source={require('../Images/camera.png')}></Image>}
                                    {this.state.ids_documents_uri != '' && <Image style={{ height: metrics.dimen_150, width: metrics.dimen_200, alignSelf: 'center', resizeMode: 'center' }}
                                        source={{ uri: this.state.id_url }}>
                                    </Image>}
                                </View>
                            </TouchableOpacity>

                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_16, fontWeight: '600', textAlign: 'center', margin: metrics.dimen_15 }}>OR</Text>
                            <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_16, fontWeight: 'bold', padding: 10 }}>Upload ID</Text>
                            <View style={{ margin: metrics.dimen_10, flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.showDocumentPicker2()}>
                                    <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.dimen_40, width: metrics.dimen_150, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_description, fontWeight: 'bold', textAlign: 'center' }}>Choose File</Text>
                                    </View>
                                </TouchableOpacity>

                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_description, fontWeight: '400', marginLeft: 5, alignSelf: 'center' }}>{this.state.fileName2 == "" ? " No File Chosen " : this.state.fileName2}</Text>
                            </View>


                            <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isVisible}>
                                    <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, flexDirection: 'column', height: 260 }}>
                                        <Text style={{ fontSize: metrics.text_heading, color: colors.theme_caribpay, fontWeight: 'bold', textAlign: 'center', margin: 10 }}>Select Identity Type</Text>
                                        <View style={{ flexDirection: 'column', margin: 10 }}>
                                            <View style={{ marginLeft: metrics.dimen_30 }}>
                                                <RadioForm
                                                    radio_props={radio_props}
                                                    initial={-1}
                                                    buttonSize={metrics.dimen_20}
                                                    onPress={(value) => { this.setState({ selectedIdentity: value }) }} /></View>

                                            <View style={styles.horizontalLine}></View>
                                        </View>

                                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                                            <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                                                </View>
                                            </TouchableOpacity>

                                            <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Modal>
                            </View>


                            <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid}>
                                    <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 220, justifyContent: 'center' }}>
                                        <View style={{ flex: 1 }}>
                                            <View>
                                                <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                                    source={require("../Images/logo.png")}></Image>
                                                <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Please provide the ID number</Text>
                                            </View>
                                            <View style={styles.bottomview2}>
                                                <TouchableOpacity onPress={() => this.setState({ invalid: false })}>
                                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>



                            {/* <View>
                                <Modal isVisible={this.state.isVisible}>
                                    <View style={{ width: "80%", alignSelf: 'center', backgroundColor: 'white' }}>

                                        <View style={{ margin: metrics.dimen_15 }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_large, color: colors.black }}>Identity Type</Text>
                                        </View>

                                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, marginTop: metrics.dimen_15 }} />

                                        <TouchableOpacity onPress={() => this.setState({ selectedDL: true, selectedNID: false, selectedPP: false })}>
                                            <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                                                <View style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: metrics.dimen_20, borderColor: 'black', borderWidth: 1, alignSelf: 'center', backgroundColor: this.state.selectedDL ? colors.carib_pay_blue : colors.transparent }}></View>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.black }}>Driving License</Text>
                                            </View>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={() => this.setState({ selectedPP: true, selectedDL: false, selectedNID: false })}>
                                            <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                                                <View style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: metrics.dimen_20, borderColor: 'black', borderWidth: 1, alignSelf: 'center', backgroundColor: this.state.selectedPP ? colors.carib_pay_blue : colors.transparent }}></View>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.black }}>Passport </Text>
                                            </View>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={() => this.setState({ selectedNID: true, selectedDL: false, selectedPP: false })}>
                                            <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                                                <View style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: metrics.dimen_20, borderColor: 'black', borderWidth: 1, alignSelf: 'center', backgroundColor: this.state.selectedNID ? colors.carib_pay_blue : colors.transparent }}></View>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.black }}>Nation ID</Text>
                                            </View>
                                        </TouchableOpacity>


                                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, marginTop: metrics.dimen_15 }} />

                                        <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                            <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.carib_pay_blue, textAlign: 'left' }}>Cancel</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity onPress={() => this.state.selectedDL ? this.setState({ selectedIdentity: 'Driving License', isVisible: false }) : this.state.selectedPP ? this.setState({ selectedIdentity: 'Passport', isVisible: false }) : this.setState({ selectedIdentity: 'National ID', isVisible: false })}>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.carib_pay_blue, textAlign: 'right' }}>Okay</Text>
                                            </TouchableOpacity>

                                        </View>
                                    </View>

                                </Modal>
                            </View> */}
                        </View>
                    }

                    {this.state.uploadAddress &&

                        <View style={{ width: "100%", alignSelf: 'center', borderRadius: 10, marginTop: metrics.dimen_20, backgroundColor: colors.white }}>
                            <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_16, fontWeight: 'bold', padding: 10 }}>Upload Address Proof</Text>

                            <View style={{ width: '90%', backgroundColor: colors.app_light_gray, height: metrics.dimen_150, borderRadius: metrics.dimen_10, justifyContent: 'center', margin: metrics.dimen_10, alignSelf: 'center' }}>
                                <Image style={{ height: metrics.dimen_70, width: metrics.dimen_70, alignSelf: 'center' }}
                                    source={require('../Images/pdf.png')}></Image>
                            </View>

                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_16, fontWeight: '600', textAlign: 'center', margin: metrics.dimen_15 }}>OR</Text>
                            <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_16, fontWeight: 'bold', padding: 10 }}>Upload Address Proof</Text>
                            <View style={{ margin: metrics.dimen_10, flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.showDocumentPicker3()}>
                                    <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.dimen_40, width: metrics.dimen_150, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_description, fontWeight: 'bold', textAlign: 'center' }}>Choose File</Text>
                                    </View>
                                </TouchableOpacity>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_description, fontWeight: '400', marginLeft: 5, alignSelf: 'center' }}>{this.state.fileName3 == "" ? " No File Chosen " : this.state.fileName3}</Text>
                            </View>
                        </View>
                    }

                </View>

                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'black' }}
                    position='center'
                    positionValue={200}
                    fadeInDuration={200}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }} />




                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.visible}>
                        <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                            <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                        </View>
                    </Modal>
                </View>


                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isVisibleAlert}>
                        <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 350, justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                        source={require("../Images/logo.png")}></Image>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Thank you for uploading KYC verification documents.</Text>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 15, fontWeight: 'bold', textAlign: 'center', width: 300, alignSelf: 'center' }}>Your documents have been submitted. It will take up-to 2 working days to verify your application.</Text>
                                </View>
                                <View style={styles.bottomview}>
                                    <TouchableOpacity onPress={() => this.setState({ isVisibleAlert: false }, () => this.state.fromSignup == false ? this.props.navigation.navigate("Home") : this.props.navigation.navigate("Login"))}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{this.state.fromSignup == false ? "Back to Home" : "Proceed To Login"}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>


                <Modal style={{ width: metrics.dimen_300, alignSelf: 'center' }} isVisible={this.state.openModal}>
                    <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7, height: 150 }}>
                        <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                            <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Photo</Text>
                        </View>

                        <View style={{ margin: metrics.dimen_25, flexDirection: 'row', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={() => this.setState({ openModal: false }, () => setTimeout(() => {
                                this.showImagePickerOpenCamera()
                            }, 500))}>
                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Open Camera</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.setState({ openModal: false }, () => setTimeout(() => {
                                this.showImagePickerGallery()
                            }, 500))}>
                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Select from Gallery</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                <Modal style={{ width: metrics.dimen_300, alignSelf: 'center' }} isVisible={this.state.openModal2}>
                    <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7, height: 150 }}>
                        <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                            <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Photo</Text>
                        </View>

                        <View style={{ margin: metrics.dimen_25, flexDirection: 'row', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={() => this.setState({ openModal2: false }, () => setTimeout(() => {
                                this.showImagePicker2OpenCamera()
                            }, 500))}>
                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Open Camera</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.setState({ openModal2: false }, () => setTimeout(() => {
                                this.showImagePicker2Gallery()
                            }, 500))}>
                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Select from Gallery</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>


                <Modal style={{ width: metrics.dimen_300, alignSelf: 'center' }} isVisible={this.state.openModal3}>
                    <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7, height: 150 }}>
                        <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                            <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Photo</Text>
                        </View>

                        <View style={{ margin: metrics.dimen_25, flexDirection: 'row', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={() => this.setState({ openModal3: false }, () => setTimeout(() => {
                                this.showImagePicker3OpenCamera()
                            }, 500))}>
                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Open Camera</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.setState({ openModal3: false }, () => setTimeout(() => {
                                this.showImagePicker3Gallery()
                            }, 500))}>
                                <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                    <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Select from Gallery</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>







                {this.state.uploadId && <TouchableOpacity onPress={() => this.onNext()}>
                    <View elevation={1} style={styles.buttonStyle}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>NEXT</Text>
                    </View>
                </TouchableOpacity>}
                {this.state.uploadAddress && <TouchableOpacity onPress={() => this.onNext()}>
                    <View elevation={1} style={styles.buttonStyle}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>NEXT</Text>
                    </View>
                </TouchableOpacity>}
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({

    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    buttonStyle: {
        flexDirection: 'row',
        width: '80%',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 30,
        height: 40,
        backgroundColor: colors.theme_caribpay,
        shadowColor: "#f2f2f2",
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    container: { flexDirection: 'row', margin: 10 },
    image_style: { height: metrics.dimen_40, width: metrics.dimen_40, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, fontWeight: "bold" },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300', width: 350, marginTop: 7 },
    bottomview: { bottom: 20, position: 'absolute', height: 50, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    horizontalLine: { borderBottomColor: '#D3D3D3', marginBottom: 10, borderBottomWidth: 1, width: '100%', alignSelf: 'center', marginTop: 20 },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },


})
