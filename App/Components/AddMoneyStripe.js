import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
import Toast, { DURATION } from 'react-native-easy-toast';
import Stripe from 'react-native-stripe-api';
import Modal from 'react-native-modal';
const axios = require('axios');
import { CreditCardInput } from "react-native-credit-card-input";


export default class AddMoneyStripe extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: this.props.navigation.state.params.amount, fromCard: true, fromBanking: false,
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            currency_id: this.props.navigation.state.params.currency_id,
            cvv: '',
            expiryMonth: '',
            expiryYear: '',
            visible: false,
            cardNumber: '',
            stripeKey: '',
            updated_balance: '',
            isVisible: false,
            isVisiblePayment: false,
            card_details: ''
        }
    }

    componentDidMount() {

     

        console.log(this.state.token)

        let postData = { method_id: "2", currency_id: "1" }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/deposit/get-stripe-info',
            data: postData,
            headers: { 'Authorization': this.state.token },
        })
            .then((response) => {
                if (response.data.success.status == 200) {
                    this.setState({
                        stripeKey: response.data.success.stripe_keys.secret_key
                    })
                    console.log(this.state.stripeKey)
                }
            }).catch((err) => {
                // alert(err)
            });

    }

    OnProccede = async () => {

        let month = this.state.card_details.expiry.substr(0, 2)
        let year = this.state.card_details.expiry.substr(3, 5)
        this.setState({ visible: true })
        const apiKey = this.state.stripeKey;
        const client = new Stripe(apiKey);
        const token = await client.createToken({
            number: this.state.card_details.number,
            exp_month: month,
            exp_year: year,
            cvc: this.state.card_details.cvc,
            address_zip: '12345'
        }).then((response) => {
            if (response) {
                console.log("response", response)
                let postData = {
                    amount: this.state.amount,
                    totalAmount: this.state.amount,
                    currency_id: this.state.currency_id,
                    user_id: this.state.user_id,
                    deposit_payment_id: 2,
                    stripeToken: response.id
                }
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/deposit/stripe-payment-store',
                    headers: { 'Authorization': this.state.token },
                    data: postData
                }).then((response) => {
                    console.log("stripe payment store --", response.data)
                    if (response.data.status == 200) {
                        let createdOn = response.data.success.created_at
                        let postData = { user_id: this.state.user_id }
                        axios.get('https://sandbox.caribpayintl.com/api/get-default-wallet-balance', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
                            if (response.data.success.status == 200) {
                                let balance = response.data.success.defaultWalletBalance
                                let available_bal = parseFloat(balance).toFixed(2)
                                this.setState({ updated_balance: available_bal, visible: false })
                            }
                            this.props.navigation.navigate('SuccessFull', { amount: this.state.amount, remarks: "Funds Added via Debit/Credit Card", updated_balance: this.state.updated_balance, createdOn: createdOn, payment_mode: "Debit/Credit Card", user_id: this.state.user_id, token: this.state.token })
                        }).catch((error) => {
                            console.log("error updated bal---", error);
                        })
                    }
                }).catch((err) => {
                    alert(err)
                    this.setState({ visible: false })
                });
            } else {
                alert(response.error.message)
                this.setState({ visible: false })
            }
        }).catch((err) => {
            console.log(err)
            this.setState({ visible: false })
        })

    }

    _onChange = form => {
        let card_details = form.values
        if (card_details.number != "" && card_details.expiry !== "" && card_details.cvc.length == 3) {
            this.setState({
                creditInputDone: true,
                card_details: card_details
            })
        }
        console.log(card_details)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
          
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Add Fund</Text>
                </View>
                <View style={styles.absouluteview}>


                    {/* <LinearGradient colors={['#b4e6f1', '#d7f3f7', '#f9fdff']} style={{ flex: 1 }}> */}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: colors.light_grey_backgroud, height: metrics.dimen_35 }}>
                        <Text style={{ color: colors.black, fontSize: metrics.text_normal, fontWeight: '600', marginLeft: metrics.dimen_10, textAlignVertical: 'center' }}>Add Fund to Wallet</Text>
                        <Text style={{ color: colors.black, fontSize: metrics.text_normal, fontWeight: '600', marginRight: metrics.dimen_10, textAlignVertical: 'center' }}>{this.state.currency_id == 1 ? "$ " + this.state.amount : "EC$ " + this.state.amount}</Text>
                    </View>
                    <View style={{ flex: 1, margin: metrics.dimen_15 }}>
                        <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_35, alignSelf: 'center', width: '100%', margin: 2, borderRadius: metrics.dimen_10 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromCard ? colors.white : colors.theme_caribpay }} onPress={() => this.setState({ fromCard: true, fromBanking: false, isVisibleAlert: false })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 1, backgroundColor: this.state.fromCard ? colors.white : colors.theme_caribpay }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromCard ? colors.black : colors.white, textAlign: 'center' }}>Credit/Debit Card</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 1, backgroundColor: this.state.fromBanking ? colors.white : colors.theme_caribpay }} onPress={() => this.setState({ fromCard: false, fromBanking: true, isVisibleAlert: true })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 1, backgroundColor: this.state.fromBanking ? colors.white : colors.theme_caribpay }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromBanking ? colors.black : colors.white, textAlign: 'center' }}>Online Banking</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{ margin: 20 }}>
                            <CreditCardInput onChange={this._onChange}
                                placeholderColor={colors.app_light_gray} />
                        </View>
                        <Spinner style={{justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />



                        <Toast
                            ref="toast"
                            style={{ backgroundColor: 'black' }}
                            position='center'
                            positionValue={200}
                            fadeInDuration={200}
                            fadeOutDuration={1000}
                            opacity={0.8}
                            textStyle={{ color: 'white' }} />

                    </View>
                </View>
                {this.state.creditInputDone && <View style={{ ...styles.bottomview, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => this.OnProccede()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed</Text>
                    </TouchableOpacity>
                </View>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

})