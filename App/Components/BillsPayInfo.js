import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, FlatList, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { Input } from 'react-native-elements';
import RBSheet from "react-native-raw-bottom-sheet";

let Facilities_data = [
    {
        id: 111,
        name: "Electricity",
    },
    {
        id: 222,
        name: "Water",
    },
    {
        id: 333,
        name: "Internet",
    },
    {
        id: 444,
        name: "Insurance",
    },
    {
        id: 555,
        name: "Cable TV",
    },
    {
        id: 666,
        name: "Telephone",
    }
]

let ElectricityServices = [{
    id: 1,
    image: require('../Images/lucelec.jpg'),
    name: "St. Lucia Electrcity Services Limited, LUCELEC",
}]

let WaterServcies = [{
    id: 1,
    image: require('../Images/wasco.jpg'),
    name: "The Water and Sewage Company Inc, WASCO",
}]

let InternetServices = [{
    id: 1,
    image: require('../Images/digi_cel.png'),
    name: "DIGICEL",
}, {
    id: 2,
    image: require('../Images/flow_in.png'),
    name: "FLOW",
}]

let InsuranceServices = [{
    id: 1,
    image: require('../Images/axcel.jpg'),
    name: "Axcel Insurance",
}, {
    id: 1,
    image: require('../Images/dm.png'),
    name: "Demerara Insurance",
},
{
    id: 1,
    image: require('../Images/massy.png'),
    name: "Massy United Insurance",
},
{
    id: 1,
    image: require('../Images/PAN.png'),
    name: "Pan American Life Insurance",
},
{
    id: 1,
    image: require('../Images/cu.jpg'),
    name: "Co-Operative Credit Union LTD.",
}]

export default class BillsPayInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            billName: this.props.navigation.state.params.billName,
            service: '',
            refrenceNumber: '',
            accountNumber: '',
            amount: '',
            remarks: '',
            amountsuggestions: [],

        }
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let data = Facilities_data.map((item, index) => {
            item.id = ++index
            return { ...item };
        })
        this.setState({
            amountsuggestions: data
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    handleSelection = (item, id) => {
        var selectedid = this.state.selectedID
        if (selectedid === id) {
            this.setState({
                selectedID: null,
                billName: item.name,
            })
        } else {
            this.setState({
                selectedID: id,
                billName: item.name,
            })
        }
    }



    renderServices = () => {
        return (
            <View>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetAlert.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_17, color: 'white', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{"Select " + this.state.billName + " Services"}</Text>
                </View>
                <View style={{ marginTop: metrics.dimen_5, width: "95%", alignSelf: 'center' }}>
                    <Input
                        containerStyle={{ alignSelf: 'center', backgroundColor: '#DCDCDC', borderRadius: 10, height: 45 }}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        placeholder='Search'
                        placeholderTextColor="#9D9D9F"
                        inputStyle={{ color: colors.black, fontSize: metrics.text_description }}
                        value={this.state.text}
                        // onChangeText={text => this.SearchFilterFunction(text)}
                        leftIcon={<Image style={{ width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }} source={require('../Images/search.png')}></Image>} />
                </View>
                {this.state.billName == "Electricity" &&
                    <FlatList
                        style={{ marginTop: metrics.dimen_7 }}
                        showsVerticalScrollIndicator={false}
                        data={ElectricityServices}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <View>
                                <View style={{ backgroundColor: colors.white, margin: metrics.dimen_5, height: metrics.dimen_30, flexDirection: 'row' }}>
                                    <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center', marginHorizontal: 10 }}
                                        source={item.image}></Image>
                                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', textAlignVertical: "center", paddingHorizontal: metrics.dimen_10 }}>{item.name}</Text>
                                </View>
                                <View style={styles.horizontalLine}></View>
                            </View>
                        )} />}
                {this.state.billName == "Water" &&
                    <FlatList
                        style={{ marginTop: metrics.dimen_7 }}
                        showsVerticalScrollIndicator={false}
                        data={WaterServcies}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <View>
                                <View style={{ backgroundColor: colors.white, margin: metrics.dimen_5, height: metrics.dimen_30, flexDirection: 'row' }}>
                                    <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center', marginHorizontal: 10 }}
                                        source={item.image}></Image>
                                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', textAlignVertical: "center", paddingHorizontal: metrics.dimen_10 }}>{item.name}</Text>
                                </View>
                                <View style={styles.horizontalLine}></View>
                            </View>
                        )} />}
                {this.state.billName == "Internet" &&
                    <FlatList
                        style={{ marginTop: metrics.dimen_7 }}
                        showsVerticalScrollIndicator={false}
                        data={InternetServices}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <View>
                                <View style={{ backgroundColor: colors.white, margin: metrics.dimen_5, height: metrics.dimen_30, flexDirection: 'row' }}>
                                    <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center', marginHorizontal: 10 }}
                                        source={item.image}></Image>
                                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', textAlignVertical: "center", paddingHorizontal: metrics.dimen_10 }}>{item.name}</Text>
                                </View>
                                <View style={styles.horizontalLine}></View>
                            </View>
                        )} />}

                {this.state.billName == "Insurance" &&
                    <FlatList
                        style={{ marginTop: metrics.dimen_7 }}
                        showsVerticalScrollIndicator={false}
                        data={InsuranceServices}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <View>
                                <View style={{ backgroundColor: colors.white, margin: metrics.dimen_5, height: metrics.dimen_30, flexDirection: 'row' }}>
                                    <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center', marginHorizontal: 10 }}
                                        source={item.image}></Image>
                                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', textAlignVertical: "center", paddingHorizontal: metrics.dimen_10 }}>{item.name}</Text>
                                </View>
                                <View style={styles.horizontalLine}></View>
                            </View>
                        )} />}
            </View>
        )
    }




    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>{this.state.billName + " - Bill Payment"}</Text>
                </View>

                <View style={styles.absouluteview}>
                    <View style={{ flex: 1 }}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '90%', borderRadius: metrics.dimen_15, marginTop: metrics.dimen_15 }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.amountsuggestions}
                                extraData={this.state.selectedID}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{ backgroundColor: item.id == this.state.selectedID ? colors.theme_caribpay : colors.light_grey_backgroud, margin: metrics.dimen_5, justifyContent: 'center', width: 100, height: metrics.dimen_30, borderRadius: metrics.dimen_7 }}>
                                        <TouchableOpacity onPress={() => this.handleSelection(item, item.id)}>
                                            <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: item.id == this.state.selectedID ? "white" : colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>{item.name}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View>
                        <View style={{ flexDirection: 'column', margin: metrics.dimen_10 }}>
                            {this.state.billName == "Electricity" &&
                                <Input
                                    placeholder={'Select Electricity Service'}
                                    containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7 }}
                                    value={this.state.phone}
                                    leftIcon={() =>
                                        <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                            <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                                source={require('../Images/elec.png')}></Image>
                                        </TouchableOpacity>
                                    }
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ service: text })}>
                                </Input>}
                            {this.state.billName == "Water" &&
                                <Input
                                    placeholder={'Select Water Service'}
                                    containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7 }}
                                    value={this.state.phone}
                                    leftIcon={() =>
                                        <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                            <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                                source={require('../Images/water.png')}></Image>
                                        </TouchableOpacity>
                                    }
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ service: text })}>
                                </Input>}
                            {this.state.billName == "Internet" &&
                                <Input
                                    placeholder={'Select Internet Service'}
                                    containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7 }}
                                    value={this.state.phone}
                                    leftIcon={() =>
                                        <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                            <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                                source={require('../Images/internet.png')}></Image>
                                        </TouchableOpacity>
                                    }
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ service: text })}>
                                </Input>}
                            {this.state.billName == "Insurance" &&
                                <Input
                                    placeholder={'Select Insurance Service'}
                                    containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7 }}
                                    value={this.state.phone}
                                    leftIcon={() =>
                                        <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                            <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                                source={require('../Images/insu.png')}></Image>
                                        </TouchableOpacity>
                                    }
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ service: text })}>
                                </Input>}
                            {this.state.billName == "Cable TV" &&
                                <Input
                                    placeholder={'Select Cable Service'}
                                    containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7 }}
                                    value={this.state.phone}
                                    leftIcon={() => <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                        source={require('../Images/cable.png')}></Image>}
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ service: text })}>
                                </Input>}

                            {this.state.billName == "Telephone" &&
                                <Input
                                    placeholder={'Select Telephone Service'}
                                    containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7 }}
                                    value={this.state.phone}
                                    leftIcon={() => <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                        source={require('../Images/tele.png')}></Image>}
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ service: text })}>
                                </Input>}

                            <Input
                                placeholder={'Refrence No. (Bill#/Invoice#)'}
                                containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_7 }}
                                value={this.state.phone}
                                inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                inputStyle={{ fontSize: 13 }}
                                onChangeText={(text) => this.setState({ refrenceNumber: text })}>
                            </Input>



                            <Input
                                placeholder={'Account No.'}
                                containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_7 }}
                                value={this.state.phone}
                                inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                inputStyle={{ fontSize: 13 }}
                                onChangeText={(text) => this.setState({ accountNumber: text })}>
                            </Input>


                            <Input
                                placeholder={'Amount'}
                                containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_7 }}
                                value={this.state.phone}
                                keyboardType="number-pad"
                                inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                inputStyle={{ fontSize: 13 }}
                                onChangeText={(text) => this.setState({ amount: text })}>
                            </Input>



                            <Input
                                placeholder={'Remarks'}
                                containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_7 }}
                                value={this.state.phone}
                                inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                                inputStyle={{ fontSize: 13 }}
                                onChangeText={(text) => this.setState({ remarks: text })}>
                            </Input>
                        </View>
                    </View>

                    <RBSheet
                        ref={ref => {
                            this.RBSheetAlert = ref;
                        }}
                        height={Dimensions.get('screen').height}
                        duration={0}>
                        {this.renderServices()}
                    </RBSheet>

                </View>
                <View style={{ ...styles.bottomview, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => console.warn('clicked')}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, textAlignVertical: 'center' },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: colors.light_grey_backgroud, borderBottomWidth: 1, width: '90%', alignSelf: 'center', marginTop: 10 },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

})