import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, FlatList, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';

let Facilities_data = [
    {
        id: 1,
        image: require('../Images/elec.png'),
        name: "Electricity",
    },
    {
        id: 2,
        image: require('../Images/water.png'),
        name: "Water",
    },
    {
        id: 3,
        image: require('../Images/internet.png'),
        name: "Internet",
    },
    {
        id: 4,
        image: require('../Images/insu.png'),
        name: "Insurance",
    },
    {
        id: 5,
        image: require('../Images/cable.png'),
        name: "Cable TV",
    },
    {
        id: 6,
        image: require('../Images/tele.png'),
        name: "Telephone",
    }
]

export default class BillsPayment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
     }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Bills Payment</Text>
                </View>

                <View style={styles.absouluteview}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: metrics.dimen_15, marginLeft: metrics.dimen_15 }}>
                        <View style={{ flexDirection: 'column', marginLeft: metrics.dimen_10, justifyContent: 'center' }}>
                            <Text style={{ color: '#323232', fontWeight: '500', fontSize: metrics.text_header, textAlign: 'center' }}>Add New Invoices</Text>
                        </View>

                        <View style={{ height: metrics.dimen_30, width: metrics.dimen_30, borderRadius: metrics.dimen_30 / 2, backgroundColor: "#f2f2f2", justifyContent: 'center', marginRight: metrics.dimen_15 }}>
                            <Image style={{ height: metrics.dimen_15, width: metrics.dimen_15, alignSelf: 'center' }}
                                source={require('../Images/refresh.png')}></Image>
                        </View>
                    </View>



                    <View style={{ justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '90%', borderRadius: metrics.dimen_15, marginTop: metrics.dimen_15 }}>
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={Facilities_data}
                            numColumns={3}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }) => (
                                <View style={{ backgroundColor: colors.light_grey_backgroud, margin: metrics.dimen_5, flex: 1 / 3, justifyContent: 'center', height: metrics.dimen_100, width: metrics.dimen_100, borderRadius: metrics.dimen_7 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate("BillsPayInfo", { billName: item.name })}>
                                        <Image source={item.image} style={{ height: metrics.dimen_40, width: metrics.dimen_40, alignSelf: 'center' }}></Image>
                                        <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>{item.name}</Text>
                                    </TouchableOpacity>
                                </View>
                            )} />
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, textAlignVertical: 'center' },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' }
})
