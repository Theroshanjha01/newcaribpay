import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, BackHandler, FlatList } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
const axios = require('axios');
import Modal from 'react-native-modal';
var Spinner = require('react-native-spinkit');
const moment = require('moment');
import { Input } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';




export default class TicketDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            // token: this.props.navigation.state.params.token,
            AllticketsReplies: [],
            TicketData: [],
            ticket_id: this.props.navigation.state.params.ticket_id,
            user_id: this.props.navigation.state.params.user_id,
            spinvisible: false,
            type_message: ''

        }
    }

    componentDidMount() {
        this.getticketDetail()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }



    getticketDetail = () => {
        this.setState({
            spinvisible: true
        })

        let postData = {
            ticket_id: this.state.ticket_id,
        }

        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/ticket-details',
            headers: { 'Authorization': this.state.token },
            data: postData
        }).then((response) => {
            console.log("ticket replies --> ", response.data.data.ticket_replies)
            this.setState({
                spinvisible: false,
                TicketData: response.data.data.ticket,
                AllticketsReplies: response.data.data.ticket_replies
            })
        }).catch((err) => {
            console.log("ticket add response error   --- > ", err)
        })
    }



    onSendMessage = () => {
        if (this.state.type_message == "") {
            this.refs.toast.show('Please Enter the Message!')
        } else {
            this.setState({
                spinvisible: true
            })
            let postData = {
                user_id: this.state.user_id,
                ticket_id: this.state.ticket_id,
                description: this.state.type_message,
                file: ''
            }
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/send-message',
                headers: { 'Authorization': this.state.token },
                data: postData
            }).then((response) => {
                console.log("send message response  --- >", response.data)
                this.setState({
                    spinvisible: false,
                    type_message:''
                })
                this.getticketDetail()
            }).catch((err) => {
                console.log("ticket add response error   --- > ", err)

            })
        }

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    renderItem = (item) => {
        let txn_date = moment(item.created_at).format('DD MMMM YYYY , hh:mm A');

        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'column', flex: 1, padding: 10 }}>
                    <Text style={styles.headingg}>{txn_date}</Text>
                    <Text style={styles.headingg}>{item.message}</Text>
                </View>
                <View style={{ justifyContent: 'center', padding: 10 }}>
                    <Image style={styles.image_style} source={require('../Images/theater.png')}></Image>
                </View>
            </View>
        )

    }

    render() {
        let txn_date = moment(this.state.TicketData.created_at).format('DD MMMM YYYY , hh:mm A');

        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={styles.container}>
                    <View style={{ justifyContent: 'center', padding: 10 }}>
                        <Image style={styles.image_style} source={require('../Images/theater.png')}></Image>
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, padding: 10 }}>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold' }}>{this.state.TicketData.subject}</Text>
                        <Text style={styles.headingg}>{txn_date}</Text>
                        <Text style={styles.headingg}>{this.state.TicketData.message}</Text>
                    </View>
                </View>

                {
                    this.state.spinvisible == false && this.state.AllticketsReplies.length > 0 &&
                    <View>
                        <FlatList
                            data={this.state.AllticketsReplies}
                            renderItem={({ item, index }) => this.renderItem(item)}
                            keyExtractor={(item, index) => item + index}
                            ListHeaderComponent={() => <View style={{ marginTop: 5, marginLeft: 15 }}>
                                {this.state.AllticketsReplies.length > 0 && <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay }}>{"Reply ( " + this.state.AllticketsReplies.length + " )"}</Text>}
                            </View>}
                        />
                    </View>
                }
                {
                    this.state.spinvisible == false && this.state.AllticketsReplies.length == 0 &&
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <Image style={{ width: 250, height: 150, resizeMode: 'contain', alignSelf: 'center' }}
                            source={require("../Images/comment.png")}></Image>
                        <Text style={{ fontSize: metrics.text_21, color: colors.theme_caribpay, textAlign: 'center', alignSelf: 'center' }}>No Replies!</Text>
                    </View>
                }







                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                        <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                            <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                        </View>
                    </Modal>
                </View>

                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'black' }}
                    position='center'
                    positionValue={200}
                    fadeInDuration={200}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }} />


                <View style={{ position: 'absolute', bottom: 2, flexDirection: 'row', flex: 1 }}>
                    <View style={{ flex: 0.9, marginStart: 1 }}>
                        <Input
                            placeholder={'Type Message'}
                            placeholderTextColor={colors.place_holder_color}
                            value={this.state.type_message}
                            onChangeText={(text) => this.setState({ type_message: text })}
                            containerStyle={{ width: "100%", height: 50, backgroundColor: colors.light_grey_backgroud, borderRadius: 25 }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            inputStyle={{ fontSize: 14 }}>
                        </Input>
                    </View>


                    <View style={{ flex: 0.15, alignSelf: 'center' }}>
                        <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }}
                            source={require('../Images/attach.png')}></Image></View>

                    <View style={{ flex: 0.15, alignSelf: 'center' }}>
                        <TouchableOpacity onPress={() => this.onSendMessage()}>
                            <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require('../Images/send.png')}></Image>
                        </TouchableOpacity>
                    </View>


                </View>

            </SafeAreaView >
        )
    }





}
const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f2f2f2', borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    floatingView: { bottom: metrics.dimen_180, width: metrics.dimen_50, height: metrics.dimen_50, borderRadius: metrics.dimen_50 / 2, backgroundColor: colors.carib_pay_blue, position: 'absolute', right: 20, justifyContent: 'center' },
    container: { flexDirection: 'row', margin: metrics.dimen_10, backgroundColor: colors.light_grey_backgroud, borderRadius: 10 },
    image_style: { height: metrics.dimen_40, width: metrics.dimen_40, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },

})
