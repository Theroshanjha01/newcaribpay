import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity, BackHandler, Dimensions, FlatList } from 'react-native';
import { Input } from 'react-native-elements';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
const axios = require('axios');
import { showAlert } from '../Utils.js';
import RBSheet from "react-native-raw-bottom-sheet";
import CountryData from 'country-data';
import Modal from 'react-native-modal';





export default class ForgetPassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fromMobilNo: true,
            fromEmail: false,
            email: '',
            phone: '',
            visible: false,
            currencycode: '',
            countryDataSource: [],
            newData: [], text: '',
            arrayholder: [],
            isoName: '',
            invalid: false,
            visible:false

        }
    }



    componentDidMount() {
        axios.get('https://topups.reloadly.com/countries').then((response) => {
            this.setState({
                countryDataSource: response.data,
            });
            this.state.arrayholder = response.data
        }).catch((err) => {
            console.warn(err)
        });

        axios.get('https://ipinfo.io/json').then((response) => {
            let country_code = response.data.country;
            let countrycallingcode = JSON.stringify(CountryData.countries[country_code].countryCallingCodes[0]);
            this.setState({
                currencycode: countrycallingcode.replace(/"/g, ""),
                isoName: country_code
            });
        }).catch((error) => {
            console.log(error);
        });


        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    SearchFilterFunction(text) {
        const newData = this.state.arrayholder.filter(item => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ countryDataSource: newData, text: text });
    }



    getCodesByList = (item) => {
        this.setState({
            currencycode: item.callingCodes[0],
            isoName: item.isoName
        })
        this.RBSheetAlert.close();
    }



    renderItemCountries = (item) => {

        return (
            <View>
                <TouchableOpacity onPress={() => this.getCodesByList(item)} >
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                        <Image style={{ height: 30, width: 30, resizeMode: 'contain' }} source={{ uri: 'https://www.countryflags.io/' + item.isoName.toLocaleLowerCase() + '/flat/64.png' }}></Image>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, paddingHorizontal: 10 }}>{item.name}</Text>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_description }}>{" (" + item.callingCodes[0] + ")"}</Text>
                    </View>
                    <View style={{ borderBottomColor: colors.light_grey_backgroud, borderBottomWidth: 1 }} />
                </TouchableOpacity>
            </View>

        )

    }


    onConfirmClick = () => {
        this.setState({ visible: true })
        let data = this.state.fromMobilNo ? this.state.phone : this.state.email
        let postData = { email: data }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/forgot-password',
            data: postData,
        }).then((response) => {
            console.log(response.data)
            if (response.data.status == 200) {
                this.state.fromMobilNo ? this.setState({ phone: '', visible: false }, () => this.props.navigation.navigate("PasswordOTP", { otp: response.data.otp, data: this.state.currencycode + data, phone: data, fieldType: this.state.fromMobilNo ? 'phone' : 'email' }))
                    :
                    this.setState({ email: '', visible: false, emailMsg: true })
            } else if (response.data.status == 201) {
                this.setState({ visible: false })
                this.setState({
                    invalid: true
                })
            } else {
                this.setState({ visible: false })
                showAlert("CaribPay", "Something went wrong")
            }
        }).catch((err) => {
            this.setState({ visible: false })
            alert(err)
        });
    }


    renderCountries = () => {
        return (
            <View>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetAlert.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_16, color: 'white', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Select Country</Text>
                </View>
                <View style={{ marginTop: metrics.dimen_5, width: "95%", alignSelf: 'center' }}>
                    <Input
                        containerStyle={{ alignSelf: 'center', backgroundColor: '#DCDCDC', borderRadius: 10, height: 45 }}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        placeholder='Search'
                        placeholderTextColor="#9D9D9F"
                        inputStyle={{ color: colors.black, fontSize: metrics.text_16, }}
                        value={this.state.text}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        leftIcon={<Image style={{ width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }} source={require('../Images/search.png')}></Image>} />

                </View>

                <FlatList
                    style={{ marginTop: metrics.dimen_7 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.countryDataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => this.renderItemCountries(item)}>
                </FlatList>
            </View>
        )
    }





    render() {
        let name = this.state.isoName
        var inshort = name.toLocaleLowerCase();

        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ margin: metrics.dimen_20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold' }}>Let us Verify You.</Text>
                    </View>
                </View>

                <View style={{ height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop, width: "100%", backgroundColor: colors.white, borderRadius: 40 }}>

                    <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_40, alignSelf: 'center', width: '90%', margin: 2, borderRadius: metrics.dimen_10, marginTop: metrics.dimen_30 }}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, margin: 1, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromMobilNo ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromMobilNo: true, fromEmail: false })}>
                            <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromMobilNo ? colors.light_grey_backgroud : colors.transparent }}>
                                <Text style={{ fontSize: metrics.text_description, color: this.state.fromMobilNo ? colors.black : colors.white, textAlign: 'center', fontWeight: 'bold' }}>Mobile No.</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromEmail ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromEmail: true, fromMobilNo: false })}>
                            <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromEmail ? colors.light_grey_backgroud : colors.transparent }}>
                                <Text style={{ fontSize: metrics.text_description, color: this.state.fromEmail ? colors.black : colors.white, textAlign: 'center', fontWeight: 'bold' }}>Email</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {this.state.fromMobilNo ?
                        <View style={{ flexDirection: 'row', width: '90%', alignSelf: 'center', marginTop: metrics.dimen_10, borderColor: colors.app_gray, borderWidth: 1, borderRadius: metrics.dimen_7 }}>
                            <Input
                                placeholder={'Code'}
                                value={this.state.currencycode}
                                disabled={true}
                                onChangeText={(text) => this.setState({ currencycode: text })}
                                leftIcon={
                                    <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                        {this.state.isoName == "" ?
                                            <Image style={{ height: 20, width: 20 }} source={require('../Images/globe.png')}>
                                            </Image> :
                                            <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://www.countryflags.io/' + inshort + '/flat/64.png' }}>
                                            </Image>
                                        }
                                    </TouchableOpacity>
                                }
                                containerStyle={{ width: "30%", height: metrics.dimen_50 }}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                inputStyle={{ fontSize: 13 }}>
                            </Input>

                            <Input
                                placeholder={'Mobile No.'}
                                containerStyle={{ width: "70%", height: metrics.dimen_50 }}
                                keyboardType='phone-pad'
                                rightIcon={
                                    <Image style={{ height: 20, width: 20 }} source={require('../Images/phone.png')}>
                                    </Image>
                                }
                                value={this.state.phone}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                inputStyle={{ fontSize: 13 }}
                                onChangeText={(text) => this.setState({ phone: text })}>
                            </Input>
                        </View> :
                        <Input
                            placeholder={'Email-Id'}
                            containerStyle={{ width: '90%', height: metrics.dimen_50, borderColor: colors.app_gray, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10, alignSelf: 'center' }}
                            value={this.state.email}
                            keyboardType='email-address'
                            rightIcon={
                                <Image style={{ height: 20, width: 20 }} source={require('../Images/mail.png')}>
                                </Image>
                            }
                            inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                            inputStyle={{ fontSize: 13 }}
                            onChangeText={(text) => this.setState({ email: text })}>
                        </Input>}


                    {this.state.fromMobilNo ? <Text style={{ width: Dimensions.get('screen').width / 2, fontSize: metrics.text_medium, textAlign: 'center', margin: metrics.dimen_30, alignSelf: 'center' }}> 4 Digit OTP will send on your registered mobile number.</Text> : <Text style={{ width: Dimensions.get('screen').width / 2, fontSize: metrics.text_medium, textAlign: 'center', margin: metrics.dimen_30, alignSelf: 'center' }}>A Password reset link will be sent on your email.</Text>}

                    <Spinner style={{ justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />

                    <View style={{ ...styles.bottomview, backgroundColor: colors.theme_caribpay }}>
                        <TouchableOpacity onPress={() => this.onConfirmClick()}>
                            <Text style={{ fontSize: metrics.text_16, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>CONFIRM</Text>
                        </TouchableOpacity>
                    </View>

                    <RBSheet
                        ref={ref => {
                            this.RBSheetAlert = ref;
                        }}
                        height={Dimensions.get('screen').height}
                        duration={0}>
                        {this.renderCountries()}
                    </RBSheet>


                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid} >
                            <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                    <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"INVALID!"}</Text>
                                </View>
                                <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                    {/* <Text style={{ color: 'red', fontSize: metrics.text_medium, textAlign: 'center' }}>{"You have made " + this.state.loginAttempts + " unsuccessfull attempts(s)" + "\n Remaining Attempt(s) : " + (3 - parseFloat(this.state.loginAttempts)) + " "}</Text> */}
                                    <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Email Address/Phone Number does not match!"}</Text>
                                    <TouchableOpacity onPress={() => this.setState({
                                        invalid: false
                                    })}>
                                        <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.emailMsg}>
                            <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                    <Text style={{ color: colors.white, fontWeight: 'bold', fontSize: metrics.text_17, textAlign: 'center' }}>CaribPay</Text>
                                </View>

                                <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>

                                    <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Password reset link has been sent to your email address"}</Text>

                                    <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>

                                        <TouchableOpacity onPress={() => this.setState({ emailMsg: false }, () => this.props.navigation.navigate("Login"))}>
                                            <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Login</Text>
                                            </View>
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View>
                        </Modal>
                    </View>

                </View>


            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    imageStyle: {
        height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain', marginTop: 5
    },
    textStyle: {
        color: colors.app_gray, fontSize: metrics.text_description, marginLeft: 15, marginTop: 15
    },
    bottomview: { bottom: 0, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 }

})
