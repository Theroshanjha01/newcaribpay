import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, FlatList, TouchableOpacity, BackHandler, Dimensions } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { SliderBox } from "react-native-image-slider-box";
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';
const axios = require('axios');
import RadioForm from 'react-native-simple-radio-button';
var Spinner = require('react-native-spinkit');




let Facilities_data = [
  {
    id: 1,
    image: require('../Images/addfund.png'),
    name: "Add Fund",
  },
  {
    id: 2,
    image: require('../Images/topup.png'),
    name: "Mobile Topup",
  },
  {
    id: 3,
    image: require('../Images/bill.png'),
    name: "Pay Bills",
  },
  {
    id: 4,
    image: require('../Images/cards.png'),
    name: "Card",
  },
  {
    id: 5,
    image: require('../Images/remittance.png'),
    name: "Remittance",
  },
  {
    id: 6,
    image: require('../Images/mall.png'),
    name: "Carib Mall",
  }
]

let dataBannerSource = [
  require('../Images/slideshow1.png'),
  require('../Images/slideshow2.png'),
  require('../Images/slideshow3.png'),
  require('../Images/slideshow4.png'),
]

let dataBannerSourcePromotions = [
  require('../Images/p1.jpg'),
  require('../Images/p2.jpg'),
  require('../Images/p3.jpg'),
  require('../Images/p4.jpg'),
]



export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      available_Balance: '',
      currency_symbol: 'nosymbol',
      position: 0,
      // promotion_posn: 0,
      data: '',
      isVisible: false,
      isComingSoon: false,
      totalIncome: '',
      totalOutcome: '',
      enablePasscode: false,
      multipleCurrency: [],
      multipleEnable: false,
      multiWalletData: [],
      selectedCurrency: "",
      facilittyName: '',
      user_picture: '',
      visible: false,
    }
  }

  async componentDidMount() {
    this.setState({
      visible: true
    })
    let selected_Currency = await AsyncStorage.getItem('selectedCurrency')
    let balance = await AsyncStorage.getItem('available_balance')
    let currency = await AsyncStorage.getItem('symbol')
    let fname = await AsyncStorage.getItem('first_name')
    let lname = await AsyncStorage.getItem('last_name');
    let user_id = await AsyncStorage.getItem('id');
    let token = await AsyncStorage.getItem('token');
    let deviceid = await AsyncStorage.getItem('DeviceId')
    let data_wallet = await AsyncStorage.getItem('walletData')

    // let fname = await AsyncStorage.getItem('first_name')
    // let lname = await AsyncStorage.getItem('last_name');
    // let carrierCode = await AsyncStorage.getItem('carrierCode');
    // let email = await AsyncStorage.getItem('email');
    // let formattedPhone = await AsyncStorage.getItem('formattedPhone');
    // let password = await AsyncStorage.getItem('password');
    // let phone = await AsyncStorage.getItem('phone');
    // let type = await AsyncStorage.getItem('type');
    // // let user_id = await AsyncStorage.getItem('id');
    // // let token = await AsyncStorage.getItem('token');


    // console.log("********************info coming***************")
    // console.log("fname", fname)
    // console.log("lname", lname)
    // console.log("carrierCode", carrierCode)
    // console.log("formattedPhone", formattedPhone)
    // console.log("email", email)
    // console.log("password", password)
    // console.log("phone", phone)
    // console.log("type", type)
    // console.log("user_id", user_id)
    // console.log("token", token)

    console.log(data_wallet)

    const user_login_data = {
      token: token,
      fname: this.camelCase(fname), lname: this.camelCase(lname), user_id: user_id,

    }

    this.setState({
      data: user_login_data,
      available_Balance: balance,
      currency_symbol: currency,
      selectedCurrency: selected_Currency == null || undefined ? "" : selected_Currency
    });

    this.KnowAllTxns();
    this.getUserProfilePicture();

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.reloadBalance();
        this.getUserProfilePicture();
      });


    if (deviceid == null || undefined) {
      this.setState({
        enablePasscode: true
      })
    }


    let array = JSON.parse(data_wallet)
    if (array.length > 1) {
      let multiplecurrency = [];
      let i;
      console.log(array.length)
      for (i = 0; i < array.length; i++) {
        let name = array[i].curr_code
        var add_crncy = { value: name, label: name }
        multiplecurrency.push(add_crncy)
      }
      console.log(multiplecurrency)

      this.setState({
        multipleCurrency: multiplecurrency,
        multiWalletData: array,
        radioPosition: array[0].is_default == "Yes" ? 0 : 1
      })

    }

    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);


  }


  getUserProfilePicture = () => {
    console.log("called dp")
    let postData = { user_id: this.state.data.user_id }
    axios.get('https://sandbox.caribpayintl.com/api/get-user-profile', { params: postData, headers: { 'Authorization': this.state.data.token } }).then((response) => {
      console.log("picture ---> ", response.data.success.user.picture)
      this.setState({
        user_picture: response.data.success.user.picture
      })
    }).catch((err) => {
      console.log(err)
    })
  }

  reloadBalance = () => {
    console.log("sv---", this.state.selectedCurrency)
    let postData = { user_id: this.state.data.user_id }
    axios.get('https://sandbox.caribpayintl.com/api/available-balance', { params: postData, headers: { 'Authorization': this.state.data.token } }).then((response) => {
      let data = response.data.wallets
      let filterData
      if (this.state.selectedCurrency == "") {
        filterData = data.filter(roshan => roshan.is_default == "Yes")
      } else {
        filterData = data.filter(roshan => roshan.curr_code == this.state.selectedCurrency)
      }
      // console.log("filtered result on cureency change", filterData)
      let balance = filterData[0].balance
      let currency_symbol = filterData[0].curr_code
      this.setState({
        available_Balance: parseFloat(balance).toFixed(2),
        currency_symbol: currency_symbol
      })
      AsyncStorage.setItem('available_balance', this.state.available_Balance)
      AsyncStorage.setItem('symbol', this.state.currency_symbol)

      console.log("infro coming ===> ", this.state.currency_symbol, this.state.available_Balance)
    });
    this.KnowAllTxns();

  }

  camelCase(str) {
    console.log("string received = ", str)
    let wordsArray = str.split(' ')
    return wordsArray.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === dataBannerSource.length ? 0 : this.state.position + 1,
        });
      }, 3000)
    });
  }


  componentWillUnmount() {
    this.willFocusSubscription.remove()
    clearInterval(this.state.interval);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }



  camelCase(str) {
    console.log("string received = ", str)
    let wordsArray = str.split(' ')
    return wordsArray.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
  }


  knowKycStatus = () => {
    let postData = { user_id: this.state.data.user_id }
    axios({
      method: 'get',
      url: 'https://sandbox.caribpayintl.com/api/get-kyc-status',
      headers: { 'Authorization': this.state.data.token },
      data: postData,
    })
      .then((response) => {
        if (response.data.status == 200) {
          console.log("doctstatus --> ", response.data)
          let kyc_status = response.data.success.status
          if (kyc_status == "Not Uploaded") {
            this.setState({
              isVisible: true
            })
          } else {
            this.setState({
              isVisible: false
            })
          }
        }

      }).catch((err) => {
        alert(err)
      });
  }


  KnowAllTxns = () => {
    console.log('caalled')
    let postData = {
      type: "allTransactions",
      user_id: this.state.data.user_id
    }
    axios.get('https://sandbox.caribpayintl.com/api/activityall', { params: postData, headers: { 'Authorization': this.state.data.token } }).then((response) => {
      let datas = response.data.transactions
      console.log("Data", datas)
      const txnId1 = datas.filter(roshan => roshan.transaction_type_id == 1 && roshan.curr_code == this.state.currency_symbol);
      let sum_total_txnid1 = 0.00;
      if (txnId1.length > 0) {
        let i = 0;
        for (i = 0; i < txnId1.length; i++) {
          sum_total_txnid1 = parseFloat(sum_total_txnid1) + parseFloat(txnId1[i].subtotal)
        }
        console.log("total txn 1", sum_total_txnid1)
      }
      else {
        console.log("total txn 1", sum_total_txnid1)
      }

      const txnId2 = datas.filter(roshan => roshan.transaction_type_id == 4 && roshan.curr_code == this.state.currency_symbol);
      let sum_total_txnid4 = 0.00;
      if (txnId2.length > 0) {
        let i = 0;
        for (i = 0; i < txnId2.length; i++) {
          sum_total_txnid4 = parseFloat(sum_total_txnid4) + parseFloat(txnId2[i].subtotal)
        }
        console.log("total txn 4", sum_total_txnid4)
      } else {
        console.log("total txn 4", sum_total_txnid4)
      }
      const txnId9 = datas.filter(roshan => roshan.transaction_type_id == 9 && roshan.curr_code == this.state.currency_symbol);
      let sum_total_txnid9 = 0.00;
      if (txnId9.length > 0) {
        let i = 0;
        for (i = 0; i < txnId9.length; i++) {
          sum_total_txnid9 = parseFloat(sum_total_txnid9) + parseFloat(txnId9[i].subtotal)
        }
        console.log("total txn 9", sum_total_txnid9)
      } else {
        console.log("total txn 9", sum_total_txnid9)

      }

      const txnId12 = datas.filter(roshan => roshan.transaction_type_id == 12 && roshan.curr_code == this.state.currency_symbol);
      let sum_total_txnid12 = 0.00;
      if (txnId12.length > 0) {
        let i = 0;
        for (i = 0; i < txnId12.length; i++) {
          sum_total_txnid12 = parseFloat(sum_total_txnid12) + parseFloat(txnId12[i].subtotal)
        }
        console.log("total txn 12", sum_total_txnid12)
      } else {
        console.log("total txn 12", sum_total_txnid12)
      }



      let total_income = sum_total_txnid1 + sum_total_txnid4 + sum_total_txnid9 + sum_total_txnid12;

      if (parseFloat(total_income) > 0.00) {
        this.setState({
          totalIncome: this.state.currency_symbol === 'nosymbol' ? '---' : this.state.currency_symbol == 'USD' ? "$ " + parseFloat(total_income).toFixed(2) : "EC$ " + parseFloat(total_income).toFixed(2)
        });
      } else {
        this.setState({
          totalIncome: this.state.currency_symbol === 'nosymbol' ? ' --- ' : this.state.currency_symbol == 'USD' ? "$ 0.00" : "EC$ 0.00"

        });
      }

      //outcome txns:---
      const outcometxnId2 = datas.filter(roshan => roshan.transaction_type_id == 2 && roshan.curr_code == this.state.currency_symbol);
      let sum_outcome_total_txnid2 = 0.00;
      if (outcometxnId2.length > 0) {
        let i = 0;
        for (i = 0; i < outcometxnId2.length; i++) {
          sum_outcome_total_txnid2 = parseFloat(sum_outcome_total_txnid2) + parseFloat(outcometxnId2[i].total)
        }
        console.log("sum_outcome_total_txnid2", sum_outcome_total_txnid2)
      }
      else {
        console.log("sum_outcome_total_txnid2", sum_outcome_total_txnid2)
      }

      const outcometxnId3 = datas.filter(roshan => roshan.transaction_type_id == 3 && roshan.curr_code == this.state.currency_symbol);
      let sum_outcome_total_txnid3 = 0.00;
      if (outcometxnId3.length > 0) {
        let i = 0;
        for (i = 0; i < outcometxnId3.length; i++) {
          sum_outcome_total_txnid3 = parseFloat(sum_outcome_total_txnid3) + parseFloat(outcometxnId3[i].total)
        }
        console.log("sum_outcome_total_txnid3", sum_outcome_total_txnid3)
      } else {
        console.log("sum_outcome_total_txnid3", sum_outcome_total_txnid3)
      }

      const outcometxnId10 = datas.filter(roshan => roshan.transaction_type_id == 10 && roshan.curr_code == this.state.currency_symbol);
      let sum_outcome_total_txnid10 = 0.00;
      if (outcometxnId10.length > 0) {
        let i = 0;
        for (i = 0; i < outcometxnId10.length; i++) {
          sum_outcome_total_txnid10 = parseFloat(sum_outcome_total_txnid10) + parseFloat(outcometxnId10[i].total)
        }
        console.log("sum_outcome_total_txnid10", sum_outcome_total_txnid10)
      } else {
        console.log("sum_outcome_total_txnid10", sum_outcome_total_txnid10)
      }

      const outcometxnId11 = datas.filter(roshan => roshan.transaction_type_id == 11 && roshan.curr_code == this.state.currency_symbol);
      let sum_outcome_total_txnid11 = 0.00;
      if (outcometxnId11.length > 0) {
        let i = 0;
        for (i = 0; i < outcometxnId11.length; i++) {
          sum_outcome_total_txnid11 = parseFloat(sum_outcome_total_txnid11) + parseFloat(outcometxnId11[i].total)
        }
        console.log("sum_outcome_total_txnid11", sum_outcome_total_txnid11)
      } else {
        console.log("sum_outcome_total_txnid11", sum_outcome_total_txnid11)
      }


      const outcometxnId15 = datas.filter(roshan => roshan.transaction_type_id == 15 && roshan.curr_code == this.state.currency_symbol);
      let sum_outcome_total_txnid15 = 0.00;
      if (outcometxnId15.length > 0) {
        let i = 0;
        for (i = 0; i < outcometxnId15.length; i++) {
          sum_outcome_total_txnid15 = parseFloat(sum_outcome_total_txnid15) + parseFloat(outcometxnId15[i].total)
        }
        console.log("sum_outcome_total_txnid11", sum_outcome_total_txnid15)
      } else {
        console.log("sum_outcome_total_txnid11", sum_outcome_total_txnid15)
      }

      var total_outcome = sum_outcome_total_txnid2 + sum_outcome_total_txnid3 + sum_outcome_total_txnid10 + sum_outcome_total_txnid11 + sum_outcome_total_txnid15;
      console.log(total_outcome)
      if (parseFloat(total_outcome) == 0.00) {
        console.log("this comes")
        this.setState({
          totalOutcome: this.state.currency_symbol == 'nosymbol' ? '---' : this.state.currency_symbol == 'USD' ? "$ 0.00" : "EC$ 0.00", visible: false
        })
      } else {
        var newTotaloutcome = total_outcome.toString();
        var c = newTotaloutcome.substr(1)
        var convertedIntofloat = parseFloat(c).toFixed(2)
        this.setState({
          totalOutcome: this.state.currency_symbol == 'nosymbol' ? '---' : this.state.currency_symbol == 'USD' ? "$ " + convertedIntofloat : "EC$ " + convertedIntofloat, visible: false
        })
      }

    }).catch((error) => {
      console.log(error);
    })
  }

  handleBackPress = () => {
    if (this.state.data.token != null || undefined) {
      BackHandler.exitApp();
    } else {
      this.props.navigation.navigate('Login')
      return true;
    }
  }

  onPressItems = (item) => {
    console.log(item.id)
    switch (item.id) {
      case 1:
        this.props.navigation.navigate('AddMoney', { available_Balance: this.state.available_Balance, token: this.state.data.token, amount: '', symbol: this.state.currency_symbol == 'USD' ? "USD" : "XCD" })
        break;
      case 2:
        this.props.navigation.navigate('MobileReload', { user_id: this.state.data.user_id, token: this.state.data.token })
        break;
      case 3:
        this.props.navigation.navigate('BillsPayment')
        break;
      case 4:
        this.setState({ facilittyName: "Card", isComingSoon: true })
        break;
      case 5:
        this.setState({ facilittyName: "Remittance", isComingSoon: true })
        break;
      case 6:
        this.setState({ facilittyName: "Carib Mall", isComingSoon: true })
        break;
      default:
        console.log("this is called")
    }
  }



  onchangeCurrencyType = async () => {
    let data = this.state.multiWalletData
    const filterData = data.filter(roshan => roshan.curr_code == this.state.selectedCurrency)
    console.log("filtered result on cureency change", filterData)
    let currency_symbol = filterData[0].curr_code
    let balance = filterData[0].balance
    let available_bal = parseFloat(balance).toFixed(2)
    this.setState({
      available_Balance: available_bal,
      currency_symbol: currency_symbol
    });
    await AsyncStorage.setItem('available_balance', filterData[0].balance)
    await AsyncStorage.setItem('symbol', this.state.currency_symbol)

    if (this.state.selectedCurrency != "") {
      this.setState({
        radioPosition: filterData[0].is_default == "Yes" ? 1 : 0
      })
    }

    this.KnowAllTxns();

  }


  onLayout = e => {
    this.setState({
      width: e.nativeEvent.layout.width
    });
  };


  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.visible ?
          <Spinner style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} /> :

          <View style={{ flex: 0.38, backgroundColor: colors.theme_caribpay }}>
            <View style={{ flex: 0.5 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_20 }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('EditProfile', { item: this.state.data })}>
                  <View style={{ flexDirection: 'row' }}>

                    {this.state.user_picture == "" && <Image source={require('../Images/user.png')} style={styles.image_user_style}></Image>}
                    {this.state.user_picture != "" && <Image source={{ uri: this.state.user_picture }} style={{ height: 44, width: 44, borderRadius: 22 }}></Image>}
                    <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: metrics.dimen_10 }}>
                      <Text style={{ ...styles.user_text, fontSize: metrics.text_small }}>Welcome Back</Text>
                      {this.state.data.fname != undefined && <Text style={{ ...styles.user_text, color: colors.white }}>{this.state.data.fname + " " + this.state.data.lname}</Text>}
                    </View>
                  </View>
                </TouchableOpacity>

                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignSelf: 'center' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('ScanQR', { user_id: this.state.data.user_id, token: this.state.data.token })}>
                    <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: '#014a57', justifyContent: 'center' }}>
                      <Image source={require('../Images/qrcode.png')} style={styles.scan_bell_style}></Image>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: '#014a57', justifyContent: 'center', marginLeft: metrics.dimen_10 }}>
                      <Image source={require('../Images/notification.png')} style={styles.scan_bell_style}></Image>
                    </View>
                  </TouchableOpacity>
                </View>

              </View>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('AddMoney', { available_Balance: this.state.available_Balance, token: this.state.data.token, symbol: this.state.currency_symbol == 'USD' ? "USD" : "XCD" })}>
                <View style={{ flexDirection: 'row', marginTop: 2, marginLeft: metrics.dimen_25 }}>
                  {this.state.currency_symbol != "nosymbol" &&
                    <View style={{ justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, borderRadius: metrics.dimen_10, flexDirection: 'row', width: 80, borderRadius: 50, height: 35 }}>
                      <TouchableOpacity onPress={() => this.state.multiWalletData.length > 1 ? this.setState({ multipleEnable: true }) : null} style={{ justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, borderRadius: metrics.dimen_50, flexDirection: 'row', width: 80, height: 35 }}>
                        <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', alignSelf: 'center' }}>
                          <Image source={require('../Images/dropdown.png')} style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center', tintColor: colors.theme_caribpay }}></Image>
                        </View>
                        <Text style={{ color: colors.theme_caribpay, fontWeight: 'bold', fontSize: metrics.text_normal, alignSelf: 'center', marginRight: 10 }}>{this.state.currency_symbol}</Text>
                      </TouchableOpacity>
                    </View>}
                  <Text style={styles.available_bal}>{this.state.currency_symbol === "nosymbol" ? "" : this.state.currency_symbol == "USD" ? "$ " + this.state.available_Balance : "EC$ " + this.state.available_Balance}</Text>
                  <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: '#014a57', justifyContent: 'center', marginLeft: metrics.dimen_10, alignSelf: 'center' }}>
                    <Image source={require('../Images/more.png')} style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center' }}></Image>
                  </View>
                </View>
              </TouchableOpacity>

              <View style={{ flexDirection: 'row', marginTop: metrics.dimen_20, justifyContent: 'space-evenly' }}>

                <View style={{ flexDirection: 'row', justifyContent: 'center', width: "30%" }}>
                  <Image source={require('../Images/income.png')} style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center' }}></Image>
                  <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: metrics.dimen_10 }}>
                    <Text style={{ ...styles.user_text, fontSize: metrics.text_medium }}>Received</Text>
                    <Text style={{ ...styles.user_text, color: colors.white }}>{this.state.totalIncome}</Text>
                  </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Image source={require('../Images/outcome.png')} style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center' }}></Image>
                  <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: metrics.dimen_10 }}>
                    <Text style={{ ...styles.user_text, fontSize: metrics.text_medium }}>Spent</Text>
                    <Text style={{ ...styles.user_text, color: colors.white }}>{this.state.totalOutcome}</Text>
                  </View>
                </View>

                <View style={{ marginLeft: metrics.dimen_15, justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, borderRadius: metrics.dimen_40, width: 100 }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('TransactionsList', { user_id: this.state.data.user_id, token: this.state.data.token, available_Balance: this.state.available_Balance, symbol: this.state.currency_symbol })}>
                    <Text style={{ ...styles.user_text, color: colors.theme_caribpay, fontSize: metrics.text_medium, textAlign: 'center', fontFamily: 'bold' }}>{"All Activity"}</Text>
                  </TouchableOpacity>
                </View>

              </View>

              <View style={{
                justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '90%', height: 160, borderRadius: 1, marginTop: metrics.dimen_15,
                borderWidth: 1,
                borderRadius: 7,
                borderColor: '#ddd',
                borderBottomWidth: 1,
                shadowColor: 'black',
                shadowOffset: { width: 1, height: 2 },
                shadowOpacity: 0.9,
                shadowRadius: 3,
                elevation: 5,
              }}>
                <FlatList
                  style={{ marginTop: 5 }}
                  showsVerticalScrollIndicator={false}
                  data={Facilities_data}
                  numColumns={3}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) => (
                    <View style={{
                      backgroundColor: colors.white, margin: 10, flex: 1 / 3
                    }}>
                      <TouchableOpacity onPress={() => this.onPressItems(item)}>
                        <Image source={item.image} style={{
                          height: metrics.dimen_35, width: metrics.dimen_35, alignSelf: 'center', backgroundColor: item.id > 3 ? 'white' : null,
                          opacity: item.id > 3 ? 0.2 : 2
                        }}></Image>
                        <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>{item.name}</Text>
                      </TouchableOpacity>
                    </View>
                  )} />
              </View>


              <View style={{
                width: Dimensions.get('screen').width - 40, marginTop: 10, alignSelf: 'center',
                borderWidth: 1,
                borderRadius: 7,
                borderColor: '#ddd',
                borderBottomWidth: 1,
                shadowColor: 'black',
                shadowOffset: { width: 1, height: 2 },
                shadowOpacity: 0.9,
                shadowRadius: 3,
                elevation: 5,
              }} onLayout={this.onLayout}>
                <SliderBox
                  autoplay={true}
                  circleLoop
                  imageLoadingColor={colors.transparent}
                  images={dataBannerSource}
                  sliderBoxHeight={140}
                  parentWidth={this.state.width}
                  ImageComponentStyle={{ height: 130, borderRadius: 10 }} />
                {/* /> */}
              </View>

              <Text style={{ fontSize: metrics.text_16, color: colors.theme_caribpay, fontWeight: 'bold', marginTop: 7, marginLeft: 20, marginBottom: 7 }}>Offers</Text>
              <View style={{
                width: Dimensions.get('screen').width - 40, alignSelf: 'center',
                borderWidth: 1,
                borderRadius: 7,
                borderColor: '#ddd',
                borderBottomWidth: 1,
                shadowColor: 'black',
                shadowOffset: { width: 1, height: 2 },
                shadowOpacity: 0.9,
                shadowRadius: 3,
                elevation: 5,
              }} onLayout={this.onLayout}>
                <SliderBox
                  autoplay={true}
                  circleLoop
                  imageLoadingColor={colors.transparent}
                  images={dataBannerSourcePromotions}
                  sliderBoxHeight={100}
                  parentWidth={this.state.width}
                  ImageComponentStyle={{ height: 100, borderRadius: 10 }} />
                {/* /> */}
              </View>
              <Text style={{ fontSize: metrics.text_16, color: colors.theme_caribpay, fontWeight: 'bold', marginTop: 12, marginBottom: 10, marginLeft: 20 }}>Discover More</Text>

              <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 7, width: "100%", height: 40 }}>
                <TouchableOpacity style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }} onPress={() => this.setState({ facilittyName: "Petrol Services", isComingSoon: true })}>
                  <View style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }}>
                    <View style={{ height: 40, width: 40, borderRadius: 20, backgroundColor: '#B0E0E6', alignSelf: 'center', justifyContent: 'center' }}>
                      <Image style={{ height: 35, width: 35, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/petrol.png')}></Image>
                    </View>
                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>Petrol</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }} onPress={() => this.setState({ facilittyName: "Facial Services", isComingSoon: true })}>
                  <View style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }}>
                    <View style={{ height: 40, width: 40, borderRadius: 20, backgroundColor: '#B0E0E6', alignSelf: 'center', justifyContent: 'center' }}>
                      <Image style={{ height: 35, width: 35, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/massage.png')}></Image>
                    </View>
                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>Facial</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }} onPress={() => this.setState({ facilittyName: "Hair Cut Services", isComingSoon: true })}>
                  <View style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }}>
                    <View style={{ height: 40, width: 40, borderRadius: 20, backgroundColor: '#B0E0E6', alignSelf: 'center', justifyContent: 'center' }}>
                      <Image style={{ height: 35, width: 35, resizeMode: 'contain', alignSelf: 'center', marginStart: 2 }} source={require('../Images/salon.png')}></Image>
                    </View>
                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>Hair cut</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }} onPress={() => this.setState({ facilittyName: "Cafes Services", isComingSoon: true })}>
                  <View style={{ flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }}>
                    <View style={{ height: 40, width: 40, borderRadius: 20, backgroundColor: '#B0E0E6', alignSelf: 'center', justifyContent: 'center' }}>
                      <Image style={{ height: 35, width: 35, resizeMode: 'contain', alignSelf: 'center', marginStart: 2 }} source={require('../Images/cafe.png')}></Image>
                    </View>
                    <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>Cafes</Text>
                  </View>
                </TouchableOpacity>


              </View>

              <View>
                <Modal backdropTransitionOutTiming={0} backdropTransitionInTiming={0} style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isVisible}>
                  <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                    <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                      <Text style={{ color: colors.white, fontWeight: 'bold', fontSize: metrics.text_17, textAlign: 'center' }}>Confirm!</Text>
                    </View>
                    <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                      <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Please complete your KYC\nprocess"}</Text>
                      <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                          <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>LATER</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isVisible: false }, () => this.props.navigation.navigate('UpdateKYC'))}>
                          <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>PROCEED</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>




              <View>
                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isComingSoon}>
                  <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.red, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 200, justifyContent: 'center' }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                          source={require("../Images/comingsoon.png")}></Image>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center' }}>{this.state.facilittyName}</Text>
                      </View>
                      <View style={styles.bottomview2}>
                        <TouchableOpacity onPress={() => this.setState({
                          isComingSoon: false
                        })}>
                          <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>

              <View>
                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.enablePasscode}>
                  <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 220, justifyContent: 'center' }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                          source={require("../Images/logo.png")}></Image>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 10 }}>Do you want to set passcode?</Text>
                      </View>

                      <View style={styles.bottomview}>
                        <View style={{ width: "45%", backgroundColor: colors.theme_caribpay, borderRadius: 10, justifyContent: 'center' }}>
                          <TouchableOpacity onPress={() => this.setState({ enablePasscode: false }, () => this.props.navigation.navigate('SetPasscode', { user_id: this.state.data.user_id, token: this.state.data.token }))}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Set</Text>
                          </TouchableOpacity>
                        </View>

                        <View style={{ width: "45%", marginLeft: 10, backgroundColor: colors.theme_caribpay, borderRadius: 10, justifyContent: 'center' }}>
                          <TouchableOpacity onPress={() => this.setState({ enablePasscode: false },()=>AsyncStorage.setItem('DeviceId',"1"))}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Later</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>

              <Modal style={{ width: metrics.dimen_300, alignSelf: 'center' }} isVisible={this.state.multipleEnable}>
                <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7 }}>
                  <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                    <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Currency</Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ margin: 20, marginLeft: metrics.dimen_30 }}>
                      <RadioForm
                        radio_props={this.state.multipleCurrency}
                        initial={this.state.radioPosition}
                        buttonColor={colors.carib_pay_blue}
                        buttonSize={metrics.dimen_20}
                        onPress={(value) => { this.setState({ selectedCurrency: value }) }} /></View>
                    <View style={styles.horizontalLine}></View>
                  </View>

                  <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.setState({ multipleEnable: false })}>
                      <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                        <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.setState({ multipleEnable: false }, () => this.onchangeCurrencyType())}>
                      <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                        <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>

            </View>
          </View>}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whitesmoke
  },
  image_user_style: {
    height: metrics.dimen_40,
    width: metrics.dimen_40,
    resizeMode: 'contain'
  },
  user_text: { fontFamily: metrics.quicksand_regular, fontSize: metrics.text_normal, color: colors.white },
  scan_bell_style: {
    height: metrics.dimen_20,
    width: metrics.dimen_20,
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  available_bal: {
    fontSize: metrics.text_large,
    color: colors.white,
    fontFamily: metrics.quicksand_semibold,
    paddingLeft: metrics.dimen_10,
    textAlignVertical: 'center'
  },
  horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.5, width: '90%', alignSelf: 'center', marginTop: 20 },
  bottomview: { bottom: 20, position: 'absolute', color: colors.theme_caribpay, height: 40, justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10, flexDirection: 'row' },
  bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }


})
