import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, BackHandler, TouchableOpacity, Image } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
// import ProgressDialog from '../ProgressDialog.js';
var Spinner = require('react-native-spinkit');

import { Input, CheckBox } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';
const axios = require('axios');
import RNPaypal from 'react-native-paypal-lib';
// let PayPal = require('react-native-paypal');/
import Modal from 'react-native-modal';


export default class AddFunds extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: this.props.navigation.state.params.amount,
            paymentMethodId: this.props.navigation.state.params.paymentMethodId,
            currency_id: this.props.navigation.state.params.currency_id,
            token: this.props.navigation.state.params.token,
            txn_fee: '...',
            total_amount_pay: '...',
            visible: true,
            checked: false,
            isVisibleAlert: false,
            note: ''
        }
    }

    componentDidMount() {
        console.log("direct parset flot -- >", this.state.currency_id)
        this.setState({
            amount: parseFloat(this.state.amount).toFixed(2)
        });

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let postData = { paymentMethodId: this.state.paymentMethodId, currencyId: this.state.currency_id }
        axios.get('https://sandbox.caribpayintl.com/api/get-fees-list-by-payment-method', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            let fees_charge_in_percentage = response.data.success.feesdetails[0].charge_percentage
            let feees = fees_charge_in_percentage / 100 * this.state.amount
            // let total_amount =
            console.log(this.state.amount, feees)
            this.setState({
                txn_fee: parseFloat(feees).toFixed(2),
                total_amount_pay: parseFloat(this.state.amount) + parseFloat(feees),
                visible: false
            })
        }).catch((error) => {
            console.log(error);
        })

        this.getPayPalInfo();
    }




    getPayPalInfo = () => {
        console.log("getPaypalinfo", this.state.token)
        let postData = { method_id: 3, currency_id: this.state.currency_id }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/deposit/get-paypal-info',
            data: postData,
            headers: { 'Authorization': this.state.token },
        })
            .then((response) => {
                // console.log(response.data)
                // if (response.data.success.status == 200) {
                console.log("paypalInfo ---> ", response.data)
                //     AsyncStorage.setItem("clientIdPaypal", response.data.success.method_info.client_id);
                //     AsyncStorage.setItem("clientSecretIdPaypal", response.data.success.method_info.client_secret);
                // }
            }).catch((err) => {
                alert("roshan")
            });
    }

    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }



    onPayPalBtnClick = async () => {

        if (this.state.paymentMethodId == 3) {
            alert("Under Construction!")
            // let client_id = await AsyncStorage.getItem('clientIdPaypal');
            // let user_id = await AsyncStorage.getItem('id');
            // this.setState({
            //     visible: true
            // })
            // RNPaypal.paymentRequest({
            //     clientId: client_id,
            //     environment: RNPaypal.ENVIRONMENT.PRODUCTION,
            //     intent: RNPaypal.INTENT.SALE,
            //     price: this.state.total_amount_pay,
            //     currency: 'USD',
            //     description: this.state.note == '' ? "Money Deposit" : this.state.note,
            //     acceptCreditCards: true
            // }).then(response => {
            //     if (response.response.state == "approved") {
            //         console.log("yes it is approved")
            //         let postData = {
            //             amount: this.state.amount,
            //             currencyID: 1,
            //             userId: user_id,
            //             methodID: 3,
            //             details: {
            //                 status: "COMPLETED"
            //             }
            //         }
            //         axios({
            //             method: 'post',
            //             url: 'https://sandbox.caribpayintl.com/api/deposit/paypal-payment-store',
            //             data: postData,
            //             headers: { 'Authorization': this.state.token },
            //         }).then((response) => {
            //             if (response.data.success.status == 200) {
            //                 let createdOn = response.data.success.transaction.created_at
            //                 let postData = { user_id: user_id }
            //                 axios.get('https://sandbox.caribpayintl.com/api/get-default-wallet-balance', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            //                     if (response.data.success.status == 200) {
            //                         let balance = response.data.success.defaultWalletBalance
            //                         let available_bal = parseFloat(balance).toFixed(2)
            //                         this.props.navigation.navigate('SuccessFull', { amount: this.state.amount, remarks: this.state.note, updated_balance: available_bal, createdOn: createdOn, payment_mode: "PayPal", user_id: user_id, token: this.state.token })
            //                         this.setState({ visible: false })
            //                     }
            //                 }).catch((error) => {
            //                     console.log("error updated bal---", error);
            //                 })
            //             }
            //         }).catch((err) => {
            //             console.log("error in paypal payment store ", err)
            //         })
            //     } else {
            //         this.setState({ visible: false })
            //         alert('Payment Request Failed!')
            //     }
            // }).catch((error) => {
            //     this.setState({ visible: false })
            //     console.log("error in paymentrequest", error)
            // })

        } else {
            let user_id = await AsyncStorage.getItem('id');
            this.props.navigation.navigate('AddMoneyStripe', { amount: this.state.amount, token: this.state.token, user_id: user_id, fromTopup: false, currency_id: this.state.currency_id })
        }

    }


    // onPayPalBtnClick = () => {
    //     console.log("cdwcdsjkcbjdsk")
    // }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ margin: metrics.dimen_20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        {/* <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image> */}
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold' }}>Add Funds</Text>
                    </View>
                </View>


                <View style={styles.curveview}>
                    <View style={{ flex: 1 }}>



                        <View style={{ width: '90%', alignSelf: 'center', height: this.state.paymentMethodId == 3 ? metrics.dimen_170 : metrics.dimen_100, borderRadius: metrics.dimen_8, marginTop: metrics.dimen_40, borderColor: colors.carib_light_grey, borderWidth: 0.4 }}>
                            {this.state.paymentMethodId == 3 && <Text style={{ fontSize: metrics.text_16, color: colors.theme_caribpay, textAlign: 'center', marginTop: metrics.dimen_35 }}>You are about to Deposit money via</Text>}
                            {this.state.paymentMethodId == 2 && <Text style={{ fontSize: metrics.text_16, color: colors.theme_caribpay, textAlign: 'center', marginTop: metrics.dimen_35 }}>
                                {"You are about to Deposit money via \n Debit/Credit Card"}</Text>}
                            {this.state.paymentMethodId == 3 && <Image style={{ height: metrics.dimen_80, width: metrics.dimen_160, alignSelf: 'center', resizeMode: 'contain' }}
                                source={require('../Images/paypal.png')}></Image>}
                            {this.state.paymentMethodId == 3 && <View style={{ width: "80%", height: 50, alignSelf: 'center', marginBottom: 10 }}>
                                <View style={{ margin: metrics.dimen_7, alignSelf: 'center', flexDirection: 'row' }}>
                                    <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/addnote.png')}></Image>
                                    <Input
                                        placeholder={'Add Note'}
                                        containerStyle={{ alignSelf: 'center', height: 30 }}
                                        value={this.state.note}
                                        multiline={false}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.text_description }}
                                        onChangeText={(text) => this.setState({ note: text })}>
                                    </Input>
                                </View>
                            </View>}
                            <View style={{ height: 40, width: metrics.dimen_150, backgroundColor: colors.theme_caribpay, position: 'absolute', top: -20, alignSelf: 'center', justifyContent: 'center', borderRadius: metrics.dimen_8 }}>
                                <Text style={{ fontSize: metrics.text_16, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{this.state.currency_id == 1 ? "$ " + this.state.amount : "EC$ " + this.state.amount}</Text>
                            </View>

                        </View>

                        <View style={{ flexDirection: 'column', margin: 5 }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                <Text style={{ fontSize: metrics.text_header, color: colors.app_gray }}>Deposit Amount</Text>
                                <Text style={{ fontSize: metrics.text_header, color: colors.black, textAlign: 'center' }}>{this.state.currency_id == 1 ? "$ " + this.state.amount : "EC$ " + this.state.amount}</Text>
                            </View>
                            <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                <Text style={{ fontSize: metrics.text_header, color: colors.app_gray }}>Fee</Text>
                                <Text style={{ fontSize: metrics.text_header, color: colors.black, textAlign: 'center' }}>{this.state.currency_id == 1 ? "$ " + this.state.txn_fee : "EC$ " + this.state.txn_fee}</Text>
                            </View>
                            <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>


                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                <Text style={{ fontSize: metrics.text_header, color: colors.app_gray }}>Total</Text>
                                {this.state.total_amount_pay != NaN && <Text style={{ fontSize: metrics.text_header, color: colors.carib_pay_blue, textAlign: 'center' }}>{this.state.currency_id == 1 ? "$ " +  parseFloat(this.state.total_amount_pay).toFixed(2) : "EC$ " + parseFloat(this.state.total_amount_pay).toFixed(2)}</Text>}
                            </View>
                            <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                        </View>


                        <Spinner style={{ justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />


                        <Toast
                            ref="toast"
                            style={{ backgroundColor: 'black' }}
                            position='center'
                            positionValue={200}
                            fadeInDuration={200}
                            fadeOutDuration={1000}
                            opacity={0.8}
                            textStyle={{ color: 'white' }} />

                        <Modal style={{ alignSelf: 'center', width: '80%' }} isVisible={this.state.isVisibleAlert}>
                            <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7 }}>
                                <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.theme_caribpay, justifyContent: 'center' }}>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>CaribPay</Text>
                                </View>
                                <View style={{ flexDirection: 'column', margin: metrics.dimen_10 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, textAlign: 'center' }}>Please agree to our terms and condition.</Text>
                                </View>

                                <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.setState({ isVisibleAlert: false })}>
                                        <View style={{ backgroundColor: colors.theme_caribpay, borderRadius: metrics.dimen_7, height: metrics.dimen_40, width: 100, marginLeft: 10, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </Modal>
                    </View>
                </View>


                <View style={{ ...styles.bottomview, backgroundColor: colors.transparent, bottom: 50 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <CheckBox
                            checkedIcon={<Image source={require('../Images/tick.png')}
                                style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: 3 }}></Image>}
                            uncheckedIcon={<Image source={require('../Images/unchecked.png')}
                                style={{ height: metrics.dimen_20, width: metrics.dimen_20 }}></Image>}
                            containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, marginLeft: -10 }}
                            checked={this.state.checked}
                            onPress={() => this.setState({ checked: !this.state.checked })}
                        />
                        <Text style={{
                            color: colors.heading_black_text,
                            marginLeft: -20, fontSize: metrics.text_description,
                        }}
                            onPress={() => { console.warn("clicked") }}>I Agree to Terms {'\u0026'} Conditions</Text>
                    </View>
                </View>

                {this.state.paymentMethodId == 3 ?
                    <View style={styles.bottomview}>
                        <TouchableOpacity onPress={() => this.state.checked ? this.onPayPalBtnClick() : this.setState({ isVisibleAlert: true })}>
                            <Image style={{ height: metrics.dimen_30, width: metrics.dimen_120, alignSelf: 'center' }}
                                source={require('../Images/paypal1.png')}></Image>
                        </TouchableOpacity>
                    </View> :
                    <View style={{ ...styles.bottomview, backgroundColor: colors.theme_caribpay }}>
                        <TouchableOpacity onPress={() => this.state.checked ? this.onPayPalBtnClick() : this.setState({ isVisibleAlert: true })}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed</Text>
                        </TouchableOpacity>
                    </View>}

            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop - metrics.dimen_60, width: "100%", backgroundColor: colors.white, borderRadius: 40 },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 }
})