import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, TouchableOpacity, Image, Dimensions, BackHandler, FlatList } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { Input } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import RadioForm from 'react-native-simple-radio-button';
const axios = require('axios');
import Modal from 'react-native-modal';
import Toast, { DURATION } from 'react-native-easy-toast';
var radio_props = [
    { label: 'USD', value: 'USD' },
    { label: 'XCD', value: 'XCD' },

];
let Facilities_data = [
    {
        id: 111,
        amount: "20",
    },
    {
        id: 222,
        amount: "50",
    },
    {
        id: 333,
        amount: "100",
    },
    {
        id: 444,
        amount: "150",
    },
    {
        id: 555,
        amount: "200",
    },
    {
        id: 666,
        amount: "300",
    }
]

import RBSheet from "react-native-raw-bottom-sheet";
import { SvgUri } from 'react-native-svg';
import CountryData from 'country-data';

// let Facilities_data = [
//     {
//         id: 1,
//         amount: "20",
//     },
//     {
//         id: 2,
//         amount: "50",
//     },
//     {
//         id: 3,
//         amount: "100",
//     },
//     {
//         id: 4,
//         amount: "150",
//     },
//     {
//         id: 5,
//         amount: "200",
//     },
//     {
//         id: 6,
//         amount: "300",
//     }
// ]



export default class SendMoney extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: this.props.navigation.state.params.email,
            phone: '',
            fromMobilNo: this.props.navigation.state.params.fromMobilNo,
            fromEmail: this.props.navigation.state.params.fromEmail,
            available_Balance: '',
            value: 'Select One', amount: '', user_id: '',
            token: '',
            symbol: '',
            countryDataSource: [],
            currencycode: '',
            isoName: '',
            arrayholder: [],
            invalid: false,
            amountsuggestions: [],
        }
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let balance = await AsyncStorage.getItem('available_balance')
        let symbol = await AsyncStorage.getItem('symbol')
        console.log(symbol)
        let user_id = await AsyncStorage.getItem('id')
        let token_value = await AsyncStorage.getItem('token')
        this.setState({
            available_Balance: balance,
            user_id: user_id,
            token: token_value,
            symbol: symbol
        })

        axios.get('https://topups.reloadly.com/countries').then((response) => {
            this.setState({
                countryDataSource: response.data,
            });
            this.state.arrayholder = response.data
        }).catch((err) => {
            console.warn(err)
        });

        axios.get('https://ipinfo.io/json').then((response) => {
            let country_code = response.data.country;
            let countrycallingcode = JSON.stringify(CountryData.countries[country_code].countryCallingCodes[0]);
            this.setState({
                currencycode: countrycallingcode.replace(/"/g, ""),
                isoName: country_code
            });
        }).catch((error) => {
            console.log(error);
        });

        if (this.state.symbol == "USD") {
            this.setState({ value: 'USD' })
        } else {
            this.setState({ value: 'XCD' })
        }


        let data = Facilities_data.map((item, index) => {
            item.id = ++index
            return { ...item };
        })
        this.setState({
            amountsuggestions: data
        })


    }


    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    getCodesByList = (item) => {
        this.setState({
            currencycode: item.callingCodes[0],
            isoName: item.isoName
        })
        this.RBSheetAlert.close();
    }

    renderItemCountries = (item) => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.getCodesByList(item)} >
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                        <SvgUri
                            width="10%"
                            height="100%"
                            uri={item.flag}
                        />
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, paddingHorizontal: 10 }}>{item.name}</Text>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_description }}>{" (" + item.callingCodes[0] + ")"}</Text>
                    </View>
                    <View style={{ borderBottomColor: colors.light_grey_backgroud, borderBottomWidth: 1 }} />
                </TouchableOpacity>
            </View>
        )
    }

    handleSelection = (item, id) => {
        var selectedid = this.state.selectedID
        if (selectedid === id) {
            this.setState({
                selectedID: null,
                amount: item.amount,
                fromSelections: true
            })
        } else {
            this.setState({
                selectedID: id,
                amount: item.amount,
                fromSelections: true
            })
        }
    }

    SearchFilterFunction(text) {
        const newData = this.state.arrayholder.filter(item => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ countryDataSource: newData, text: text });
    }

    renderCountries = () => {
        return (
            <View>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetAlert.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_16, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Select Country</Text>
                </View>
                <View style={{ marginTop: metrics.dimen_5, width: "95%", alignSelf: 'center' }}>
                    <Input
                        containerStyle={{ alignSelf: 'center', backgroundColor: '#DCDCDC', borderRadius: 10, height: 45 }}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        placeholder='Search'
                        placeholderTextColor="#9D9D9F"
                        inputStyle={{ color: colors.black, fontSize: metrics.text_description }}
                        value={this.state.text}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        leftIcon={<Image style={{ width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }} source={require('../Images/search.png')}></Image>} />
                </View>

                <FlatList
                    style={{ marginTop: metrics.dimen_7 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.countryDataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => this.renderItemCountries(item)}>
                </FlatList>
            </View>
        )
    }


    onProceedToNext = () => {
        if (this.state.fromMobilNo) {
            if (this.state.currencycode == "") {
                this.refs.toast.show("Please enter number with country code")
            }
            else if (this.state.phone == "") {
                this.refs.toast.show("Phone Number Required!")
            }
            else if (this.state.value == 'Select One') {
                this.refs.toast.show("Please Select Currency!")
            } else if (this.state.amount == "") {
                this.refs.toast.show("Enter Amount to send!")
            } else if (parseFloat(this.state.available_Balance) < parseFloat(this.state.amount)) {
                this.refs.toast.show("Insufficient funds!")
            }
            else {
                let postData = { receiverPhone: this.state.currencycode + this.state.phone, user_id: this.state.user_id }
                console.log("postData send money", postData)
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/send-money-phone-check',
                    data: postData,
                    headers: { 'Authorization': this.state.token },
                }).then((response) => {
                    console.log("send-money-phone-check", response.data)
                    if (response.data.success.status == 200) {
                        this.props.navigation.navigate('ReviewSend', { sendCurrency: this.state.value == "USD" ? 1 : 5, user_id: this.state.user_id, sendAmount: this.state.amount, balance: this.state.available_Balance, emailPhone: this.state.fromMobilNo ? this.state.currencycode + this.state.phone : this.state.email, txn_ref_id: '', totalFees: '', fromTxnList: "0" })
                    } else {
                        this.setState({
                            invalid: true
                        })
                    }
                }).catch((err) => {
                });

            }
        } else {
            if (this.state.email == "") {
                this.refs.toast.show("Email ID Required!")
            } else if (this.state.value == 'Select One') {
                this.refs.toast.show("Please Select Currency!")
            } else if (this.state.amount == "") {
                this.refs.toast.show("Enter Amount to send!")
            } else if (parseFloat(this.state.available_Balance) < parseFloat(this.state.amount)) {
                this.refs.toast.show("Insufficient funds!")
            }
            else {
                let postData = { receiverEmail: this.state.email, user_id: this.state.user_id }
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/send-money-email-check',
                    data: postData,
                    headers: { 'Authorization': this.state.token },
                }).then((response) => {
                    console.log("send-money-email-check", response)
                    if (response.data.success.status == 200) {
                        this.props.navigation.navigate('ReviewSend', { sendCurrency: this.state.value == "USD" ? 1 : 5, user_id: this.state.user_id, sendAmount: this.state.amount, balance: this.state.available_Balance, emailPhone: this.state.fromMobilNo ? this.state.phone : this.state.email, fromTxnList: "0", txn_ref_id: '', totalFees: '' })
                    } else {
                        this.setState({
                            invalid: true
                        })
                    }
                }).catch((err) => {
                    alert(err)
                });
            }
        }


    }

    render() {
        let symbol_cuurency = this.state.symbol
        let currencyUsed;
        if (symbol_cuurency === "USD") {
            currencyUsed = "$ "
        } else {
            currencyUsed = "EC$ "
        }


        let name = this.state.isoName
        var inshort = name.toLocaleLowerCase();

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Send Money</Text>
                </View>

                <View style={styles.absouluteview}>



                    <View style={{ flex: 1 }}>

                        <View style={{ margin: metrics.dimen_25, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ color: '#323232', fontWeight: '500', fontSize: metrics.text_header }}>CaribPay Wallet</Text>
                                <Text style={{ color: colors.heading_black_text, fontWeight: '500', fontSize: metrics.text_description, marginTop: metrics.dimen_10 }}>{"Available Balance : " + currencyUsed + this.state.available_Balance}</Text>
                            </View>

                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, borderRadius: metrics.dimen_40 / 2, backgroundColor: '#f2f2f2', justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_22, width: metrics.dimen_22, alignSelf: 'center' }}
                                    source={require('../Images/balance_wallet.png')}></Image>
                            </View>
                        </View>



                        <View style={{ flexDirection: 'row', backgroundColor: colors.carib_pay_blue, height: metrics.dimen_40, alignSelf: 'center', width: '90%', margin: 2, borderRadius: metrics.dimen_10, borderColor: colors.light_grey_backgroud, borderWidth: 0.3 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromMobilNo ? '#f2f2f2' : colors.transparent }} onPress={() => this.setState({ fromMobilNo: true, fromEmail: false })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromMobilNo ? '#f2f2f2' : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromMobilNo ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Mobile No.</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromEmail ? '#f2f2f2' : colors.transparent }} onPress={() => this.setState({ fromEmail: true, fromMobilNo: false })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromEmail ? '#f2f2f2' : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromEmail ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Email</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        {this.state.fromMobilNo == false ?
                            <View style={{ ...styles.boxContainer, marginTop: metrics.dimen_10 }}>
                                <View style={{ flex: 1 }}>
                                    <Input
                                        placeholder={'Enter Email ID'}
                                        placeholderTextColor={colors.heading_black_text}
                                        containerStyle={{ alignSelf: 'center' }}
                                        value={this.state.email}
                                        multiline={false}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.text_header, color: colors.heading_black_text }}
                                        onChangeText={(text) => this.setState({ email: text })}>
                                    </Input>
                                </View>
                            </View> :
                            <View style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Input
                                        placeholder={'Code'}
                                        value={this.state.currencycode}
                                        onChangeText={(text) => this.setState({ currencycode: text })}
                                        leftIcon={
                                            <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                                {this.state.isoName == "" ?
                                                    <Image style={{ height: 20, width: 20 }} source={require('../Images/globe.png')}>
                                                    </Image> :
                                                    <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://www.countryflags.io/' + inshort + '/flat/64.png' }}>
                                                    </Image>
                                                }
                                            </TouchableOpacity>
                                        }
                                        containerStyle={{ width: "30%", height: 40 }}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: 13 }}>
                                    </Input>

                                    <Input
                                        placeholder={'Mobile No.'}
                                        containerStyle={{ width: "70%", height: 50 }}
                                        keyboardType="number-pad"
                                        value={this.state.phone}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: 13 }}
                                        onChangeText={(text) => this.setState({ phone: text })}>
                                    </Input>
                                </View>
                            </View>}

                        <TouchableOpacity onPress={() => this.setState({ isVisible: true })}>
                            <View style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, padding: 10 }}>Currency</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: colors.app_gray, fontSize: metrics.text_description, padding: 10, alignSelf: 'center' }}>{this.state.value}</Text>
                                    <Image style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center', marginRight: 10, tintColor: colors.app_gray }}
                                        source={require('../Images/dropdown.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <Text style={{ fontSize: metrics.text_medium, color: colors.app_light_gray, marginHorizontal: metrics.dimen_20, marginTop: 5 }}>Suggestions</Text>


                        <View style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}>
                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, padding: 10 }}>Amount</Text>
                            <View style={{ flex: 1 }}>
                                <Input
                                    placeholder={'$ 10'}
                                    containerStyle={{ alignSelf: 'center' }}
                                    keyboardType="number-pad"
                                    value={this.state.amount}
                                    multiline={false}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: this.state.fromSelections ? metrics.text_large : metrics.text_normal, textAlign: 'right', color: this.state.fromSelections ? colors.app_blueColor : null, fontWeight: this.state.fromSelections ? 'bold' : "200" }}
                                    onChangeText={(text) => this.setState({ amount: text, fromSelections: false })}>
                                </Input>
                            </View>
                        </View>


                        <View style={{ justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '90%', borderRadius: metrics.dimen_15, marginTop: metrics.dimen_7 }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.amountsuggestions}
                                extraData={this.state.selectedID}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{ backgroundColor: item.id == this.state.selectedID ? colors.theme_caribpay : colors.light_grey_backgroud, margin: metrics.dimen_5, justifyContent: 'center', width: 70, height: metrics.dimen_35, borderRadius: metrics.dimen_20, borderColor: colors.app_gray, borderWidth: 0.5 }}>
                                        <TouchableOpacity onPress={() => this.handleSelection(item, item.id)}>
                                            <Text style={{ fontSize: metrics.text_normal, color: item.id == this.state.selectedID  ? "white" : colors.heading_black_text, textAlign: 'center', marginTop: 2, fontWeight: 'bold' }}>{"USD " + item.amount}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View>


                        {/* <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, marginHorizontal: metrics.dimen_20, marginTop:10}}>Suggestions</Text>


                        <View style={{ justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '90%', borderRadius: metrics.dimen_15, marginTop: metrics.dimen_7 }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={Facilities_data}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{ backgroundColor: colors.light_grey_backgroud, margin: metrics.dimen_5, justifyContent: 'center', width: 100, height: metrics.dimen_35, borderRadius: metrics.dimen_7 }}>
                                        <TouchableOpacity onPress={() => this.setState({
                                            amount: item.amount
                                        })}>
                                            <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>{item.amount}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View> */}
                    </View>






                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isVisible}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, flexDirection: 'column', height: 230 }}>
                                <Text style={{ fontSize: metrics.text_heading, color: colors.theme_caribpay, fontWeight: 'bold', textAlign: 'center', margin: 10 }}>Select Currency</Text>
                                <View style={{ flexDirection: 'column', margin: 10 }}>
                                    <View style={{ marginLeft: metrics.dimen_30 }}>
                                        <RadioForm
                                            radio_props={radio_props}
                                            initial={this.state.symbol == "USD" ? 0 : 1}
                                            buttonSize={metrics.dimen_20}
                                            onPress={(value) => { this.setState({ value: value, symbol: value }) }} /></View>

                                    <View style={styles.horizontalLine}></View>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                                    <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>

                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />

                    <RBSheet
                        ref={ref => {
                            this.RBSheetAlert = ref;
                        }}
                        height={Dimensions.get('screen').height}
                        duration={0}>
                        {this.renderCountries()}
                    </RBSheet>




                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 250, justifyContent: 'center' }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                            source={require("../Images/logo.png")}></Image>
                                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>{this.state.fromMobilNo ? "No user found by this Phone Number!" : "No user found by this Email ID!"}</Text>
                                    </View>
                                    <View style={styles.bottomview2}>
                                        <TouchableOpacity onPress={() => this.setState({ invalid: false })}>
                                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>




                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.onProceedToNext()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Next</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    horizontalLine: { borderBottomColor: '#D3D3D3', marginBottom: 10, borderBottomWidth: 1, width: '100%', alignSelf: 'center', marginTop: 20 },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f2f2f2', borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
})
