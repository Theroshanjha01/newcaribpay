import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, BackHandler } from 'react-native';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';



export default class ScanSuccessFull extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            transfferedTo: this.props.navigation.state.params.transfferedTo,
            from: this.props.navigation.state.params.from,
            paymentMode: this.props.navigation.state.params.paymentMode,
            amount: this.props.navigation.state.params.amount,
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    render() {
        var newdDate = new Date().toLocaleDateString();
        var NewTeime = new Date().toLocaleTimeString();
        let txn_date = newdDate + " " + NewTeime
        return (
            <View style={styles.container}>

                <View style={{ height: metrics.dimen_200, backgroundColor: colors.app_dark_green_color, justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_80, resizeMode: 'contain', alignSelf: 'center', marginStart: metrics.dimen_30 }} source={require('../Images/tick_success.png')}></Image>
                        <Text style={{ fontSize: metrics.text_17, color: colors.white, fontWeight: 'bold', marginLeft: metrics.dimen_30, textAlignVertical: 'center' }}>Transferred Successfully!</Text>
                    </View>
                </View>


                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Transferred To</Text>
                        <Text style={styles.subHeading}>{this.state.transfferedTo}</Text>
                        <View style={styles.horizontal} />
                    </View>

                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>From</Text>
                        <Text style={styles.subHeading}>{this.state.from}</Text>
                        <View style={styles.horizontal} />
                    </View>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Amount Transferred </Text>
                        <Text style={styles.subHeading}>{"$" + this.state.amount + " USD"}</Text>
                        <View style={styles.horizontal} />
                    </View>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Payment Mode</Text>
                        <Text style={styles.subHeading}>{this.state.paymentMode}</Text>
                        <View style={styles.horizontal} />
                    </View>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>{"Date & Time"}</Text>
                        <Text style={styles.subHeading}>{txn_date}</Text>
                        <View style={styles.horizontal} />
                    </View>

                </View>

                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                        <View style={{ height: 50, borderRadius: metrics.dimen_10, backgroundColor: colors.app_gray, justifyContent: 'center' }}>
                            <Text style={{ textAlignVertical: 'center', textAlign: 'center', fontSize: metrics.text_description, color: 'white', fontWeight: 'bold' }}>Back to Home</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    textHeading: { fontSize: metrics.text_medium, color: colors.black, textAlignVertical: 'center' },
    subHeading: { fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', textAlignVertical: 'center' },
    horizontal: { borderBottomColor: 'black', borderBottomWidth: 0.5, marginTop: metrics.dimen_10 },
    bottomview: { bottom: 10, height: 60, width: '90%', position: 'absolute', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },

})