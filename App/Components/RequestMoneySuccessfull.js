import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image,BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';

export default class RequestMoneySuccessfull extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: this.props.navigation.state.params.amount,
            phone: this.props.navigation.state.params.phone,
            remarks: this.props.navigation.state.params.remarks,
            name: this.props.navigation.state.params.name,
        }
    }


    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    render() {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        console.log(this.state.phone, dateTime)
        return (
            <View style={styles.container}>

            <View style={{ height: metrics.dimen_200, backgroundColor: colors.green, justifyContent: 'center' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_80, resizeMode: 'contain', alignSelf: 'center', marginStart: metrics.dimen_30 }} source={require('../Images/tick_success.png')}></Image>
                    <Text style={{ fontSize: metrics.text_17, color: colors.white, fontWeight: 'bold', marginLeft: metrics.dimen_30, textAlignVertical: 'center' }}>Requested SuccessFully!</Text>
                </View>
            </View>


            <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>


                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                    <Text style={styles.textHeading}>Transferred To </Text>
                    <Text style={styles.subHeading}>{this.state.phone}</Text>
                    <View style={styles.horizontal} />
                </View>

                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                    <Text style={styles.textHeading}>From</Text>
                    <Text style={styles.subHeading}>{this.state.name}</Text>
                    <View style={styles.horizontal} />
                </View>


                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                    <Text style={styles.textHeading}>Amount Transferred</Text>
                    <Text style={styles.subHeading}>{"$ " + this.state.amount }</Text>
                    <View style={styles.horizontal} />
                </View>


                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                    <Text style={styles.textHeading}>Payment Mode</Text>
                    <Text style={styles.subHeading}>{"Wallet"}</Text>
                    <View style={styles.horizontal} />
                </View>


                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                    <Text style={styles.textHeading}>Recharge On</Text>
                    <Text style={styles.subHeading}>{dateTime}</Text>
                    <View style={styles.horizontal} />
                </View>



            </View>

            <View style={styles.bottomview}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <View style={{ height: 50, borderRadius: metrics.dimen_10, backgroundColor: colors.theme_caribpay, justifyContent: 'center' }}>
                        <Text style={{ textAlignVertical: 'center', textAlign: 'center', fontSize: metrics.text_description, color: 'white', fontWeight: 'bold' }}>Back to Home</Text>
                    </View>
                </TouchableOpacity>
            </View>

        </View>
    )
}
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: colors.white
},
textHeading: { fontSize: metrics.text_medium, color: colors.black, textAlignVertical: 'center' },
subHeading: { fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', textAlignVertical: 'center' },
horizontal: { borderBottomColor: 'black', borderBottomWidth: 0.5, marginTop: metrics.dimen_10 },
bottomview: { bottom: 10, height: 60, width: '90%', position: 'absolute', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },

})