import React, { Component } from 'react';
import {
    SafeAreaView, StyleSheet, Text, View, BackHandler, Image, TouchableOpacity, Dimensions
} from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import { Input } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';
import ProgressDialog from '../ProgressDialog.js';
import Modal from 'react-native-modal';








export default class TermsNConditions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            passcode: '',
            newPasscode: '',
            oldPasscode: '',
            confirmPasscode: '',
            modalvisible: false,
            oldPasscodenotVerify: false,
            passwordmismatches: false
        }
    };

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let ispasscode = await AsyncStorage.getItem('passcode')
        console.log("ispascode --- > ", ispasscode)
        this.setState({
            passcode: ispasscode
        })

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    OnChangePasscode = () => {
        if (this.state.oldPasscode == "") {
            this.refs.toast.show("Please enter your current Passcode");
        }
        else if (this.state.newPasscode == "") {
            this.refs.toast.show("Please enter your New Passcode");
        }
        else if (this.state.confirmPasscode == "") {
            this.refs.toast.show("Please confirm your New Passcode");
        }
        else if (this.state.newPasscode != this.state.confirmPasscode) {
            this.setState({
                passwordmismatches: true
            })
        }
        else if (this.state.oldPasscode != this.state.passcode) {
            this.setState({
                oldPasscodenotVerify: true
            })
        }
        else {
            AsyncStorage.setItem('passcode', this.state.newPasscode)
            this.setState({
                modalvisible: true
            })
        }
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>{"Change Passcode"}</Text>
                </View>

                <View style={{ flex: 1 }}>
                    <ProgressDialog visible={this.state.visible} />
                    <View style={{ margin: metrics.dimen_20 }}>
                        <Text style={{ fontSize: metrics.text_header, color: colors.black }}>Current Passcode</Text>
                        <Input
                            containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                            placeholder='old Passcode'
                            placeholderTextColor={colors.place_holder_color}
                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                            keyboardType='number-pad'
                            maxLength={4}
                            inputStyle={{ color: colors.heading_black_text, fontSize: metrics.text_normal, textAlignVertical: 'bottom', paddingHorizontal: 5 }}
                            value={this.state.oldPasscode}
                            onChangeText={(text) => this.setState({ oldPasscode: text })} />

                        <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>New Passcode</Text>

                        <Input
                            containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                            keyboardType='number-pad'
                            placeholder='New Passcode'
                            maxLength={4}
                            placeholderTextColor={colors.place_holder_color}
                            inputContainerStyle={{ borderBottomWidth: 0.2, marginBottom: 0 }}
                            inputStyle={{ color: colors.heading_black_text, fontSize: metrics.text_normal, textAlignVertical: 'bottom', paddingHorizontal: 5 }}
                            value={this.state.newPasscode}
                            onChangeText={(text) => this.setState({ newPasscode: text })} />


                        <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Confirm Passcode</Text>


                        <Input
                            containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                            keyboardType='number-pad'
                            placeholder='Confirm Passcode'
                            maxLength={4}
                            placeholderTextColor={colors.place_holder_color}
                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                            inputStyle={{ color: colors.heading_black_text, fontSize: metrics.text_normal, textAlignVertical: 'bottom', paddingHorizontal: 5 }}
                            value={this.state.confirmPasscode}
                            onChangeText={(text) => this.setState({ confirmPasscode: text })} />

                    </View>

                    <View style={{ ...styles.bottomview, backgroundColor: colors.theme_caribpay }}>
                        <TouchableOpacity onPress={() => this.OnChangePasscode()}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Change Passcode</Text>
                        </TouchableOpacity>
                    </View>

                </View>


                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'black' }}
                    position='center'
                    positionValue={200}
                    fadeInDuration={200}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }} />


                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.modalvisible} >
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Passcode Change Successfully!"}</Text>
                                <TouchableOpacity onPress={() => this.setState({
                                    modalvisible: false
                                }, () => this.props.navigation.goBack(null))}>
                                    <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>


                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.oldPasscodenotVerify} >
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Old Passcode is Wrong , Try Again!"}</Text>
                                <TouchableOpacity onPress={() => this.setState({
                                    oldPasscodenotVerify: false
                                })}>
                                    <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>

                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.passwordmismatches} >
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"New Passcode Mismatchs , Please Try Again!"}</Text>
                                <TouchableOpacity onPress={() => this.setState({
                                    passwordmismatches: false
                                })}>
                                    <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>



            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    header: {
        height: metrics.dimen_60,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_15,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        // marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        // marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', textAlign: 'center'
    },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: "#f2f2f2", borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },

});
