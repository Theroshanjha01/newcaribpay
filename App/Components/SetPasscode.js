import React, { Component } from 'react';
import { SafeAreaView , View, Text, Image, TouchableOpacity, BackHandler } from 'react-native';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import ProgressDialog from '../ProgressDialog.js';
const axios = require('axios');
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';



export default class SetPasscode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: "",
            visible: false,
            deviceID: '',
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            enabled: false,
        }
    }

    componentDidMount() {
        let uniqueId = DeviceInfo.getUniqueId();
        this.setState({
            deviceID: uniqueId
        });
        // this.requestUserPermission();
        // this.recvmessages();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    // 
    // async requestUserPermission() {
    //     const authStatus = await messaging().requestPermission();
    //     const enabled =
    //         authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    //         authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    //     if (enabled) {
    //         console.log('Authorization status:', authStatus);
    //         let fcmtoken = await firebase.messaging.getToken()
    //         console.log("fcmm token is------> ",fcmtoken)
    //     }




    // messaging().onNotificationOpenedApp(remoteMessage => {
    //     console.log(
    //       'Notification caused app to open from background state:',
    //       remoteMessage.notification,
    //     );
    //     navigation.navigate(remoteMessage.data.type);
    //   });


    //   messaging()
    //     .getInitialNotification()
    //     .then(remoteMessage => {
    //       if (remoteMessage) {
    //         console.log(
    //           'Notification caused app to open from quit state:',
    //           remoteMessage.notification,
    //         );
    //         // setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
    //       }

    //     });
    // }






    // async getToken() {
    //     let fcmToken = await AsyncStorage.getItem('fcmToken');
    //     if (!fcmToken) {
    //         fcmToken = await firebase.messaging().getToken();
    //         if (fcmToken) {
    //             await AsyncStorage.setItem('fcmToken', fcmToken);
    //             let data = {
    //                 user_id: this.state.user_id,
    //                 device_id: this.state.deviceID,
    //                 fcm_token: fcmToken
    //             }
    //             axios({
    //                 method: 'post',
    //                 url: 'https://sandbox.caribpayintl.com/api/register-device',
    //                 headers: { 'Authorization': this.state.token },
    //                 data: data,
    //             }).then((response) => {
    //                 if (response.data.success.status == 200) {
    //                     console.log("success message ---> ",response.data.success.message)
    //                 }
    //             }).catch((err) => {
    //                 this.setState({ visible: false })
    //                 alert(err)
    //             });
    //         }
    //     }
    // }

    // async requestPermission() {
    //     try {
    //         await firebase.messaging().requestPermission();
    //         this.getToken();
    //     } catch (error) {
    //         console.log('permission rejected');
    //     }
    // }




    onSetPasscodeClick = () => {
        this.setState({ visible: true })
        let data = {
            device_id: this.state.deviceID,
            passcode: this.state.code,
            user_id: this.state.user_id
        }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/set-passcode',
            headers: { 'Authorization': this.state.token },
            data: data,
        }).then((response) => {
            if (response.data.success.status == 200) {
                AsyncStorage.setItem("DeviceId", this.state.deviceID)
                AsyncStorage.setItem("passcode", this.state.code)
                this.setState({
                    visible: false,
                    enabled: true
                })
            }
        }).catch((err) => {
            this.setState({ visible: false })
            alert(err)
        });
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, alignSelf: 'center', margin: metrics.dimen_20 }}>
                <ProgressDialog visible={this.state.visible} />
                <Text style={{ fontSize: metrics.text_heading, color: colors.heading_black_text, fontWeight: '700', textAlign: 'center' }}>Create your Passcode</Text>
                <Image style={{ height: metrics.dimen_100, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/logo.png')}></Image>
                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, textAlign: 'center', marginTop: 10 }}>You can now also login into the application by using these passcode.</Text>
                <View style={{ marginTop: 15, alignSelf: 'center' }}>
                    <SmoothPinCodeInput
                        placeholder=""
                        cellSize={65}
                        cellSpacing={7}
                        keyboardType='phone-pad'
                        cellStyle={{
                            borderWidth: 2,
                            borderRadius: 7,
                            borderColor: 'mediumturquoise',
                            backgroundColor: 'azure',
                        }}
                        cellStyleFocused={{
                            borderColor: 'lightseagreen',
                            backgroundColor: 'lightcyan',
                        }}
                        textStyle={{
                            fontSize: 24,
                            color: colors.black,
                            fontWeight: 'bold'
                        }}
                        textStyleFocused={{
                            color: 'crimson'
                        }}
                        value={this.state.code}
                        onTextChange={code => this.setState({ code: code })}
                    />
                </View>



                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.enabled}>
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', fontWeight: 'bold' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Passcode Set Successfully!"}</Text>

                                <TouchableOpacity onPress={() => this.setState({ enabled: false }, () => this.props.navigation.goBack(null))}>
                                    <View style={{ height: metrics.dimen_30, width: 200, borderRadius: metrics.dimen_7, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"Back to Home"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>



                <View style={{ bottom: 5, position: 'absolute', height: 40, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => this.onSetPasscodeClick()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Set Passcode</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        );
    }
}

