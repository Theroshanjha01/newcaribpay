import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, BackHandler, Image, Clipboard, Dimensions } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Permissions from 'react-native-permissions';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
const axios = require('axios');
import ProgressDialog from '../ProgressDialog.js';
import AsyncStorage from '@react-native-community/async-storage';
import { Input } from 'react-native-elements';
import Modal from 'react-native-modal';
import Toast, { DURATION } from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';


export default class ScanQR extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            scannedQRCode: '',

            fromGetUser: true,
            fromusers: false,
            baseUrl: 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=',
            utfCoding: '&choe=UTF-8',
            secretQRCode: '',
            fromOnsuccess: false,


            merchantId: '',
            merchantPaymentAmount: '',
            merchantActualFee: '',
            merchantCalculatedChargePercentageFee: '',
            merchantCurrencyId: '',
            merchantUserId: '',
            total_merchant_pay: '',
            merchantName: '',
            from: '',

            enteredAmount: '',
            isVisiblePayment: false,
            fromExpressView: false,

            expressMerchantActualFee: '',
            expressMerchantBusinessName: '',
            expressMerchantPaymentCurrencyId: '',
            expressMerchantUserId: '',
            expreessFess: '',
            total_expreess_pay: ''
        }
    }





    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Permissions.request('camera', {
        }).then(response => {
            if (response == 'authorized') {
                this.setState({ secretQRCode: '' });
            }
        });
        this.setState({ fromGetUser: true, fromusers: false })

        this.getUserQRCode();

        let user_name = await AsyncStorage.getItem('first_name');
        let last_user_name = await AsyncStorage.getItem('last_name');

        let userName = this.camelCase(user_name) + " " + this.camelCase(last_user_name)

        this.setState({
            from: userName
        })
    }


    camelCase(str) {
        console.log("string received = ", str)
        let wordsArray = str.split(' ')
        return wordsArray.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    }


    writeToClipboard = async () => {
        await Clipboard.setString(this.state.secretQRCode);
        this.refs.toast.show('Copied to Clipboard!')
    }



    getUserQRCode = () => {
        let postData = { user_id: this.state.user_id }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/add-or-update-user-qr-code',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            if (response.data.status == 200) {
                this.setState({
                    secretQRCode: response.data.secret,
                    visible: false

                });
            }
        }).catch((err) => {
            alert(err)
        });
    }



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }


    onSuccess = (e) => {
        console.log("scan code is ---", e.data)
        this.setState({ scannedQRCode: e.data })
        let postData = { resultText: e.data }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/perform-qr-code-operation',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            if (response.status == 200) {
                if (response.data.userType == "user") {
                    let scan_email = response.data.receiverEmail
                    this.props.navigation.navigate('SendMoney', { fromMobilNo: false, fromEmail: true, email: scan_email })
                }
                else if (response.data.userType == "standard_merchant") {
                    if (response.data.status == true) {
                        let merchantId = response.data.merchantId
                        let user_id = this.state.user_id
                        let merchantDefaultCurrencyCode = response.data.merchantDefaultCurrencyCode
                        let merchantPaymentAmount = response.data.merchantPaymentAmount
                        console.log("Inform coming ---> ", merchantId, user_id, merchantPaymentAmount, merchantDefaultCurrencyCode)
                        let bodyData = { user_id: this.state.user_id, userType: 'standard_merchant', merchantId: merchantId, merchantDefaultCurrencyCode: merchantDefaultCurrencyCode, merchantPaymentAmount: merchantPaymentAmount }
                        console.log(bodyData)
                        axios({
                            method: 'post',
                            url: 'https://sandbox.caribpayintl.com/api/perform-merchant-payment-qr-code-review',
                            data: bodyData,
                            headers: { 'Authorization': this.state.token },
                        }).then((response) => {
                            if (response.data.status == 200) {
                                this.setState({
                                    merchantId: merchantId,
                                    merchantPaymentAmount: parseInt(response.data.merchantPaymentAmount).toFixed(2),
                                    merchantActualFee: response.data.merchantActualFee,
                                    merchantCalculatedChargePercentageFee: response.data.merchantCalculatedChargePercentageFee,
                                    merchantCurrencyId: response.data.merchantCurrencyId,
                                    merchantUserId: response.data.merchantUserId,
                                    merchantName: response.data.merchantBusinessName
                                })

                                var a = parseFloat(this.state.merchantPaymentAmount)
                                var b = parseFloat(this.state.merchantCalculatedChargePercentageFee)
                                var total = a + b

                                this.setState({
                                    total_merchant_pay: total,
                                    fromusers: false,
                                    fromOnsuccess: true
                                })

                            } else {
                                alert("Merchant Not Exists!");
                            }
                        }).catch((err) => {
                            console.log("message ---> ", err)
                        });
                    }
                }
                else if (response.data.userType == "express_merchant") {
                    let merchantId = response.data.merchantId
                    let merchantDefaultCurrencyCode = response.data.merchantDefaultCurrencyCode
                    this.setState({ fromusers: false, merchantId: merchantId, merchantDefaultCurrencyCode: merchantDefaultCurrencyCode, isVisiblePayment: true, })
                }
                else {
                    alert("Invalid Code!");
                }
            }
        }).catch((err) => {
            alert("Invalid Code!");
        })



    }


    onPayNow = () => {
        let postData = {
            merchantPaymentAmount: this.state.merchantPaymentAmount,
            merchantActualFee: this.state.merchantActualFee,
            merchantCurrencyId: this.state.merchantCurrencyId,
            merchantUserId: this.state.merchantUserId,
            merchantId: this.state.merchantId,
            user_id: this.state.user_id
        }
        console.log(postData)
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/perform-merchant-payment-qr-code-submit',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            if (response.status == 200) {
                this.setState({
                    fromOnsuccess: false,
                    fromusers: true
                });
                this.props.navigation.navigate("ScanSuccessFull", { transfferedTo: this.state.merchantName, from: this.state.from, paymentMode: "Wallet", amount: this.state.merchantPaymentAmount })
            }
        }).catch((err) => {
            alert(err)
        });
    }


    onPayNowExpressPayment = () => {
        this.setState({
            visible: true
        });
        let postData = {
            expressMerchantPaymentAmount: this.state.enteredAmount,
            expressMerchantActualFee: this.state.expressMerchantActualFee,
            expressMerchantPaymentCurrencyId: this.state.expressMerchantPaymentCurrencyId,
            expressMerchantUserId: this.state.expressMerchantUserId,
            expressMerchantId: this.state.expressMerchantId,
            user_id: this.state.user_id
        }
        console.log(postData)
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/perform-express-merchant-payment-qr-code-submit',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            if (response.status == 200) {
                this.setState({ visible: false })
                this.props.navigation.navigate("ScanSuccessFull", { transfferedTo: this.state.expressMerchantBusinessName, from: this.state.from, paymentMode: "Wallet", amount: this.state.enteredAmount })
            }
        }).catch((err) => {
            alert(err)
        });
    }


    proceedtoPay = () => {
        if (this.state.enteredAmount == "") {
            alert('Please enter the transfer amount before proceed')
        } else {
            this.setState({
                fromusers: false,
                fromExpressView: true
            })
            let postData = {
                expressMerchantId: this.state.merchantId,
                expressMerchantPaymentCurrencyCode: this.state.merchantDefaultCurrencyCode,
                user_id: this.state.user_id
            }
            console.log(postData)
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/perform-express-merchant-payment-qr-code-merchant-currency-user-wallets-review',
                data: postData,
                headers: { 'Authorization': this.state.token },
            }).then((response) => {
                if (response.data.status == 200) {
                    this.setState({
                        expressMerchantActualFee: parseFloat(response.data.expressMerchantActualFee).toFixed(2),
                        expressMerchantBusinessName: response.data.expressMerchantBusinessName,
                        expressMerchantPaymentCurrencyId: response.data.expressMerchantPaymentCurrencyId,
                        expressMerchantUserId: response.data.expressMerchantUserId,
                        fromExpressView: true
                    })
                    var fees = this.state.expressMerchantActualFee / 100 * parseFloat(this.state.enteredAmount).toFixed(2)
                    var a = fees
                    var b = parseFloat(this.state.enteredAmount).toFixed(2)
                    var total = parseFloat(a) + parseFloat(b)
                    this.setState({
                        expreessFess: fees,
                        total_expreess_pay: total,
                        isVisiblePayment: false,
                        fromExpressView: true
                    })
                }
            }).catch((err) => {
                alert(err)
            });
        }

    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.theme_caribpay }}>
                <View style={{ flexDirection: 'row', height: 50, backgroundColor: colors.theme_caribpay }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                        <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center', marginTop: 5 }}>
                            <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, tintColor: 'white', alignSelf: 'center', marginStart: 10 }} source={require('../Images/leftarrow.png')}></Image>
                        </View>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center', marginHorizontal: 110 }}>{"Scan & Pay"}</Text>
                </View>
                <ProgressDialog visible={this.state.visible} />

                <View style={{ justifyContent: 'center' }}>
                    {this.state.fromGetUser && this.state.secretQRCode != null &&
                        <View style={{ alignSelf: 'center', marginBottom: 10, justifyContent: 'center', marginTop: metrics.dimen_180 }}>
                            <Image style={styles.barcode}
                                source={{ uri: (this.state.baseUrl) + (this.state.secretQRCode) + (this.state.utfCoding) }}>
                            </Image>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center' }}>Scan QR Code to Pay</Text>
                        </View>
                    }
                    {this.state.fromusers &&
                        <View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center' }}>Scan QR Code to Pay/Transfer</Text>
                            <QRCodeScanner
                                showMarker={true}
                                containerStyle={{ height: metrics.dimen_300, width: Dimensions.get('screen').width - 60, alignSelf: 'center', marginTop: 100 }}
                                cameraStyle={{ height: metrics.dimen_300, width: Dimensions.get('screen').width - 60, alignSelf: 'center' }}
                                onRead={this.onSuccess} />
                        </View>
                    }

                    {this.state.fromOnsuccess &&
                        <View style={{ margin: metrics.dimen_15 }}>
                            <Image style={{
                                height: 200,
                                width: 200,
                                resizeMode: 'contain',
                                alignSelf: 'center',
                                marginBottom: 10
                            }}
                                source={{ uri: (this.state.baseUrl) + (this.state.scannedQRCode) + (this.state.utfCoding) }}>
                            </Image>

                            <Text style={{ fontSize: metrics.text_normal, color: colors.theme_ornage_color, marginTop: metrics.dimen_15, marginLeft: metrics.dimen_15 }}>Transaction Preview</Text>

                            <View style={{ flexDirection: 'column', margin: 5 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Amount </Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_yellow, textAlign: 'center' }}>{"$ " + parseFloat(this.state.merchantPaymentAmount).toFixed(2)}</Text>
                                </View>
                                <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Fee</Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_yellow, textAlign: 'center' }}>{"$ " + parseFloat(this.state.merchantCalculatedChargePercentageFee).toFixed(2)}</Text>
                                </View>
                                <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>


                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Total Amount</Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_yellow, textAlign: 'center' }}>{"$ " + parseFloat(this.state.total_merchant_pay).toFixed(2)}</Text>
                                </View>
                                <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                            </View>

                            <TouchableOpacity onPress={() => this.onPayNow()}>
                                <View style={{ height: metrics.dimen_50, width: '60%', alignSelf: 'center', justifyContent: 'center', borderRadius: metrics.dimen_10, backgroundColor: colors.carib_pay_blue, marginTop: 30 }}>
                                    <LinearGradient style={{ borderRadius: metrics.dimen_10, height: metrics.dimen_50, justifyContent: 'center' }} colors={['#283142', '#3b5998', '#283142']}>
                                        <Text style={{ fontSize: metrics.text_16, color: colors.white, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center' }}>PAY NOW</Text>
                                    </LinearGradient>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    {this.state.fromExpressView &&
                        <View style={{ margin: metrics.dimen_15 }}>
                            <Image style={{
                                height: 200,
                                width: 200,
                                resizeMode: 'contain',
                                alignSelf: 'center',
                                marginBottom: 10
                            }}
                                source={{ uri: (this.state.baseUrl) + (this.state.scannedQRCode) + (this.state.utfCoding) }}>
                            </Image>

                            <Text style={{ fontSize: metrics.text_normal, color: colors.theme_ornage_color, marginTop: metrics.dimen_15, marginLeft: metrics.dimen_15 }}>Transaction Preview</Text>

                            <View style={{ flexDirection: 'column', margin: 5 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Amount </Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_yellow, textAlign: 'center' }}>{"$ " + parseFloat(this.state.enteredAmount).toFixed(2)}</Text>
                                </View>
                                <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Fee</Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_yellow, textAlign: 'center' }}>{"$ " + parseFloat(this.state.expreessFess).toFixed(2)}</Text>
                                </View>
                                <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>


                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Total Amount</Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_yellow, textAlign: 'center' }}>{"$ " + parseFloat(this.state.total_expreess_pay).toFixed(2)}</Text>
                                </View>
                                <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                            </View>

                            <TouchableOpacity onPress={() => this.onPayNowExpressPayment()}>
                                <View style={{ height: metrics.dimen_50, width: '60%', alignSelf: 'center', justifyContent: 'center', borderRadius: metrics.dimen_10, backgroundColor: colors.carib_pay_blue, marginTop: 30 }}>
                                    <LinearGradient style={{ borderRadius: metrics.dimen_10, height: metrics.dimen_50, justifyContent: 'center' }} colors={['#283142', '#3b5998', '#283142']}>
                                        <Text style={{ fontSize: metrics.text_16, color: colors.white, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center' }}>PAY NOW</Text>
                                    </LinearGradient>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }

                    <Modal isVisible={this.state.isVisiblePayment}>
                        <View style={{ width: '90%', alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_7, height: metrics.dimen_190 }}>
                            <View style={{ flexDirection: 'column', margin: 15 }}>
                                <Text style={{ fontSize: metrics.text_description, color: colors.black, marginTop: 5 }}>Enter Transfer Amount</Text>
                                <Input
                                    placeholder={'$ 10'}
                                    containerStyle={{ width: '90%', height: 50 }}
                                    keyboardType="number-pad"
                                    numberOfLines={1}
                                    value={this.state.enteredAmount}
                                    inputContainerStyle={{ borderBottomWidth: 0.5 }}
                                    inputStyle={{ fontSize: metrics.dimen_22 }}
                                    onChangeText={(text) => this.setState({
                                        enteredAmount: text
                                    })}>
                                </Input>

                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, alignSelf: 'center' }}>
                                <TouchableOpacity onPress={() => this.proceedtoPay()}>
                                    <View style={{ flex: 0.5, width: 100, alignSelf: 'center', backgroundColor: colors.carib_pay_blue, margin: 10, justifyContent: 'center', height: metrics.dimen_30, borderRadius: metrics.dimen_7 }}>
                                        <Text style={{ textAlign: 'center', fontSize: metrics.text_description, color: colors.white, fontWeight: 'bold' }}>Pay</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({
                                    isVisiblePayment: false, fromusers: true
                                })}>
                                    <View style={{ flex: 0.5, width: 100, alignSelf: 'center', backgroundColor: colors.carib_pay_blue, margin: 10, justifyContent: 'center', height: metrics.dimen_30, borderRadius: metrics.dimen_7 }}>
                                        <Text style={{ textAlign: 'center', fontSize: metrics.text_description, color: colors.white, fontWeight: 'bold' }}>Cancel</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />
                </View>

                <View style={{ position: 'absolute', bottom: 10, flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_40, alignSelf: 'center', width: '95%', margin: 2, borderRadius: metrics.dimen_10, marginTop: metrics.dimen_7 }}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2 }} onPress={() => this.setState({ fromGetUser: true, fromusers: false, fromExpressView: false, fromOnsuccess: false })}>
                        <Text style={{ fontSize: metrics.text_medium, color: this.state.fromGetUser ? colors.app_yellow : colors.whitesmoke, textAlign: 'center' }}>My QR Code</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2 }} onPress={() => this.setState({ fromGetUser: false, fromusers: true })}>
                        <Text style={{ fontSize: metrics.text_medium, color: this.state.fromusers ? colors.app_yellow : colors.whitesmoke, textAlign: 'center' }}>Scan Users QR Code</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}



const styles = StyleSheet.create({
    centerText: {
        fontSize: metrics.text_16,
        color: '#777', textAlign: 'center'
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
        textAlign: 'center',
        fontSize: 15
    },
    buttonText: {
        fontSize: metrics.text_heading,
        fontWeight: '500',
        color: colors.heading_black_text,
        marginTop: metrics.dimen_30
    },
    barcode: {
        height: 300,
        width: 300,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginBottom: 10
    }
});





















