import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, BackHandler, FlatList } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { Input } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import Toast, { DURATION } from 'react-native-easy-toast';
const axios = require('axios');
const moment = require('moment');
import RBSheet from "react-native-raw-bottom-sheet";
var Spinner = require('react-native-spinkit');
let Facilities_data = [
    {
        id: 1,
        amount: "20",
    },
    {
        id: 2,
        amount: "50",
    },
    {
        id: 3,
        amount: "100",
    },
    {
        id: 4,
        amount: "150",
    },
    {
        id: 5,
        amount: "200",
    },
    {
        id: 6,
        amount: "300",
    }
]



export default class Wallet extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: '',
            balance: '',
            token: '',
            fromAll: true, fromRecieve: false, fromsent: false,
            user_id: '',
            transactionData: [],
            recieved_txn_Data: '',
            sent_txn_Data: '', subtotalValue: '', transaction_id: '', type: '', payment_method_name: '',
            symbol: '',
            visible: false

        }
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.getData();
            });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    getData = async () => {
        let userbalance = await AsyncStorage.getItem('available_balance');
        let userToken = await AsyncStorage.getItem('token');
        let user_id = await AsyncStorage.getItem('id');
        let symbol = await AsyncStorage.getItem('symbol');
        this.setState({
            balance: userbalance,
            token: userToken, user_id: user_id,
            symbol: symbol == "USD" ? '$' : "EC$",
            visible: true
        })

        let postData = {
            type: "allTransactions",
            user_id: this.state.user_id
        }
        axios.get('https://sandbox.caribpayintl.com/api/activityall', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            let datas = response.data.transactions
            const currencywise = datas.filter(roshan => roshan.curr_symbol == this.state.symbol && roshan.status == "Success");
            console.log(currencywise)
            const txnId1 = currencywise.filter(roshan => roshan.transaction_type_id == 1);
            const txnId4 = currencywise.filter(roshan => roshan.transaction_type_id == 4);
            const txnId9 = currencywise.filter(roshan => roshan.transaction_type_id == 9);
            const txnId12 = currencywise.filter(roshan => roshan.transaction_type_id == 12);
            var total_txn_recieved = txnId1.concat(txnId4, txnId9, txnId12);
            const txnId2 = currencywise.filter(roshan => roshan.transaction_type_id == 2);
            const txnId3 = currencywise.filter(roshan => roshan.transaction_type_id == 3);
            const txnId10 = currencywise.filter(roshan => roshan.transaction_type_id == 10);
            const txnId11 = currencywise.filter(roshan => roshan.transaction_type_id == 11);
            var total_txn_sent = txnId2.concat(txnId3, txnId10, txnId11);
            this.setState({
                transactionData: currencywise,
                recieved_txn_Data: total_txn_recieved,
                sent_txn_Data: total_txn_sent,
                visible: false
            })
        }).catch((error) => {
            console.log(error);
        })
    }


    componentWillUnmount() {
        this.willFocusSubscription.remove()
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    renderTxnDetails = () => {
        return (
            <View>
                <View style={{ height: 60, backgroundColor: colors.theme_caribpay, justifyContent: 'center' }}>
                    <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', paddingHorizontal: 20 }}>Transaction Detail</Text>
                </View>

                <View style={{ flexDirection: 'column', margin: metrics.dimen_10, borderTopLeftRadius: 10, borderTopLeftRadius: 10 }}>
                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Amount Added</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{"$ " + this.state.subtotalValue}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Transaction Id </Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.transaction_id}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Type</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.type}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Payment Method</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.payment_method_name == null ? "Wallet" : this.state.payment_method_name}</Text>
                    </View>


                </View>
            </View>
        )
    }


    onTopupWallet = () => {
        if (this.state.amount == "") {
            this.refs.toast.show("Please enter an amount first.")
        } else {
            this.props.navigation.navigate('AddMoney', { available_Balance: this.state.balance, token: this.state.token, amount: this.state.amount, symbol: this.state.symbol == "$" ? "USD" : "XCD" })
        }
    }

    onPressItems = async (item) => {
        console.log(item)
        let postData = {
            user_id: item.user_id,
            tr_id: item.id
        }
        await axios.get('https://sandbox.caribpayintl.com/api/transaction-details', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            console.log(response.data.transaction)
            if (response.data.success.status == 200) {
                this.setState({
                    subtotalValue: response.data.transaction.subtotal,
                    transaction_id: response.data.transaction.transaction_id,
                    type: response.data.transaction.transaction_type.name,
                    payment_method_name: response.data.transaction.payment_method_name,
                })
                this.RBSheetAlert.open()
            }
        }).catch((error) => {
            console.log(error);
        })
    }



    renderItem = (item) => {
        var txn_type;
        switch (item.transaction_type_id) {
            case 1:
                txn_type = "Reload"
                break;
            case 4:
                txn_type = "Money Receive"
                break;
            case 9:
                txn_type = "Request Sent"
                break;
            case 12:
                txn_type = "Shopping"
                break;
            case 3:
                txn_type = "Money Transfer"
                break;
            case 10:
                txn_type = "Request Received"
                break;
            case 11:
                txn_type = "Shopping"
                break;
            case 15:
                txn_type = "Mobile Top-Up"
                break;
        }
        return (
            <TouchableOpacity onPress={() => this.onPressItems(item)}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 15, height: 35 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        {/* {item.transaction_type == "Deposit" || item.transaction_type == "Received" ?
                            <Image style={{ width: metrics.dimen_30, height: metrics.dimen_30, resizeMode: 'center', alignSelf: 'center', marginLeft: 10 }} source={require('../Images/recv.png')}></Image> :
                            <Image style={{ width: metrics.dimen_30, height: metrics.dimen_30, resizeMode: 'center', alignSelf: 'center', marginLeft: 10 }} source={require('../Images/sent.png')}></Image>
                        } */}
                        <Image style={{ width: metrics.dimen_30, height: metrics.dimen_30, resizeMode: 'center', alignSelf: 'center', marginLeft: 10 }} source={{ uri: item.user_photo }}></Image> 

                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 20 }}>
                            <Text style={{ fontSize: metrics.text_header, color: colors.carib_my_black, marginTop: 2 }}>{txn_type}</Text>
                            <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.app_gray, marginTop: 2 }}>{moment(item.t_created_at).format('DD-MMMM-YYYY hh:mm A')}</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', marginRight: 10 }}>
                        <Text style={{ fontSize: metrics.text_header, color: colors.carib_my_black, marginTop: 2, fontWeight: 'bold' }}>{item.curr_symbol}</Text>
                        <Text style={{ fontSize: metrics.text_header, color: colors.carib_my_black, marginTop: 2, fontWeight: 'bold' }}>{item.subtotal}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <Text style={styles.headertextStyle}>Add Fund</Text>
                </View>
                <View style={styles.absouluteview}>
                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={{ fontSize: metrics.text_header, color: colors.app_gray, textAlignVertical: 'center' }}>Current Balance</Text>
                        <View style={{ flexDirection: 'row', marginTop: metrics.dimen_7 }}>
                            <Text style={{ fontSize: metrics.text_25, color: colors.metalic_gold, fontWeight: 'bold' }}>{this.state.symbol}</Text>
                            <Text style={{ fontSize: metrics.text_25, color: colors.metalic_gold, fontWeight: 'bold', paddingHorizontal: 10 }}>{this.state.balance}</Text>
                        </View>
                        <View style={styles.horizontalLine}></View>
                    </View>

                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={{ fontSize: metrics.text_header, color: colors.app_gray, textAlignVertical: 'center' }}>Add Balance in Wallet</Text>
                        <Input
                            placeholder={'Enter Amount'}
                            placeholderTextColor={colors.metalic_gold}
                            value={this.state.amount}
                            keyboardType='number-pad'
                            leftIcon={
                                <Image style={{ height: 20, width: 20 }} source={require('../Images/dollar.png')}>
                                </Image>
                            }
                            rightIcon={
                                <TouchableOpacity onPress={() => this.onTopupWallet()}>
                                    <Image style={{ height: 20, width: 20, tintColor: colors.theme_caribpay }} source={require('../Images/plus.png')}>
                                    </Image>
                                </TouchableOpacity>
                            }
                            containerStyle={{ width: "100%", height: 50, backgroundColor: colors.light_grey_backgroud, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10 }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            inputStyle={{ fontSize: this.state.fromSelections ? metrics.text_large : metrics.text_normal, textAlign: 'right', color: this.state.fromSelections ? colors.app_blueColor : null, fontWeight: this.state.fromSelections ? 'bold' : "200" }}
                            onChangeText={(text) => this.setState({ amount: text, fromSelections: false })}>
                        </Input>




                        <Text style={{ fontSize: 13, color: colors.app_gray, marginTop: 7 }}>Suggestions</Text>

                        <View style={{ justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.white, width: '100%', borderRadius: metrics.dimen_15, marginTop: metrics.dimen_7 }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={Facilities_data}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{ backgroundColor: colors.light_grey_backgroud, margin: metrics.dimen_5, justifyContent: 'center', width: 70, height: metrics.dimen_35, borderRadius: metrics.dimen_20, borderColor: colors.app_gray, borderWidth: 0.5 }}>
                                        <TouchableOpacity onPress={() => this.setState({
                                            amount: item.amount, fromSelections: true
                                        })}>
                                            <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, textAlign: 'center', marginTop: 2, fontWeight: 'bold' }}>{"USD " + item.amount}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_30, alignSelf: 'center', width: '95%', borderRadius: metrics.dimen_10 }}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromAll ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromAll: true, fromRecieve: false, fromsent: false })}>
                            <View style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromAll ? colors.light_grey_backgroud : colors.transparent }}>
                                <Text style={{ fontSize: metrics.text_description, color: this.state.fromAll ? colors.black : colors.whitesmoke, textAlign: 'center' }}>All</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromRecieve ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromRecieve: true, fromAll: false, fromsent: false })}>
                            <View style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromRecieve ? colors.light_grey_backgroud : colors.transparent }}>
                                <Text style={{ fontSize: metrics.text_description, color: this.state.fromRecieve ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Received</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromsent ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromRecieve: false, fromAll: false, fromsent: true })}>
                            <View style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromsent ? colors.light_grey_backgroud : colors.transparent }}>
                                <Text style={{ fontSize: metrics.text_description, color: this.state.fromsent ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Sent</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {this.state.visible &&
                        <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                            <Spinner style={{ marginTop: 100 }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                        </View>
                    }

                    {this.state.fromAll == false && this.state.fromRecieve == false && this.state.fromsent && this.state.sent_txn_Data.length == 0 &&
                        <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 50 }}>
                            <Image style={{ width: 150, height: 150, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require("../Images/notxn.png")}></Image>
                            <Text style={{ fontSize: metrics.text_16, color: colors.app_gray, textAlign: 'center', alignSelf: 'center' }}>Sorry, No Transactions!</Text>
                        </View>}

                    {this.state.fromAll == false && this.state.fromRecieve && this.state.fromsent == false && this.state.recieved_txn_Data.length == 0 &&
                        <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 50 }}>
                            <Image style={{ width: 150, height: 150, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require("../Images/notxn.png")}></Image>
                            <Text style={{ fontSize: metrics.text_16, color: colors.app_gray, textAlign: 'center', alignSelf: 'center' }}>Sorry, No Transactions!</Text>
                        </View>}

                    {this.state.visible == false && this.state.transactionData.length == 0 && this.state.fromsent == false && this.state.fromRecieve == false &&
                        <View style={{ justifyContent: 'center', alignSelf: 'center', margin: 50 }}>
                            <Image style={{ width: 150, height: 150, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require("../Images/notxn.png")}></Image>
                            <Text style={{ fontSize: metrics.text_16, color: colors.app_gray, textAlign: 'center', alignSelf: 'center' }}>Sorry, No Transactions!</Text>
                        </View>
                    }
                    {this.state.visible == false && this.state.transactionData.length > 0 &&
                        <FlatList
                            style={{ marginTop: 10, marginBottom: 215 }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.fromAll ? this.state.transactionData : this.state.fromRecieve ? this.state.recieved_txn_Data : this.state.sent_txn_Data}
                            ItemSeparatorComponent={() => <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }) => this.renderItem(item)} />
                    }

                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />


                    <RBSheet
                        ref={ref => {
                            this.RBSheetAlert = ref;
                        }}
                        height={320}
                        duration={0}>
                        {this.renderTxnDetails()}
                    </RBSheet>

                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.onTopupWallet()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Topup Wallet</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        justifyContent: 'center'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_40, width: metrics.dimen_40, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '100%', alignSelf: 'center', marginTop: metrics.dimen_10 },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

})
