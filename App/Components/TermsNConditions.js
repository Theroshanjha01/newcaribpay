import React, { Component } from 'react';
import {
    SafeAreaView, StyleSheet, Text, View, ScrollView, BackHandler, Image, TouchableOpacity, Dimensions
} from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';
const axios = require('axios');
import HTML from "react-native-render-html";


export default class TermsNConditions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: this.props.navigation.state.params.token,
            fromPrivacy: this.props.navigation.state.params.fromPrivacy,
            htmlContent: ''
        }
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let postData = { url: this.state.fromPrivacy == 0 ? "terms-condition" : "privacy-policy" }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/pages',
            data: postData,
            headers: { 'Authorization': this.state.token },
        })
            .then((response) => {
                console.log("TNC response -- > ", response.data)
                this.setState({
                    htmlContent: response.data.success.data.content
                })
            }).catch((err) => {
                alert(err)
            });

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    {this.state.fromPrivacy == 0 ? <Text style={styles.headertextStyle}>{"Terms & Conditions"}</Text> : <Text style={styles.headertextStyle}>{"Privacy Notice"}</Text>}
                </View>

                <ScrollView style={{ flex: 1, margin: 10 }}>
                    <HTML
                        html={this.state.htmlContent}
                        imagesMaxWidth={Dimensions.get("window").width}/>
                </ScrollView>


            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: metrics.dimen_60,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_15,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        // marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        // marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', textAlign: 'center'
    }
});
