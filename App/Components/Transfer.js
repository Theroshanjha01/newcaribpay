import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';

export default class Transfer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <Text style={styles.headertextStyle}>Transfer</Text>
                </View>

                <View style={styles.absouluteview}>

                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('SendMoney', {
                        fromMobilNo: true,
                        fromEmail: false,
                        email:''
                    })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/send1.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Send Money</Text>
                                <Text style={styles.descriiption}>Instant money transfer to someone</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity> */}


                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SendMoney', {
                        fromMobilNo: true,
                        fromEmail: false,
                        email: ''
                    })}>
                        <View style={styles.cellContainer}>
                            <View style={styles.shellContainer}>
                            <View style={{ flexDirection: 'row',justifyContent:'center',alignSelf:'center'}}>
                                    <View style={{height:50,width:50 , borderRadius:25 , justifyContent:'center',backgroundColor:colors.light_grey_backgroud ,marginLeft: 10,alignSelf:'center'}}>
                                    <Image style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center',marginStart:5}} source={require('../Images/send1.png')}></Image>
                                    </View>
                                    <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 20 }}>
                                        <Text style={styles.headingg}>Send Money</Text>
                                        <Text style={styles.descriiption}>Instant money transfer to someone</Text>
                                    </View>
                                </View>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('RequestMoney')}>
                        <View style={styles.cellContainer}>
                            <View style={styles.shellContainer}>
                                <View style={{ flexDirection: 'row',justifyContent:'center',alignSelf:'center'}}>
                                    <View style={{height:50,width:50 , borderRadius:25 , justifyContent:'center',backgroundColor:colors.light_grey_backgroud ,marginLeft: 10,alignSelf:'center'}}>
                                    <Image style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center',marginStart:5}} source={require('../Images/request1.png')}></Image>
                                    </View>
                                    <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 20 }}>
                                        <Text style={styles.headingg}>Request Money</Text>
                                        <Text style={styles.descriiption}>Request someone for payment</Text>
                                    </View>
                                </View>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>

                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('RequestMoney')}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/request1.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Request Money</Text>
                                <Text style={styles.descriiption}>Request someone for payment</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} /> */}


                    {/* <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/payout1.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                            <Text style={styles.headingg}>Payout</Text>
                            <Text style={styles.descriiption}>Withdrawal Money to Account</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                        </View>
                    </View>
                    <View style={styles.horizontalLine} /> */}



                    {/* <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/order.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                            <Text style={styles.headingg}>Beneficiary List</Text>
                            <Text style={styles.descriiption}>Maintain your Beneficiary List</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                        </View>
                    </View>
                    <View style={styles.horizontalLine} /> */}
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        justifyContent: 'center'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_40, width: metrics.dimen_40, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black,fontWeight:'bold',marginBottom:5 },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain', marginRight: 20 },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    cellContainer: {
        width: '100%',
        flexDirection: 'column',
        paddingLeft: 20,
        paddingRight: 20
    },
    shellContainer: {
        flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between', marginTop: 10,
        borderWidth: 1,
        borderRadius: 7,
        borderColor: '#ddd',
        borderBottomWidth: 1,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
        elevation: 1,
    }
})
