import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, BackHandler, FlatList } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
const axios = require('axios');
import Modal from 'react-native-modal';
var Spinner = require('react-native-spinkit');
const moment = require('moment');


export default class Tickets extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            Alltickets: []
        }
    }

    componentDidMount() {
        console.log(this.state.user_id, this.state.token)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.getAllTickets();

        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.getAllTickets();
            });
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove()
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    getAllTickets = () => {
        this.setState({
            spinvisible: true
        })

        let postData = {
            user_id: this.state.user_id,
        }

        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/list-support-ticket',
            headers: { 'Authorization': this.state.token },
            data: postData
        }).then((response) => {
            this.setState({
                spinvisible: false,
                Alltickets: response.data.data.tickets
            })
        }).catch((err) => {
            console.log("ticket add response error   --- > ", err)
        })
    }

    renderItem = (item) => {
        let txn_date = moment(item.created_at).format('DD MMMM YYYY , hh:mm A');
        return (
            <View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("TicketDetails",{ticket_id:item.id , user_id:this.state.user_id})}>
                    <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/theater.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.descriiption}>{item.code}</Text>
                                <View style={{ borderRadius: 5, justifyContent: 'center', backgroundColor: item.priority == "High" ? "red" : item.priority == "Normal" ? colors.app_yellow_color : item.priority == "Low" ? colors.carib_pay_blue : transparent, marginLeft: 10 }}>
                                    <Text style={{ color: colors.white, fontSize: metrics.text_small, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center', paddingVertical: 3, paddingHorizontal: 8 }}>{item.priority}</Text>
                                </View>
                            </View>
                            <Text style={{ fontSize: metrics.text_normal }}>{"Status : "} <Text style={{ fontSize: metrics.text_normal, marginLeft: 10, color: colors.theme_ornage_color }}>{item.ticket_status.name}</Text>  </Text>

                            <Text style={styles.headingg}>{txn_date}</Text>
                            <Text style={styles.headingg}>{item.subject}</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={styles.horizontalLine} />
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Inbox</Text>
                </View>
                <View style={styles.absouluteview}>

                    {this.state.spinvisible == false && this.state.Alltickets.length > 0 &&
                        <View>
                            <FlatList
                                style={{ marginTop: metrics.dimen_10 }}
                                data={this.state.Alltickets}
                                renderItem={({ item, index }) => this.renderItem(item)}
                                keyExtractor={(item, index) => item + index}
                            />
                        </View>
                    }
                    {this.state.spinvisible == false && this.state.Alltickets.length == 0 &&
                        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 250, height: 150, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require("../Images/theater.png")}></Image>
                            <Text style={{ fontSize: metrics.text_21, color: colors.theme_caribpay, textAlign: 'center', alignSelf: 'center' }}>No Ticket Found!</Text>
                        </View>
                    }



                    <View style={styles.floatingView}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateTickets', { user_id: this.state.user_id, token: this.state.token })}>
                            <Image style={{ height: 20, width: 20, alignSelf: 'center', resizeMode: 'contain', tintColor: colors.white }}
                                source={require('../Images/plus.png')}>
                            </Image>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible} out>
                            <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                                <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                            </View>
                        </Modal>
                    </View>

                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f2f2f2', borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    floatingView: { bottom: metrics.dimen_180, width: metrics.dimen_50, height: metrics.dimen_50, borderRadius: metrics.dimen_50 / 2, backgroundColor: colors.carib_pay_blue, position: 'absolute', right: 20, justifyContent: 'center' },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_40, width: metrics.dimen_40, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },

})
