import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { Input } from 'react-native-elements';
import RadioForm from 'react-native-simple-radio-button';
import Modal from 'react-native-modal';
var Spinner = require('react-native-spinkit');
import Toast, { DURATION } from 'react-native-easy-toast';
const axios = require('axios');
var radio_props = [
    { label: 'High', value: "High" },
    { label: 'Normal', value: "Normal" },
    { label: 'Low', value: "Low" }
];




export default class Tickets extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            subject: '',
            description: '',
            isVisible: false,
            value: '',
            spinvisible: false,
            message: ''
        }
    }

    componentDidMount() {
        console.log(this.state.user_id, this.state.token)

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    onSubmit = () => {
        if (this.state.subject == "") {
            this.refs.toast.show('Please Enter the Subject!')
        } else if (this.state.description == "") {
            this.refs.toast.show('Required Description, to assit you easily!')
        } else if (this.state.value == "") {
            this.refs.toast.show('Set Priority!')
        } else {
            this.setState({
                spinvisible: true
            })
            let postData = {
                user_id: this.state.user_id,
                subject: this.state.subject,
                description: this.state.description,
                priority: this.state.value,
            }
            console.log("postdata--> ", postData)
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/new-support-ticket',
                headers: { 'Authorization': this.state.token },
                data: postData
            }).then((response) => {
                if (response.data.status == 200) {
                    this.setState({
                        spinvisible: false,
                        message: response.data.message,
                        successmodal: true, subject: '', description: '', value: ''
                    })
                }
            }).catch((err) => {
                console.log("ticket add response error   --- > ", err)

            })
        }
    }




    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>New Message</Text>
                </View>
                <View style={styles.absouluteview}>

                    <View style={{ flexDirection: 'column', margin: metrics.dimen_20 }}>


                        <Text style={{ textAlign: 'center', color: colors.carib_pay_blue, fontWeight: '400', fontSize: metrics.text_16 }}>New Message for Support</Text>


                        <Input
                            placeholder={'Subject'}
                            containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10 }}
                            value={this.state.subject}
                            rightIcon={() => <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require('../Images/text.png')}></Image>}
                            inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                            inputStyle={{ fontSize: metrics.text_description }}
                            onChangeText={(text) => this.setState({ subject: text })}>
                        </Input>


                        <Input
                            placeholder={'Description'}
                            containerStyle={{ height: metrics.dimen_50, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10 }}
                            value={this.state.description}
                            rightIcon={() => <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain', alignSelf: 'center' }}
                                source={require('../Images/text.png')}></Image>}
                            inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                            inputStyle={{ fontSize: metrics.text_description }}
                            onChangeText={(text) => this.setState({ description: text })}>
                        </Input>



                        <TouchableOpacity onPress={() => this.setState({ isVisible: true })}>
                            <View style={styles.boxContainer}>
                                <Text style={{ fontSize: metrics.description, padding: 12, color: colors.app_gray }}>Priority</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: colors.app_gray, fontSize: metrics.text_description, padding: 10, alignSelf: 'center' }}>{this.state.value}</Text>
                                    <Image style={{ height: metrics.dimen_18, width: metrics.dimen_18, alignSelf: 'center', marginRight: metrics.dimen_20, tintColor: colors.app_gray }}
                                        source={require('../Images/dropdown.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>


                    </View>
                    <Modal style={{ alignSelf: 'center', width: "80%" }} isVisible={this.state.isVisible}>
                        <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7 }}>
                            <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                                <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Select Priority</Text>
                            </View>
                            <View style={{ flexDirection: 'column' }}>
                                <View style={{ margin: 20, marginLeft: metrics.dimen_30 }}>
                                    <RadioForm
                                        radio_props={radio_props}
                                        initial={-1}
                                        buttonColor={colors.carib_pay_blue}
                                        buttonSize={metrics.dimen_20}
                                        onPress={(value) => { this.setState({ value: value }) }} /></View>
                                <View style={styles.horizontalLine}></View>
                            </View>

                            <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                    <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                        <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({ isVisible: false })}>
                                    <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                        <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </Modal>

                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible} out>
                            <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                                <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                            </View>
                        </Modal>
                    </View>

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.successmodal} >
                            <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                    <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"CaribPay"}</Text>
                                </View>
                                <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                    <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{this.state.message}</Text>
                                    <TouchableOpacity onPress={() => this.setState({
                                        successmodal: false
                                    }, () => this.props.navigation.goBack(null))}>
                                        <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"View"}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>




                </View>
                <View style={{ ...styles.bottomview, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => this.onSubmit()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', height: metrics.dimen_50, alignSelf: 'center', width: '100%', borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10 },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }
})
