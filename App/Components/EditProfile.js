import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Dimensions, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
import RadioForm from 'react-native-simple-radio-button';
const axios = require('axios');
import ImgToBase64 from 'react-native-image-base64';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import RBSheet from "react-native-raw-bottom-sheet";
import { Input } from 'react-native-elements';
var Spinner = require('react-native-spinkit');


export default class EditProfile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            userdata: this.props.navigation.state.params.item,
            userProfileResponse: '',
            visible: false, Password: '',
            allDataWipes: false,
            openModal: false,
            profile_dp: '',
            photo_uri: '', userWallets: '',
            wallet_id: '',
            onchangeTextFrom: false,
            onchangeTextFrom2: false,
            onchangeTextFrom3: false,
            onchangeTextFrom4: false,
            onchangeTextFromPhone: false,
            fromDefaultWalletUpdate: false,
            address: '',
            city: '',
            state: '', country: '',
            phone: '',
            fromupdateAddress: false,
            fromupdateMobile: false,
            openWallet: false,
            multipleCurrency: [],
            defaultWallet: '',
            selectedCurrency: '',
            change_wallet_id: ''


        }
    }

    componentDidMount() {
        this.getUserProfile()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    getUserProfile() {
        this.setState({
            visible: true
        })
        let postData = { user_id: this.state.userdata.user_id }
        console.log(postData, this.state.userdata.token)
        axios.get('https://sandbox.caribpayintl.com/api/get-user-profile', { params: postData, headers: { 'Authorization': this.state.userdata.token } }).then((response) => {
            console.log("getuser profile ---", response.data.success.wallets)
            let wallet_data = response.data.success.wallets
            let walletId
            let multiplecurrency = [];
            let i;
            let wallet_currency;
            if (wallet_data.length == 1) {
                walletId = response.data.success.wallets[0].id
                wallet_currency = response.data.success.wallets[0].currencyCode
                this.saveUserWallet_id(walletId)
                let array = wallet_data
                console.log(array.length)
                for (i = 0; i < array.length; i++) {
                    let name = array[i].currencyCode
                    var add_crncy = { value: name, label: name }
                    multiplecurrency.push(add_crncy)
                }
                console.log("single wallet --->", multiplecurrency)
            } else {
                let wallet_data = response.data.success.wallets
                const multiplewalletData = wallet_data.filter((roshan) => roshan.is_default == "Yes")
                walletId = multiplewalletData[0].id
                wallet_currency = multiplewalletData[0].currencyCode
                this.saveUserWallet_id(walletId)
                let array = wallet_data
                console.log(array.length)
                for (i = 0; i < array.length; i++) {
                    let name = array[i].currencyCode
                    var add_crncy = { value: name, label: name }
                    multiplecurrency.push(add_crncy)
                }
                console.log("multiple wallet ---> ", multiplecurrency)
            }
            this.setState({
                userProfileResponse: response.data.success.user,
                userWallets: response.data.success.wallets,
                wallet_id: walletId,
                multipleCurrency: multiplecurrency,
                defaultWallet: wallet_currency,
                radioPosition: wallet_data[0].is_default == "Yes" ? 0 : 1,
                visible: false
            })
            console.log("userprofile respone ---", this.state.userProfileResponse)
        }).catch((error) => {
            console.log(error);
        })
    }

    saveUserWallet_id = async (data) => {
        await AsyncStorage.setItem("getWalletid", JSON.stringify(data))
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    showImagePickerOpenCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    profile_dp: base64String,
                    photo_uri: image.path,
                }, () => this.setState({ fromupdateAddress: false, fromupdateMobile: false }, () => AsyncStorage.setItem("base64", this.state.profile_dp).then((x) => {
                    this.updateProfileImage()
                }))
                )).catch(err => console.log(err));
        });
    }

    showImagePickerGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            ImgToBase64.getBase64String(image.path)
                .then(base64String => this.setState({
                    profile_dp: base64String,
                    photo_uri: image.path,
                }, () => this.setState({ fromupdateAddress: false, fromupdateMobile: false }, () => AsyncStorage.setItem("base64", this.state.profile_dp).then((x) => {
                    this.updateProfileImage()
                }))
                )).catch(err => console.log(err));
        });
    }

    onupdate = () => {
        this.setState({
            fromupdateAddress: true
        }, () => this.updateProfileImage())
        this.RBSheetUpdate.close();
    }

    onupdatePhone = () => {
        this.setState({
            fromupdateMobile: true
        }, () => this.updateProfileImage())
        this.RBSheetUpdatePhoneNumber.close();
    }

    onchangeCurrencyType = async () => {
        let data = this.state.userWallets
        const filterData = data.filter(roshan => roshan.currencyCode == this.state.selectedCurrency)
        console.log("filtered result on cureency change", filterData)
        let currency_symbol = filterData[0].currencyCode
        let id = filterData[0].id
        await AsyncStorage.setItem('symbol', currency_symbol)
        await AsyncStorage.setItem('selectedCurrency', this.state.selectedCurrency)
        if (this.state.selectedCurrency != "") {
            this.setState({
                radioPosition: filterData[0].is_default == "Yes" ? 1 : 0,
                defaultWallet: currency_symbol,
                fromDefaultWalletUpdate: true,
                change_wallet_id: id
            })
          this.updateProfileImage()  
        }
    }

    renderUpdatePhone = () => {
        return (
            <View style={{ flex: 1 }}>
                <View>
                    <View style={{ height: 60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                        <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', paddingHorizontal: 20, textAlign: 'center' }}>Update Mobile No.</Text>
                    </View>
                    <View style={{ margin: metrics.dimen_15, justifyContent: 'center', flexDirection: 'column', margin: 20 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay, fontWeight: 'bold' }}>Mobile No.</Text>
                        <Input
                            placeholder={'Mobile No.'}
                            placeholderTextColor={colors.place_holder_color}
                            containerStyle={{ width: "100%", height: 40 }}
                            value={this.state.onchangeTextFromPhone ? this.state.phone : this.state.userProfileResponse.phone}
                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                            inputStyle={{ fontSize: 14 }}
                            onChangeText={(text) => this.setState({ phone: text, onchangeTextFromPhone: true })}>
                        </Input>

                        <View style={{ height: 50, margin: 20, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10, width: 100, backgroundColor: colors.theme_caribpay }}>
                            <TouchableOpacity onPress={() => this.onupdatePhone()}>
                                <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Update</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }


    renderRequestMoney = () => {
        return (
            <View style={{ flex: 1 }}>
                <View>
                    <View style={{ height: 60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                        <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', paddingHorizontal: 20, textAlign: 'center' }}>Update Address</Text>
                    </View>
                    <View style={{ margin: metrics.dimen_15, justifyContent: 'center', flexDirection: 'column', margin: 20 }}>

                        <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay, fontWeight: 'bold' }}>Address</Text>
                        <Input
                            placeholder={'Address'}
                            placeholderTextColor={colors.place_holder_color}
                            containerStyle={{ width: "90%", height: 40 }}
                            value={this.state.onchangeTextFrom ? this.state.address : this.state.userProfileResponse.address_1}
                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                            inputStyle={{ fontSize: 14 }}
                            onChangeText={(text) => this.setState({ address: text, onchangeTextFrom: true })}>
                        </Input>

                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ flexDirection: 'column', flex: 0.5 }}>
                                <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay, fontWeight: 'bold', marginTop: 5 }}>City</Text>
                                <Input
                                    placeholder={'City'}
                                    placeholderTextColor={colors.place_holder_color}
                                    containerStyle={{ width: "90%", height: 40 }}
                                    value={this.state.onchangeTextFrom2 ? this.state.city : this.state.userProfileResponse.city}
                                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                                    inputStyle={{ fontSize: 14 }}
                                    onChangeText={(text) => this.setState({ city: text, onchangeTextFrom2: true })}>
                                </Input>
                            </View>

                            <View style={{ flexDirection: 'column', flex: 0.5 }}>
                                <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay, fontWeight: 'bold', marginTop: 5 }}>State</Text>
                                <Input
                                    placeholder={'State'}
                                    placeholderTextColor={colors.place_holder_color}
                                    containerStyle={{ width: "80%", height: 40 }}
                                    value={this.state.onchangeTextFrom3 ? this.state.state : this.state.userProfileResponse.state}
                                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                                    inputStyle={{ fontSize: 14 }}
                                    onChangeText={(text) => this.setState({ state: text, onchangeTextFrom3: true })}>
                                </Input>
                            </View>
                        </View>



                        <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay, fontWeight: 'bold', marginTop: 10 }}>Country</Text>
                        <Input
                            placeholder={'Country'}
                            placeholderTextColor={colors.place_holder_color}
                            containerStyle={{ width: "90%", height: 40 }}
                            value={this.state.onchangeTextFrom4 ? this.state.country : this.state.userProfileResponse.defaultCountry == null ? " " : this.state.userProfileResponse.defaultCountry}
                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                            inputStyle={{ fontSize: 14 }}
                            onChangeText={(text) => this.setState({ country: text, onchangeTextFrom4: true })}>
                        </Input>

                    </View>
                </View>
                <View style={{ ...styles.bottomview, backgroundColor: colors.theme_caribpay }}>
                    <TouchableOpacity onPress={() => this.onupdate()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Update</Text>
                    </TouchableOpacity>
                </View>


            </View>
        )
    }

    updateProfileImage = async () => {
        let base64 = await AsyncStorage.getItem('base64')
        var s1 = this.state.userProfileResponse.carrierCode
        var country_code = s1.substr(1);
        var walletsave_id
        if (this.state.wallet_id == "") {
            walletsave_id = await AsyncStorage.getItem('getWalletid')
            console.log('save wallet id is --->', walletsave_id)
        }

        let postDataforPhone = {
            user_id: this.state.userdata.user_id,
            first_name: this.state.userProfileResponse.first_name,
            last_name: this.state.userProfileResponse.last_name,
            email: this.state.userProfileResponse.email,
            phone: this.state.onchangeTextFromPhone ? this.state.phone : this.state.userProfileResponse.phone,
            user_defaultCountry: this.state.userProfileResponse.defaultCountry,
            user_carrierCode: this.state.userProfileResponse.carrierCode,
            formattedPhone: this.state.userProfileResponse.formattedPhone,
            picture: "",
            country: country_code,
            address: this.state.userProfileResponse.address_1,
            city: this.state.userProfileResponse.city,
            state: this.state.userProfileResponse.state,
            timezone: this.state.userProfileResponse.timezone,
            defaultWallet: this.state.wallet_id
        }

        let postData = {
            user_id: this.state.userdata.user_id,
            first_name: this.state.userProfileResponse.first_name,
            last_name: this.state.userProfileResponse.last_name,
            email: this.state.userProfileResponse.email,
            phone: this.state.userProfileResponse.phone,
            user_defaultCountry: this.state.userProfileResponse.defaultCountry,
            user_carrierCode: this.state.userProfileResponse.carrierCode,
            formattedPhone: this.state.userProfileResponse.formattedPhone,
            picture: this.state.profile_dp,
            country: country_code,
            address: this.state.userProfileResponse.address_1,
            city: this.state.userProfileResponse.city,
            state: this.state.userProfileResponse.state,
            timezone: this.state.userProfileResponse.timezone,
            defaultWallet: this.state.wallet_id
        }

        let postdataforupdateAddress = {
            user_id: this.state.userdata.user_id,
            first_name: this.state.userProfileResponse.first_name,
            last_name: this.state.userProfileResponse.last_name,
            email: this.state.userProfileResponse.email,
            phone: this.state.userProfileResponse.phone,
            user_carrierCode: this.state.userProfileResponse.carrierCode,
            formattedPhone: this.state.userProfileResponse.formattedPhone,
            picture: '',
            country: country_code,
            address: this.state.onchangeTextFrom ? this.state.address : this.state.userProfileResponse.address_1,
            city: this.state.onchangeTextFrom2 ? this.state.city : this.state.userProfileResponse.city,
            state: this.state.onchangeTextFrom3 ? this.state.state : this.state.userProfileResponse.state,
            user_defaultCountry: this.state.onchangeTextFrom4 ? this.state.country : this.state.userProfileResponse.defaultCountry,
            timezone: this.state.userProfileResponse.timezone,
            defaultWallet: this.state.wallet_id
        }


        let postdataforsetdefaultWallet = {
            user_id: this.state.userdata.user_id,
            first_name: this.state.userProfileResponse.first_name,
            last_name: this.state.userProfileResponse.last_name,
            email: this.state.userProfileResponse.email,
            phone: this.state.userProfileResponse.phone,
            user_carrierCode: this.state.userProfileResponse.carrierCode,
            formattedPhone: this.state.userProfileResponse.formattedPhone,
            picture: "",
            country: country_code,
            address: this.state.onchangeTextFrom ? this.state.address : this.state.userProfileResponse.address_1,
            city: this.state.onchangeTextFrom2 ? this.state.city : this.state.userProfileResponse.city,
            state: this.state.onchangeTextFrom3 ? this.state.state : this.state.userProfileResponse.state,
            user_defaultCountry: this.state.onchangeTextFrom4 ? this.state.country : this.state.userProfileResponse.defaultCountry,
            timezone: this.state.userProfileResponse.timezone,
            defaultWallet: this.state.fromDefaultWalletUpdate ? this.state.change_wallet_id : this.state.wallet_id
        }

        console.log("post Data Upload Image --->  ", postData)
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/update-user-profile',
            headers: { 'Authorization': this.state.userdata.token },
            data: this.state.fromupdateAddress ? postdataforupdateAddress : this.state.fromupdateMobile ? postDataforPhone : this.state.fromDefaultWalletUpdate ? postdataforsetdefaultWallet : postData
        }).then((response) => {
            if (response.data.status == 200) {
                this.getUserProfile();
            }
        }).catch((err) => {
            console.log("update profile error   --- > ", err)
        })
    }

    render() {
        let password = this.state.Password
        let encrypted_password = password.replace(/./g, '*');
        return (
            <SafeAreaView style={{ flex: 1 }} >

                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ margin: metrics.dimen_20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>

                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>Profile</Text>

                        <TouchableOpacity onPress={() => this.setState({ openModal: true })}>
                            {this.state.userProfileResponse.picture == "" && <Image style={styles.imageStyle2} source={require('../Images/user.png')}></Image>}
                            {this.state.userProfileResponse.picture != "" && <Image style={styles.imageStyle2} source={{ uri: this.state.userProfileResponse.picture }}></Image>}
                            {this.state.userProfileResponse.picture != "" && <Image style={{ height: 30, width: 30, resizeMode: 'contain', position: 'absolute', top: 70, marginLeft: 60 }} source={require('../Images/addphoto.png')}></Image>}
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.curveview}>
                    <View style={{ margin: metrics.dimen_10 }}>

                        <TouchableOpacity onPress={() => this.state.userWallets.length > 1 ? this.setState({
                            openWallet: true
                        }) : null}>
                            <View style={styles.container}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.image_style} source={require('../Images/wallet.png')}></Image>
                                </View>
                                <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                    <Text style={styles.headingg}>Default Wallet</Text>
                                    <Text style={{ ...styles.descriiption, color: colors.theme_caribpay, fontWeight: 'bold' }}>{this.state.defaultWallet}</Text>
                                </View>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.horizontalLine} />

                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/mail.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Email Address</Text>
                                <Text style={styles.descriiption}>{this.state.userProfileResponse.email}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                        <View style={styles.horizontalLine} />





                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/user.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>First Name</Text>
                                <Text style={styles.descriiption}>{this.state.userProfileResponse.first_name}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                        <View style={styles.horizontalLine} />


                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/user.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Last Name</Text>
                                <Text style={styles.descriiption}>{this.state.userProfileResponse.last_name}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                        <View style={styles.horizontalLine} />



                        {/* <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/key.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Password</Text>
                                <Text style={styles.descriiption}>{encrypted_password}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View> */}
                        <View style={styles.horizontalLine} />


                        <TouchableOpacity onPress={() => this.RBSheetUpdatePhoneNumber.open()}>
                            <View style={styles.container}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.image_style} source={require('../Images/phone.png')}></Image>
                                </View>
                                <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                    <Text style={styles.headingg}>Mobile No.</Text>
                                    <Text style={styles.descriiption}>{this.state.userProfileResponse.phone}</Text>
                                </View>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.horizontalLine} />



                        {/* <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                            <Spinner style={{ marginTop: 100 }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                        </View> */}


                        <TouchableOpacity onPress={() => this.RBSheetUpdate.open()}>
                            <View style={styles.container}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.image_style} source={require('../Images/way.png')}></Image>
                                </View>
                                <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                    <Text style={styles.headingg}>Address</Text>
                                    <Text style={styles.descriiption}>{this.state.userProfileResponse.address_1}</Text>
                                </View>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.horizontalLine} />

                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/calendar.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Birthday</Text>
                                <Text style={styles.descriiption}>{"10 December 1995"}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                        <View style={styles.horizontalLine} />



                        {/* <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/camera.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                                <Text style={styles.headingg}>Photo</Text>
                                <Text style={styles.descriiption}>Change your Profile Photo</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                        <View style={styles.horizontalLine} /> */}


                        <Modal style={{ width: metrics.dimen_300, alignSelf: 'center' }} isVisible={this.state.openModal}>
                            <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7, height: 240 }}>
                                <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Take Photo</Text>
                                </View>

                                <View style={{ margin: 10, flex: 1 }}>
                                    <TouchableOpacity onPress={() => this.setState({ openModal: false }, () => setTimeout(() => {
                                        this.showImagePickerOpenCamera()
                                    }, 500))}>
                                        <View style={{ flexDirection: 'row', height: 40, margin: 5 }}>
                                            <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/camera.png')}></Image>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.theme_caribpay, fontWeight: '900', textAlignVertical: 'center' }}>Camera</Text>
                                        </View>
                                    </TouchableOpacity>


                                    <View style={styles.horizontalLine} />


                                    <TouchableOpacity onPress={() => this.setState({ openModal: false }, () => setTimeout(() => {
                                        this.showImagePickerGallery()
                                    }, 500))}>
                                        <View style={{ flexDirection: 'row', height: 40, margin: 5 }}>
                                            <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/gallery.png')}></Image>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.theme_caribpay, fontWeight: '900', textAlignVertical: 'center' }}>Gallery</Text>
                                        </View>
                                    </TouchableOpacity>

                                    <View style={{ margin: metrics.dimen_10, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.setState({ openModal: false })}>
                                            <View style={{ backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                                <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>Cancel</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>


                                </View>
                            </View>
                        </Modal>




                        <Modal style={{ width: metrics.dimen_300, alignSelf: 'center' }} isVisible={this.state.openWallet}>
                            <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_7 }}>
                                <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Select Default Wallet</Text>
                                </View>
                                <View style={{ flexDirection: 'column' }}>
                                    <View style={{ margin: 20, marginLeft: metrics.dimen_30 }}>
                                        <RadioForm
                                            radio_props={this.state.multipleCurrency}
                                            initial={this.state.radioPosition}
                                            buttonColor={colors.carib_pay_blue}
                                            buttonSize={metrics.dimen_20}
                                            onPress={(value) => { this.setState({ selectedCurrency: value }) }} /></View>
                                    <View style={styles.horizontalLine}></View>
                                </View>

                                <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.setState({ openWallet: false })}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>CANCEL</Text>
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.setState({ openWallet: false }, () => this.onchangeCurrencyType())}>
                                        <View style={{ width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, marginLeft: 10, justifyContent: 'center' }}>
                                            <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>


                        <View>
                            <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.visible} out>
                                <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                                    <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                                </View>
                            </Modal>
                        </View>


                        <RBSheet
                            ref={ref => {
                                this.RBSheetUpdate = ref;
                            }}
                            height={420}
                            duration={0}>
                            {this.renderRequestMoney()}
                        </RBSheet>

                        <RBSheet
                            ref={ref => {
                                this.RBSheetUpdatePhoneNumber = ref;
                            }}
                            height={240}
                            duration={0}>
                            {this.renderUpdatePhone()}
                        </RBSheet>
                    </View>
                </View>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    imageStyle: {
        height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain', marginTop: 2
    },
    imageStyle2: {
        height: 90, width: 90, marginTop: 10, borderRadius: 45
    },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop + 40, width: "100%", backgroundColor: colors.white, borderRadius: 40 }
})