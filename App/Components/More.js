import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, Linking } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
const axios = require('axios');
import Modal from 'react-native-modal';


export default class More extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: '', isVisible: false, KycStatus: '',
            invalid: false,
            pendingTrue: false
        }
    }

    async componentDidMount() {
        let fname = await AsyncStorage.getItem('first_name')
        let lname = await AsyncStorage.getItem('last_name');
        let carrierCode = await AsyncStorage.getItem('carrierCode');
        let email = await AsyncStorage.getItem('email');
        let formattedPhone = await AsyncStorage.getItem('formattedPhone');
        let password = await AsyncStorage.getItem('password');
        let phone = await AsyncStorage.getItem('phone');
        let type = await AsyncStorage.getItem('type');
        let user_id = await AsyncStorage.getItem('id');
        let token = await AsyncStorage.getItem('token');

        const user_login_data = {
            token: token,
            fname: fname, lname: lname, carrierCode: carrierCode,
            email: email, formattedPhone: formattedPhone, password: password,
            phone: phone, type: type, user_id: user_id
        }
        this.setState({
            data: user_login_data
        })

        let postData = { user_id: this.state.data.user_id }
        axios.get('https://sandbox.caribpayintl.com/api/get-kyc-status', { params: postData, headers: { 'Authorization': this.state.data.token } }).then((response) => {
            if (response.data.status == 200) {
                let kyc_status = response.data.success.status
                const KycStatus = kyc_status.toUpperCase();
                // if (kyc_status == "Not Uploaded") {
                this.setState({
                    isVisible: true,
                    KycStatus: KycStatus
                })

            }
        })

        this.knowKycStatus();
    }


    componentWillUnmount() {
        this.willFocusSubscription.remove()
    }

    knowKycStatus = () => {
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                let postData = { user_id: this.state.data.user_id }
                axios.get('https://sandbox.caribpayintl.com/api/get-kyc-status', { params: postData, headers: { 'Authorization': this.state.data.token } }).then((response) => {
                    if (response.data.status == 200) {
                        let kyc_status = response.data.success.status
                        const KycStatus = kyc_status.toUpperCase();
                        // if (kyc_status == "Not Uploaded") {
                        this.setState({
                            isVisible: true,
                            KycStatus: KycStatus
                        })
                    }
                })
            });
    }

    async onLogout() {
        await AsyncStorage.clear();
        this.props.navigation.navigate("Login"); // if using react-navigation
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <Text style={styles.headertextStyle}>More</Text>
                </View>

                <View style={styles.absouluteview}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('EditProfile', { item: this.state.data })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/user.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>Profile</Text>
                                {/* <Text style={styles.descriiption}>Edit User Profile</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.horizontalLine} />




                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Faqs')}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/question.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>FAQ</Text>
                                {/* <Text style={styles.descriiption}>Frequently Asked Questions</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />


                    <TouchableOpacity onPress={() => this.state.KycStatus == "APPROVED" ? this.setState({ invalid: true }) : this.state.KycStatus == "PENDING" ? this.setState({
                        pendingTrue: true
                    }) : this.props.navigation.navigate('AccountVerification', { user_id: this.state.data.user_id, fromSignup:false })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/kyc.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                                    <Text style={styles.headingg}>KYC</Text>
                                    {this.state.isVisible &&
                                        <View style={{ borderRadius: 5, justifyContent: 'center', backgroundColor: this.state.KycStatus == "NOT UPLOADED" ? "red" : this.state.KycStatus == "PENDING" ? colors.app_yellow_color : this.state.KycStatus == "APPROVED" ? colors.green : colors.carib_pay_blue, marginRight: 10 }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_small, fontWeight: 'bold', textAlign: 'center', textAlignVertical: 'center', paddingVertical: 3, paddingHorizontal: 8 }}>{this.state.KycStatus == "PENDING" ? "Processing" : this.state.KycStatus == "NOT UPLOADED" ? "Verify Now" : this.state.KycStatus}</Text>
                                        </View>}
                                </View>
                                {/* <Text style={styles.descriiption}>Process of verifying the identity</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />


                    <TouchableOpacity onPress={() => this.setState({
                        isComingSoon: true, fromFees: true
                    })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/fee.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>Fees and Charges</Text>
                                {/* <Text style={styles.descriiption}>Check transfer Fees and Charges</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.horizontalLine} />

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("CouponsVouchers")}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/gift.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>{"eCoupons & eVoucher"}</Text>
                                {/* <Text style={styles.descriiption}>Check transfer Fees and Charges</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />

                    <TouchableOpacity onPress={() =>
                        Linking.openURL('https://sandbox.caribpayintl.com/register').catch(err => console.error('An error occurred', err))}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/stair.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>{"Sign up as a CaribPay Merchant"}</Text>
                                {/* <Text style={styles.descriiption}>Check transfer Fees and Charges</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Tickets', { user_id: this.state.data.user_id, token: this.state.data.token })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/support_ticket.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>Have Question ,  We’are happy to help!</Text>
                                {/* <Text style={styles.descriiption}>Generate Ticket for Support</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />



                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Settings', { token: this.state.data.token })}>

                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/app.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>Settings</Text>
                                {/* <Text style={styles.descriiption}>Do App Setting</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={{ backgroundColor: '#D3D3D3', height: 20, width: '100%', alignSelf: 'center', marginTop: 10 }} />


                    <TouchableOpacity>
                        <View style={styles.container}>
                            {/* <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/support_ticket.png')}></Image>
                            </View> */}
                            <View style={{ flexDirection: 'column', flex: 1 }}>
                                <Text style={styles.headingg}>Membership</Text>
                                {/* <Text style={styles.descriiption}>Generate Ticket for Support</Text> */}
                            </View>
                            {/* <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View> */}
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Points")}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/coins.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>Point Balance</Text>
                                {/* <Text style={styles.descriiption}>Generate Ticket for Support</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Text style={{ ...styles.headingg, color: colors.app_yellow_color }}>100 Points</Text>
                                {/* <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image> */}
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />

                    <TouchableOpacity onPress={() => this.setState({
                        facilittyName: 'Membership Benefits', isComingSoon: true
                    })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/staff.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
                                <Text style={styles.headingg}>Membership Benefits</Text>
                                {/* <Text style={styles.descriiption}>Generate Ticket for Support</Text> */}
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />

                    <View style={{ width: '85%', justifyContent: 'center', alignSelf: 'center', backgroundColor: colors.carib_pay_blue, height: 40, borderRadius: metrics.dimen_10, marginTop: metrics.dimen_20 }}>
                        <TouchableOpacity onPress={() => this.setState({
                            AskLogout: true
                        })}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>LOG OUT</Text>
                        </TouchableOpacity>
                    </View>

                </View>


                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid}>
                        <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 250, justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                        source={require("../Images/logo.png")}></Image>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Approved KYC Verification</Text>
                                </View>
                                <View style={styles.bottomview2}>
                                    <TouchableOpacity onPress={() => this.setState({ invalid: false })}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>


                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.pendingTrue}>
                        <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 320, justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                        source={require("../Images/logo.png")}></Image>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Verification in process.</Text>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 15, fontWeight: 'bold', textAlign: 'center', width: 300, alignSelf: "center" }}>Your documents have been submitted. It will take up-to 2 working days to verify your application.</Text>
                                </View>
                                <View style={styles.bottomview2}>
                                    <TouchableOpacity onPress={() => this.setState({ pendingTrue: false })}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>




                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isComingSoon}>
                        <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 200, justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                        source={require("../Images/comingsoon.png")}></Image>
                                  {!this.state.fromFees ? <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center' }}>Membership Benefits</Text> :
                                  <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center' }}>{"Fees & Charges"}</Text>}
                                </View>
                                <View style={styles.bottomview2}>
                                    <TouchableOpacity onPress={() => this.setState({
                                        isComingSoon: false
                                    })}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>


                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.AskLogout}>
                        <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 300, justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                        source={require("../Images/logo.png")}></Image>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 10 }}>Confirm Logout</Text>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 10, fontWeight: 'bold', textAlign: 'center', width: 200, alignSelf: 'center' }}>Are you sure you want to log out of Carib Pay App?</Text>
                                </View>
                                <View style={styles.bottomview}>

                                    <View style={{ width: "45%", backgroundColor: colors.theme_caribpay, borderRadius: 10, justifyContent: 'center' }}>
                                        <TouchableOpacity onPress={() => this.setState({ AskLogout: false })}>
                                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Cancel</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ width: "45%", marginLeft: 10, backgroundColor: colors.theme_caribpay, borderRadius: 10, justifyContent: 'center' }}>
                                        <TouchableOpacity onPress={() => this.setState({ AskLogout: false }, () => this.onLogout())}>
                                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Log Out</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        justifyContent: 'center'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_12 },
    image_style: { height: metrics.dimen_20, width: metrics.dimen_20, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 20, position: 'absolute', color: colors.theme_caribpay, height: 50, justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10, flexDirection: 'row' },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },

})






{/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Support')}>

<View style={styles.container}>
    <View style={{ justifyContent: 'center' }}>

        <Image style={styles.image_style} source={require('../Images/support.png')}></Image>
    </View>
    <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_10 }}>
        <Text style={styles.headingg}>Have Question?</Text>
//         {/* <Text style={styles.descriiption}>We're happy to help!</Text> */}
//     </View>
//     <View style={{ justifyContent: 'center' }}>
//         <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
//     </View>
// </View>
// </TouchableOpacity>

// <View style={styles.horizontalLine} /> */}