import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, FlatList, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';
import { Input } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';
const axios = require('axios');
import RBSheet from "react-native-raw-bottom-sheet";
import Modal from 'react-native-modal';
import CountryData from 'country-data';


export default class MobileReload extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            currencycode: '',
            phone: '', countryDataSource: [],
            isoOperators: [],
            isoName: '',
            fromPostPaid: false,
            fromPrepaid: true,
            arrayholder: [],
            isComingSoon: false

        }
        console.disableYellowBox = true;
    }


    componentDidMount() {
        axios.get('https://topups.reloadly.com/countries').then((response) => {
            this.setState({
                countryDataSource: response.data,
            });
            this.state.arrayholder = response.data
        }).catch((err) => {
            console.warn(err)
        });

        axios.get('https://ipinfo.io/json').then((response) => {
            let country_code = response.data.country;
            let countrycallingcode = JSON.stringify(CountryData.countries[country_code].countryCallingCodes[0]);
            this.setState({
                currencycode: countrycallingcode.replace(/"/g, ""),
                isoName: country_code
            });
        }).catch((error) => {
            console.log(error);
        });


        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }

    getCodesByList = (item) => {
        this.setState({
            currencycode: item.callingCodes[0],
            isoName: item.isoName
        })
        this.RBSheetAlert.close();
        // this.getOperatorIds(item);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }




    // getOperatorIds = (item) => {
    //     let isoName = item.isoName
    //     let postData = { iso: isoName }
    //     axios({
    //         method: 'post',
    //         url: 'https://sandbox.caribpayintl.com/api/get-operators-by-iso',
    //         data: postData,
    //         headers: { 'Authorization': this.state.token },
    //     })
    //         .then((response) => {
    //             let arr = response.data.data.map((item, index) => {
    //                 item.isSelected = false
    //                 return { ...item };
    //             })
    //             this.setState({
    //                 isoOperators: arr
    //             });
    //         }).catch((err) => {
    //             alert(err)
    //         });
    // }


    renderHeader() {
        return (
            <View style={{
                backgroundColor: colors.light_grey_backgroud, height: 40, marginTop: metrics.dimen_20, flexDirection: 'row', justifyContent: 'space-between'
            }}>
                <Text style={{ fontSize: metrics.text_description, color: colors.heading_black_text, alignSelf: 'center', marginLeft: 10 }}>Select from Recents</Text>
                <Text style={{ fontSize: metrics.text_description, color: colors.carib_pay_blue, alignSelf: 'center', marginRight: 10 }}>View all</Text>
            </View >
        )
    }


    onProceedtoNext = () => {
        if (this.state.currencycode == "") {
            this.refs.toast.show("please select the country first")
        } else if (this.state.phone == "") {
            this.refs.toast.show("please enter your phone number")
        } else if (this.state.phone.length < 8) {
            this.refs.toast.show("Invalid Phone Number")
        } else {
            this.props.navigation.navigate('Topup', { user_id: this.state.user_id, token: this.state.token, phone: this.state.currencycode + this.state.phone, isoName: this.state.isoName })
        }
    }


    // selectionHandler = (indexNumber) => {
    //     let newrray = this.state.isoOperators.map((item, index) => {
    //         if (indexNumber == index) {
    //             item.isSelected = !item.isSelected
    //         }
    //         return { ...item }
    //     });
    //     this.setState({
    //         isoOperators: newrray
    //     });


    //     let selected_operators = this.state.isoOperators.filter((isselectCheck) => {
    //         return isselectCheck.isSelected == true;
    //     });
    //     selected_operators.forEach((option) => {
    //         if (option.isSelected) {
    //             this.setState({
    //                 selectedOperators: selected_operators,
    //                 selected_operator_name: option.name,
    //                 selected_operator_logo: option.logoUrls[0]
    //             })
    //         }
    //     });

    // }


    renderItemCountries = (item) => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.getCodesByList(item)} >
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                        <Image style={{ height: 30, width: 30, resizeMode: 'contain' }} source={{ uri: 'https://www.countryflags.io/' + item.isoName.toLocaleLowerCase() + '/flat/64.png' }}></Image>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, paddingHorizontal: 10 }}>{item.name}</Text>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_description }}>{" (" + item.callingCodes[0] + ")"}</Text>
                    </View>
                    <View style={{ borderBottomColor: colors.light_grey_backgroud, borderBottomWidth: 1 }} />
                </TouchableOpacity>
            </View>
        )
    }


    // renderOperatorsISO = (item, index) => {
    //     let iso_image_url = item.logoUrls[0]
    //     return (
    //         <TouchableOpacity onPress={() => this.selectionHandler(index)}>
    //             <View style={{ flexDirection: 'row', backgroundColor: colors.light_grey_backgroud, margin: 10 }}>
    //                 {item.isSelected &&
    //                     <CheckBox
    //                         checkedIcon={<Image source={require('../Images/check.png')}
    //                             style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: 2 }}></Image>}
    //                         containerStyle={{ backgroundColor: 'transparent', borderWidth: 0 }}
    //                         checked={true}
    //                     />}
    //                 <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 1 }}>
    //                     <Image style={{ width: 40, height: 40, resizeMode: 'contain', margin: 10 }} source={{ uri: iso_image_url }}></Image>
    //                     <Text style={{ fontSize: metrics.text_16, color: colors.heading_black_text, textAlignVertical: 'center', justifyContent: 'center', flex: 1.5 }}>{item.name}</Text>

    //                 </View>
    //             </View>
    //         </TouchableOpacity>
    //     )

    // }

    SearchFilterFunction(text) {
        const newData = this.state.arrayholder.filter(item => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ countryDataSource: newData, text: text });
    }

    renderCountries = () => {
        return (
            <View>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetAlert.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_16, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Select Country</Text>
                </View>
                <View style={{ marginTop: metrics.dimen_5, width: "95%", alignSelf: 'center' }}>
                    <Input
                        containerStyle={{ alignSelf: 'center', backgroundColor: '#DCDCDC', borderRadius: 10, height: 45 }}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        placeholder='Search'
                        placeholderTextColor="#9D9D9F"
                        inputStyle={{ color: colors.black, fontSize: metrics.text_description }}
                        value={this.state.text}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        leftIcon={<Image style={{ width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }} source={require('../Images/search.png')}></Image>} />
                </View>

                <FlatList
                    style={{ marginTop: metrics.dimen_7 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.countryDataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => this.renderItemCountries(item)}>
                </FlatList>
            </View>
        )
    }

    render() {
        let name = this.state.isoName
        var inshort = name.toLocaleLowerCase();
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Mobile Reload</Text>
                </View>
                <View style={styles.absouluteview}>

                    <View style={{ flex: 1 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_20 }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ color: '#323232', fontWeight: '500', fontSize: metrics.text_header }}>Mobile Top-up</Text>
                            </View>

                            <View style={{ height: metrics.dimen_30, width: metrics.dimen_30, borderRadius: metrics.dimen_30 / 2, backgroundColor: "#f2f2f2", justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_15, width: metrics.dimen_15, alignSelf: 'center' }}
                                    source={require('../Images/refresh.png')}></Image>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_30, alignSelf: 'center', width: '95%', margin: 2, borderRadius: metrics.dimen_10 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPrepaid ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromPostPaid: false, fromPrepaid: true, isComingSoon: false })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPrepaid ? colors.light_grey_backgroud : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromPrepaid ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Prepaid</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPostPaid ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromPostPaid: true, fromPrepaid: false, isComingSoon: true })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPostPaid ? colors.light_grey_backgroud : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromPostPaid ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Postpaid</Text>
                                </View>
                            </TouchableOpacity>
                        </View>


                        <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Input
                                    placeholder={'Code'}
                                    value={this.state.currencycode}
                                    onChangeText={(text) => this.setState({ currencycode: text })}
                                    leftIcon={
                                        <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                            {this.state.isoName == "" ?
                                                <Image style={{ height: 20, width: 20 }} source={require('../Images/globe.png')}>
                                                </Image> :
                                                <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://www.countryflags.io/' + inshort + '/flat/64.png' }}>
                                                </Image>
                                            }
                                        </TouchableOpacity>
                                    }
                                    containerStyle={{ width: "30%", height: 40 }}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: 13 }}>
                                </Input>

                                <Input
                                    placeholder={'Mobile No.'}
                                    containerStyle={{ width: "70%", height: 50 }}
                                    keyboardType="number-pad"
                                    value={this.state.phone}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: 13 }}
                                    onChangeText={(text) => this.setState({ phone: text })}>
                                </Input>
                            </View>
                            <View style={styles.horizontalLine}></View>
                            <Text style={{ color: '#323232', fontWeight: '500', fontSize: metrics.text_description, marginTop: 5, marginLeft: 5 }}>Select Country and Mobile Number</Text>
                        </View>

                        {/* {this.state.isoOperators.length > 0 &&
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={this.state.isoOperators.length > 4 ? this.state.isoOperators.slice(0, 4) : this.state.isoOperators}
                                keyExtractor={(item, index) => item.toString()}
                                ListHeaderComponent={this.renderHeader}
                                renderItem={({ item, index }) => this.renderOperatorsISO(item, index)}
                            />} */}

                    </View>

                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />



                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isComingSoon}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 200, justifyContent: 'center' }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                            source={require("../Images/comingsoon.png")}></Image>
                                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center' }}>Postpaid Services</Text>
                                    </View>
                                    <View style={styles.bottomview2}>
                                        <TouchableOpacity onPress={() => this.setState({
                                            isComingSoon: false, fromPostPaid: false, fromPrepaid: true
                                        })}>
                                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>

                    <RBSheet
                        ref={ref => {
                            this.RBSheetAlert = ref;
                        }}
                        height={Dimensions.get('screen').height}
                        duration={0}>
                        {this.renderCountries()}
                    </RBSheet>


                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.onProceedtoNext()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed to Top-up</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}




const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '95%', alignSelf: 'center' },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

})
