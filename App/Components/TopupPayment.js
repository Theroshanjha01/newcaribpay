import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Dimensions, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import { Input } from 'react-native-elements';
import colors from '../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
var Spinner = require('react-native-spinkit');
const axios = require('axios');
// import RNPaypal from 'react-native-paypal-lib';
import Stripe from 'react-native-stripe-api';
import { showAlert } from '../Utils.js';
import Modal from 'react-native-modal';


export default class TopupPayment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: this.props.navigation.state.params.amount,
            phone: this.props.navigation.state.params.phone,
            userId: this.props.navigation.state.params.userId,
            token: this.props.navigation.state.params.token,
            operatorName: this.props.navigation.state.params.operatorName,
            operatorID: this.props.navigation.state.params.operatorID,
            isoName: this.props.navigation.state.params.isoName,
            fromPayPal: false,
            fromWallet: true,
            fromCards: false,
            visible: false,
            isVisible: false,
            balance: '',
            RecipientPhone: '',
            txn_date: '',
            client_id: '',
            cvv: '',
            expiryMonth: '',
            expiryYear: '',
            cardNumber: '',
            stripeKey: '',
            updated_balance: '',
            tryAgain: false

        }

        console.disableYellowBox = true;
    }

    async componentDidMount() {
        console.log("id---->", this.state.userId)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let balance = await AsyncStorage.getItem('available_balance')
        let client_id = await AsyncStorage.getItem('clientIdPaypal');
        this.setState({
            balance: balance,
            client_id: client_id
        });

        let postData = { method_id: "2", currency_id: "1" }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/deposit/get-stripe-info',
            data: postData,
            headers: { 'Authorization': this.state.token },
        })
            .then((response) => {
                if (response.data.success.status == 200) {
                    this.setState({
                        stripeKey: response.data.success.stripe_keys.secret_key
                    })
                    console.log(this.state.stripeKey)
                }
            }).catch((err) => {
                // alert(err)
            });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }


    OnProccede = async () => {
        this.setState({ visible: true })
        const apiKey = this.state.stripeKey;
        const client = new Stripe(apiKey);
        const token = await client.createToken({
            number: this.state.cardNumber,
            exp_month: this.state.expiryMonth,
            exp_year: this.state.expiryYear,
            cvc: this.state.cvv,
            address_zip: '12345'
        }).then((response) => {
            if (response) {
                console.log("response", response.id)
                let postData = {
                    amount: this.state.amount,
                    totalAmount: this.state.amount,
                    currency_id: 1,
                    user_id: this.state.userId,
                    deposit_payment_id: 2,
                    stripeToken: response.id
                }

                console.log("postdatac--->", postData)
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/deposit/stripe-payment-store',
                    data: postData,
                    headers: { 'Authorization': this.state.token },
                }).then((response) => {
                    console.log("stripe-payment ", response.data)
                    if (response.data.status == 200) {
                        let postData1 = {
                            phone: this.state.phone,
                            country: this.state.isoName,
                            amount: this.state.amount,
                            operator_id: this.state.operatorID,
                            user_id: this.state.userId,
                            currency_id: 1
                        }
                        console.log(postData1)
                        axios({
                            method: 'post',
                            url: 'https://sandbox.caribpayintl.com/api/top-up',
                            data: postData1,
                            headers: { 'Authorization': this.state.token },
                        })
                            .then((response) => {
                                console.log("rcharge from stripe ---", response.data.data)
                                if (response.data.status == 200) {
                                    let createdOn = response.data.data.transactionDate
                                    this.setState({
                                        RecipientPhone: response.data.data.recipientPhone,
                                        txn_date: createdOn,
                                        visible: false,
                                    })
                                    this.props.navigation.navigate("SuccessFullMobile", { operatorName: this.state.operatorName, RecipientPhone: this.state.RecipientPhone, paymentMode: 'PayPal', TxnDate: this.state.txn_date, amount: this.state.amount })
                                }
                            }).catch((err) => {
                                this.setState({ visible: false })
                                alert(err)
                            });
                    }
                }).catch((err) => {
                    alert(err)
                    this.setState({ visible: false })
                });
            } else {
                alert(response.error.message)
                this.setState({ visible: false })
            }
        }).catch((err) => {
            console.log(err)
            this.setState({ visible: false })

        })


    }



    onNext = () => {
        if (parseFloat(this.state.balance) > parseFloat(this.state.amount)) {
            this.setState({
                visible: true
            })
            let postData = {
                phone: this.state.phone,
                country: this.state.isoName,
                amount: this.state.amount,
                operator_id: this.props.navigation.state.params.operatorID,
                user_id: this.state.userId,
                currency_id: 1
            }
            console.log(postData)
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/top-up',
                data: postData,
                headers: { 'Authorization': this.state.token },
            }).then((response) => {
                console.log("MObile wallet ---> ", response.data.status, response.data)
                console.log("MObile wallet  response ---> ", response.data.data)
                if (response.data.status == 200) {
                    this.setState({
                        RecipientPhone: response.data.data.recipientPhone,
                        txn_date: response.data.data.transactionDate,
                        visible: false,
                    })
                    this.props.navigation.navigate("SuccessFullMobile", { operatorName: this.state.operatorName, RecipientPhone: this.state.RecipientPhone, paymentMode: 'Wallet', TxnDate: this.state.txn_date, amount: this.state.amount })
                } else if (response.data.status == 400) {
                    this.setState({
                        tryAgain: true,
                        visible: false
                    })
                }
            }).catch((err) => {
                this.setState({ visible: false })
                alert(err)
            });
        } else {
            showAlert("CaribPay", "You have Insufficient Balance : " + this.state.balance)
        }
    }

    // onNext = () => {
    //     if (this.state.fromWallet == true) {
    //         if (parseFloat(this.state.balance) > parseFloat(this.state.amount)) {
    //             this.setState({
    //                 visible: true
    //             })
    //             let postData = {
    //                 phone: this.state.phone,
    //                 country: this.state.isoName,
    //                 amount: this.state.amount,
    //                 operator_id: this.props.navigation.state.params.operatorID,
    //                 user_id: this.state.userId,
    //                 currency_id: 1
    //             }
    //             console.log(postData)
    //             axios({
    //                 method: 'post',
    //                 url: 'https://sandbox.caribpayintl.com/api/top-up',
    //                 data: postData,
    //                 headers: { 'Authorization': this.state.token },
    //             })
    //                 .then((response) => {
    //                     console.log("MObile wallet ---> ", response.data.status)
    //                     if (response.data.status == 200) {
    //                         this.setState({
    //                             RecipientPhone: response.data.data.recipientPhone,
    //                             txn_date: response.data.data.transactionDate,
    //                             visible: false,
    //                         })
    //                         this.props.navigation.navigate("SuccessFullMobile", { operatorName: this.state.operatorName, RecipientPhone: this.state.RecipientPhone, paymentMode: 'Wallet', TxnDate: this.state.txn_date, amount: this.state.amount })
    //                     }
    //                 }).catch((err) => {
    //                     this.setState({ visible: false })
    //                     alert(err)
    //                 });
    //         } else {
    //             showAlert("CaribPay", "You have Insufficient Balance : " + this.state.balance)
    //         }

    //     } else if (this.state.fromPayPal == true) {
    //         this.setState({ visible: true })
    //         RNPaypal.paymentRequest({
    //             clientId: this.state.client_id,
    //             environment: RNPaypal.ENVIRONMENT.PRODUCTION,
    //             intent: RNPaypal.INTENT.SALE,
    //             price: parseFloat(this.state.amount),
    //             currency: 'USD',
    //             description: 'mobile recharge via paypal',
    //             acceptCreditCards: true
    //         }).then(response => {
    //             if (response.response.state == "approved") {
    //                 console.log("yes it is approved")
    //                 let postData = {
    //                     amount: this.state.amount,
    //                     currencyID: 1,
    //                     userId: this.state.userId,
    //                     methodID: 3,
    //                     details: {
    //                         status: "COMPLETED"
    //                     }
    //                 }
    //                 axios({
    //                     method: 'post',
    //                     url: 'https://sandbox.caribpayintl.com/api/deposit/paypal-payment-store',
    //                     data: postData,
    //                     headers: { 'Authorization': this.state.token },
    //                 }).then((response) => {
    //                     if (response.data.success.status == 200) {
    //                         let postData1 = {
    //                             phone: this.state.phone,
    //                             country: this.state.isoName,
    //                             amount: this.state.amount,
    //                             operator_id: this.props.navigation.state.params.operatorID,
    //                             user_id: this.state.userId,
    //                             currency_id: 1
    //                         }
    //                         console.log(postData1)
    //                         axios({
    //                             method: 'post',
    //                             url: 'https://sandbox.caribpayintl.com/api/top-up',
    //                             data: postData1,
    //                             headers: { 'Authorization': this.state.token },
    //                         })
    //                             .then((response) => {
    //                                 console.log("rcharge from paypal ---", response.data.data)
    //                                 if (response.data.status == 200) {
    //                                     let createdOn = response.data.data.transactionDate
    //                                     this.setState({
    //                                         RecipientPhone: response.data.data.recipientPhone,
    //                                         txn_date: createdOn,
    //                                         visible: false,
    //                                     })
    //                                     this.props.navigation.navigate("SuccessFullMobile", { operatorName: this.state.operatorName, RecipientPhone: this.state.RecipientPhone, paymentMode: 'PayPal', TxnDate: this.state.txn_date, amount: this.state.amount })
    //                                 }
    //                             }).catch((err) => {
    //                                 this.setState({ visible: false })
    //                                 alert(err)
    //                             });
    //                     }
    //                 }).catch((err) => {
    //                     console.log("error in paypal payment store ", err)
    //                 })
    //             } else {
    //                 this.setState({ visible: false })
    //                 alert('Payment Request Failed!')
    //             }
    //         }).catch((error) => {
    //             this.setState({ visible: false })
    //             console.log("error in paymentrequest", error)
    //         })
    //     } else {
    //         this.OnProccede();
    //     }
    // }



    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Select Option to Pay</Text>
                </View>

                <View style={styles.absouluteview}>
                    <View style={{ flex: 1 }}>
                    <Spinner style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />

                        <View style={{ flexDirection: 'column', justifyContent: 'center', margin: metrics.dimen_15 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: colors.black, fontSize: metrics.text_header, fontWeight: '600', textAlignVertical: 'center' }}>{this.state.phone}</Text>
                                <Text style={{ color: colors.black, fontSize: metrics.text_normal, fontWeight: '600', marginRight: metrics.dimen_10, textAlignVertical: 'center' }}>{"$ " + this.state.amount}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: colors.app_yellow_color, fontSize: metrics.text_normal, fontWeight: '600', textAlignVertical: 'center' }}>{"Prepaid , "}</Text>
                                <Text style={{ color: colors.app_yellow_color, fontSize: metrics.text_normal, fontWeight: '600', marginRight: metrics.dimen_10, textAlignVertical: 'center' }}>{this.state.operatorName}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>

                            {/* <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_16, fontWeight: '600', textAlignVertical: 'center' }}>{"Payment Options"}</Text>
                            <TouchableOpacity onPress={() => this.setState({ fromPayPal: true, fromWallet: false, fromCards: false })}>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ width: metrics.dimen_20, height: metrics.dimen_20, borderColor: colors.carib_pay_blue, borderRadius: 20, backgroundColor: this.state.fromPayPal ? colors.carib_pay_blue : 'white', borderWidth: 1 }}>
                                    </View>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, fontWeight: '500', textAlignVertical: 'center', marginLeft: metrics.dimen_30 }}>PayPal</Text>
                                </View>
                            </TouchableOpacity> */}


                            <TouchableOpacity onPress={() => this.setState({ fromPayPal: false, fromWallet: true, fromCards: false })}>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ width: metrics.dimen_20, height: metrics.dimen_20, borderColor: colors.carib_pay_blue, borderRadius: 20, backgroundColor: this.state.fromWallet ? colors.carib_pay_blue : 'white', borderWidth: 1 }}>
                                    </View>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, fontWeight: '500', textAlignVertical: 'center', marginLeft: metrics.dimen_15 }}>Wallet</Text>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, fontWeight: '500', textAlignVertical: 'center', marginLeft: metrics.dimen_10 }}>{"( $ " + this.state.balance + " )"}</Text>
                                </View>
                            </TouchableOpacity>

                            {/* 
                            <TouchableOpacity onPress={() => this.setState({ fromPayPal: false, fromWallet: false, fromCards: true })}>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ width: metrics.dimen_20, height: metrics.dimen_20, borderColor: colors.carib_pay_blue, borderRadius: 20, backgroundColor: this.state.fromCards ? colors.carib_pay_blue : 'white', borderWidth: 1 }}>
                                    </View>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, fontWeight: '500', textAlignVertical: 'center', marginLeft: metrics.dimen_30 }}>Debit/Credit Cards</Text>
                                </View>
                            </TouchableOpacity> */}
                        </View>

                        {this.state.fromCards &&
                            <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>

                                <Text style={{ color: colors.app_yellow_color, fontSize: metrics.text_normal, fontWeight: '600', marginRight: metrics.dimen_10, textAlignVertical: 'center', marginBottom: 10 }}>Please Enter your Card Detail here to make an payment.</Text>

                                <Input
                                    placeholder={'1245 5634 9980'}
                                    containerStyle={{ width: "80%", height: 50, borderColor: colors.app_gray, borderWidth: 0.5, borderRadius: 10, alignSelf: 'center' }}
                                    keyboardType="number-pad"
                                    rightIcon={() => <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/card.png')}></Image>}
                                    value={this.state.cardNumber}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: metrics.dimen_22 }}
                                    onChangeText={(text) => this.setState({ cardNumber: text })}>
                                </Input>

                                <View style={{ flexDirection: 'row', width: '80%', borderColor: colors.app_gray, borderWidth: 0.5, height: 50, marginTop: 10, borderRadius: 10, alignSelf: 'center' }}>
                                    <Input
                                        placeholder={'MM'}
                                        containerStyle={{ flex: 0.5, width: '40%', height: 50, borderRadius: 10 }}
                                        keyboardType="number-pad"
                                        maxLength={2}
                                        value={this.state.expiryMonth}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.dimen_22 }}
                                        onChangeText={(text) => this.setState({
                                            expiryMonth: text
                                        })}>
                                    </Input>

                                    <View
                                        style={{
                                            borderLeftWidth: 1,
                                            height: 35,
                                            alignSelf: 'center',
                                            borderLeftColor: colors.app_gray,
                                        }}></View>

                                    <Input
                                        placeholder={'YY'}
                                        containerStyle={{ flex: 0.5, width: '40%', height: 50, borderRadius: 10 }}
                                        keyboardType="number-pad"
                                        maxLength={2}
                                        value={this.state.expiryYear}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.dimen_22 }}
                                        onChangeText={(text) => this.setState({
                                            expiryYear: text
                                        })}>
                                    </Input>

                                    <View
                                        style={{
                                            borderLeftWidth: 1,
                                            height: 35,
                                            alignSelf: 'center',
                                            borderLeftColor: colors.app_gray,
                                        }}></View>

                                    <Input
                                        placeholder={'CVV'}
                                        containerStyle={{ width: '40%', flex: 0.5, height: 50, alignSelf: 'center', borderRadius: 10 }}
                                        keyboardType="number-pad"
                                        value={this.state.cvv}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.dimen_22 }}
                                        onChangeText={(text) => this.setState({ cvv: text })}>
                                    </Input>
                                </View>
                            </View>
                        }


                    </View>


                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.tryAgain} out>
                            <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Mobile-Topup</Text>
                                </View>
                                <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>

                                    <Text style={{ fontSize: metrics.text_normal, color: colors.theme_caribpay, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>Please Try After Some Time!</Text>

                                    <TouchableOpacity onPress={() => this.setState({
                                        tryAgain: false
                                    })}>
                                        <View style={{ height: metrics.dimen_40, width: metrics.dimen_120, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>


                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.onNext()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{"Proceed to Pay ($" + this.state.amount + ")"}</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' }
})