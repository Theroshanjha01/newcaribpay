import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity, Dimensions, ScrollView, FlatList, ActivityIndicator, BackHandler, KeyboardAvoidingView } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import { Input, CheckBox } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';
import RBSheet from "react-native-raw-bottom-sheet";
var Spinner = require('react-native-spinkit');
const axios = require('axios');
import CountryData from 'country-data';
import Modal from 'react-native-modal';



export default class Register extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hide: true,
      confirmHide: true,
      fname: '',
      lname: '',
      email: '',
      password: '',
      confirm_password: '',
      countryCode: '',
      phone: '',
      countryDataSource: [], newData: [], text: '',
      arrayholder: [],
      isoName: '',
      isVisibleAlert:false,
      visible:false
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    axios.get('https://topups.reloadly.com/countries').then((response) => {
      this.setState({
        countryDataSource: response.data,
      });
      this.state.arrayholder = response.data
    }).catch((err) => {
      console.warn(err)
    });

    axios.get('https://ipinfo.io/json').then((response) => {
      let country_code = response.data.country;
      let countrycallingcode = JSON.stringify(CountryData.countries[country_code].countryCallingCodes[0]);
      this.setState({
        countryCode: countrycallingcode.replace(/"/g, ""),
        isoName: country_code
      });
    }).catch((error) => {
      console.log(error);
    });

  }


  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }


  handleBackPress = () => {
    this.props.navigation.navigate('Login')
    return true;
  }

  SearchFilterFunction(text) {
    const newData = this.state.arrayholder.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ countryDataSource: newData, text: text });
  }

  renderCountries = () => {
    return (
      <View>
        <View style={{ height: metrics.dimen_60, backgroundColor: colors.theme_caribpay, flexDirection: 'row' }}>
          <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetAlert.close()}>
            <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/leftarrow.png')}></Image>
          </TouchableOpacity>
          <Text style={{ fontSize: metrics.text_heading, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Select Country</Text>
        </View>
        <View style={{ marginTop: metrics.dimen_5, width: "95%", alignSelf: 'center' }}>
          <Input
            containerStyle={{ alignSelf: 'center', backgroundColor: '#DCDCDC', borderRadius: 10, height: 45 }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholder='Search'
            placeholderTextColor="#9D9D9F"
            inputStyle={{ color: colors.black, fontSize: metrics.text_16, }}
            value={this.state.text}
            onChangeText={text => this.SearchFilterFunction(text)}
            leftIcon={<Image style={{ width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }} source={require('../Images/search.png')}></Image>} />

        </View>

        <FlatList
          style={{ marginTop: metrics.dimen_7 }}
          showsVerticalScrollIndicator={false}
          data={this.state.countryDataSource}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => this.renderItemCountries(item)}>
        </FlatList>
      </View>
    )
  }


  getCodesByList = (item) => {
    this.setState({
      countryCode: item.callingCodes[0],
      isoName: item.isoName

    })
    this.RBSheetAlert.close();
  }



  renderItemCountries = (item) => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.getCodesByList(item)} >
          <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
            <Image style={{ height: 30, width: 30, resizeMode: 'contain' }} source={{ uri: 'https://www.countryflags.io/' + item.isoName.toLocaleLowerCase() + '/flat/64.png' }}></Image>
            <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, paddingHorizontal: 10 }}>{item.name}</Text>
            <Text style={{ color: colors.app_black_text, fontSize: metrics.text_description }}>{" (" + item.callingCodes[0] + ")"}</Text>
          </View>
          <View style={{ borderBottomColor: colors.light_grey_backgroud, borderBottomWidth: 1 }} />
        </TouchableOpacity>
      </View>

    )

  }

  onSignup = () => {
    var patt = new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");
    if (this.state.fname == "") { this.refs.toast.show("First Name is required", DURATION.LENGTH_LONG); }
    else if (this.state.lname == "") { this.refs.toast.show("Last Name is required", DURATION.LENGTH_LONG); }
    else if (patt.test(this.state.email) == false) { this.refs.toast.show("Email ID is Invalid,Try Again!", DURATION.LENGTH_LONG); }
    else if (this.state.countryCode == "") { this.refs.toast.show("Please Select your Country", DURATION.LENGTH_LONG); }
    else if (this.state.phone == "") { this.refs.toast.show("Mobile Number Required ", DURATION.LENGTH_LONG); }
    else if (this.state.password == "") { this.refs.toast.show("Password is required", DURATION.LENGTH_LONG); }
    else if (this.state.confirm_password == "") { this.refs.toast.show("Please Confirm your Password", DURATION.LENGTH_LONG); }
    else if (this.state.password != this.state.confirm_password) {
      this.refs.toast.show("Password Mismatchs, try again", DURATION.LENGTH_LONG);
    }
    else {
      this.setState({
        visible: true
      });
      let email = this.state.email
      let password = this.state.password
      let carrierCode = this.state.countryCode
      let phone = this.state.phone
      let first_name = this.state.fname
      let last_name = this.state.lname
      let type = 'user'
      let formattedPhone = carrierCode + this.state.phone
      console.log(formattedPhone)
      let data = [];
      
      const signup_form_data = {
        fname: first_name,
        lname: last_name,
        user_email: email,
        user_password: password,
        user_phone_number: phone,
        user_type: type,
        user_countryCode: carrierCode,
        user_formattedPhone: formattedPhone
      }
      data.push(signup_form_data);
      axios({
        method: 'post',
        url: 'https://sandbox.caribpayintl.com/api/send-otp',
        data: { phone: formattedPhone },
      })
        .then((response) => {
          console.log(response.data.response.otp)
          if (response.data.response.status == 200) {
            this.props.navigation.navigate('EnterOtp', { item: data, otp: response.data.response.otp });
            this.setState({ visible: false })
          }
        })
        .catch((err) => {
          alert(err)
        });
    }
  }
  render() {
    let name = this.state.isoName
    var inshort = name.toLocaleLowerCase();
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1 }}>
         
          <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.dimen_240 }}>
            <View style={{ margin: metrics.dimen_20 }}>
              <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image>
              {/* <Text style={{ fontSize: metrics.text_header, color: colors.app_light_gray, fontWeight: 'bold' }}>Welcome </Text> */}
              <Text style={{ fontSize: metrics.text_21, color: colors.white, fontWeight: 'bold' }}>Join us!</Text>
            </View>
          </View>
          <View style={{ position: "absolute", top: metrics.dimen_160, width: "100%", height: Dimensions.get('screen').height - 220, backgroundColor: colors.white, borderRadius: 40 }}>
            <ScrollView>
              <View style={{ flexDirection: 'column', marginHorizontal: metrics.dimen_20 }}>
                <View style={{ marginTop: metrics.dimen_40, marginHorizontal: metrics.dimen_10 }}>
                  <Text style={{ fontSize: metrics.text_header, color: colors.black }}>First Name</Text>
                  <Input
                    containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                    placeholder='First Name'
                    placeholderTextColor={colors.place_holder_color}
                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                    inputStyle={{ color: colors.heading_black_text, fontSize: metrics.text_normal, textAlignVertical: 'bottom', paddingHorizontal: 5, fontFamily: metrics.quicksand_regular }}
                    value={this.state.fname}
                    // rightIcon={
                    //   <Image style={{ height: 20, width: 20, marginTop: 2 }} source={require('../Images/userform.png')}></Image>
                    // }
                    onChangeText={(text) => { this.setState({ fname: text }) }} />

                  <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Last Name</Text>

                  <Input
                    containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                    placeholder='Last Name'
                    placeholderTextColor={colors.place_holder_color}
                    inputContainerStyle={{ borderBottomWidth: 0.2, marginBottom: 0 }}
                    inputStyle={{ color: colors.heading_black_text, fontSize: metrics.text_normal, textAlignVertical: 'bottom', paddingHorizontal: 5, fontFamily: metrics.quicksand_regular }}
                    value={this.state.lname}
                    // rightIcon={
                    //   <Image style={{ height: 20, width: 20, marginTop: 2 }} source={require('../Images/userform.png')}></Image>
                    // }
                    onChangeText={(text) => { this.setState({ lname: text }) }} />


                  <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Email</Text>


                  <Input
                    containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                    placeholder='E-mail'
                    placeholderTextColor={colors.place_holder_color}
                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                    inputStyle={{ color: colors.heading_black_text, fontSize: metrics.text_normal, textAlignVertical: 'bottom', paddingHorizontal: 5, fontFamily: metrics.quicksand_regular }}
                    value={this.state.email}
                    // rightIcon={
                    //   <Image style={{ height: 20, width: 20, marginTop: 2 }} source={require('../Images/mail.png')}></Image>
                    // }
                    onChangeText={(text) => { this.setState({ email: text }) }} />


                  <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15, marginBottom: 5 }}>{"Mobile No."}</Text>


                  <View style={{ flexDirection: 'row', width: '100%', alignSelf: 'center', height: 50 }}>
                    <Input
                      placeholder={'Code'}
                      value={this.state.countryCode}
                      disabled={true}
                      placeholderTextColor={colors.place_holder_color}
                      onChangeText={(text) => this.setState({ countryCode: text })}
                      leftIcon={
                        <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                          {this.state.isoName == "" ?
                            <Image style={{ height: 20, width: 20 }} source={require('../Images/globe.png')}>
                            </Image> :
                            <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://www.countryflags.io/' + inshort + '/flat/64.png' }}>
                            </Image>
                          }
                        </TouchableOpacity>
                      }
                      containerStyle={{ width: "30%", height: 60 }}
                      inputContainerStyle={{ borderBottomWidth: 0.5 }}
                      inputStyle={{ fontSize: 14 }}>
                    </Input>


                    <Input
                      placeholder={'Mobile No.'}
                      containerStyle={{ width: "70%", height: 60 }}
                      keyboardType="number-pad"
                      placeholderTextColor={colors.place_holder_color}
                      // rightIcon={
                      //   <Image style={{ height: 20, width: 20, marginTop: 2 }} source={require('../Images/phone.png')}></Image>
                      // }
                      value={this.state.phone}
                      inputContainerStyle={{ borderBottomWidth: 0.5 }}
                      inputStyle={{ fontSize: 14 }}
                      onChangeText={(text) => this.setState({ phone: text })}>
                    </Input>
                  </View>



                  <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Password</Text>


                  <Input
                    placeholder={'Password'}
                    placeholderTextColor={colors.place_holder_color}
                    value={this.state.password}
                    onChangeText={(text) => this.setState({ password: text })}
                    secureTextEntry={this.state.hide == true ? true : false}
                    rightIcon={
                      <TouchableOpacity onPress={() => this.setState({ hide: !this.state.hide })}>
                        <Image style={{ height: 20, width: 20, marginRight: 5 }} source={this.state.hide ? require('../Images/hide.png') : require('../Images/eye.png')}>
                        </Image>
                      </TouchableOpacity>
                    }
                    containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                    inputStyle={{ fontSize: 14 }}>
                  </Input>

                  <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Confirm Password</Text>

                  <Input
                    placeholder={'Confirm Password'}
                    value={this.state.confirm_password}
                    placeholderTextColor={colors.place_holder_color}
                    onChangeText={(text) => this.setState({ confirm_password: text })}
                    secureTextEntry={this.state.confirmHide == true ? true : false}
                    rightIcon={
                      <TouchableOpacity onPress={() => this.setState({ confirmHide: !this.state.confirmHide })}>
                        <Image style={{ height: 20, width: 20, marginRight: 5 }} source={this.state.confirmHide ? require('../Images/hide.png') : require('../Images/eye.png')}>
                        </Image>
                      </TouchableOpacity>
                    }
                    containerStyle={{ width: '100%', alignSelf: 'center', height: 50 }}
                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                    inputStyle={{ fontSize: 14 }}>
                  </Input>
                </View>
                <Toast
                  ref="toast"
                  style={{ backgroundColor: 'black' }}
                  position='center'
                  positionValue={200}
                  fadeInDuration={200}
                  fadeOutDuration={1000}
                  opacity={0.8}
                  textStyle={{ color: 'white' }} />

                <RBSheet
                  ref={ref => {
                    this.RBSheetAlert = ref;
                  }}
                  height={Dimensions.get('screen').height}
                  duration={0}>
                  {this.renderCountries()}
                </RBSheet>


                <Modal style={{width:"70%", alignSelf:"center"}} isVisible={this.state.isVisibleAlert}>
                  <View style={{ backgroundColor: 'white', borderRadius: metrics.dimen_10 }}>
                    <View style={{ flexDirection: 'column', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                      <Text style={{ fontSize: metrics.text_heading, color: colors.white, padding: 20, fontWeight: 'bold', textAlign: 'center' }}>CaribPay</Text>
                    </View>
                    <View style={{ flexDirection: 'column', margin: metrics.dimen_10 }}>
                      <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, textAlign: 'center' }}>Please agree to our terms and condition.</Text>
                    </View>

                    <View style={{ margin: metrics.dimen_15, flexDirection: 'row', justifyContent: 'center' }}>
                      <TouchableOpacity onPress={() => this.setState({ isVisibleAlert: false })}>
                        <View style={{ backgroundColor: colors.carib_pay_blue, borderRadius: metrics.dimen_7, height: metrics.dimen_40, width: 100, marginLeft: 10, justifyContent: 'center' }}>
                          <Text style={{ paddingHorizontal: metrics.dimen_10, fontSize: metrics.text_header, color: colors.white, textAlign: 'center', fontWeight: '900' }}>OK</Text>
                        </View>
                      </TouchableOpacity>

                    </View>
                  </View>
                </Modal>




              </View>



              <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: metrics.dimen_20, marginTop: metrics.dimen_15 }}>
                <CheckBox
                  checkedIcon={<Image source={require('../Images/tick.png')}
                    style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: 3 }}></Image>}
                  uncheckedIcon={<Image source={require('../Images/unchecked.png')}
                    style={{ height: metrics.dimen_20, width: metrics.dimen_20 }}></Image>}
                  containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, marginLeft: -10 }}
                  checked={this.state.checked}
                  onPress={() => this.setState({ checked: !this.state.checked })}
                />
                <Text style={{
                  color: colors.heading_black_text,
                  marginLeft: -20, fontSize: metrics.text_description,
                }}
                  onPress={() => { console.warn("clicked") }}>I Agree to Terms {'\u0026'} Conditions.</Text>
              </View>

              <Spinner style={{ justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />

              <View style={{ height: 50, backgroundColor: colors.theme_caribpay, width: '80%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5, marginTop: metrics.dimen_50, marginBottom: metrics.dimen_50 }}>
                <TouchableOpacity onPress={() =>  this.state.checked ? this.onSignup() : this.setState({ isVisibleAlert: true })}>
                  <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Register</Text>
                </TouchableOpacity>
              </View>


              <View style={{ alignSelf: 'center', marginTop: metrics.dimen_10, marginBottom:metrics.dimen_40}}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                  <Text style={{ fontSize: metrics.text_header, color: colors.carib_pay_blue, fontWeight: 'bold', marginTop: 20, marginHorizontal: metrics.dimen_20 }}>Already Have an Account ?<Text style={{ fontSize: metrics.text_heading, color: colors.theme_caribpay, fontWeight: 'bold', textAlign: 'center', marginTop: 20 }}>{" Sign in"}</Text></Text>
                </TouchableOpacity>
              </View>

            </ScrollView>

          </View>

        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  imageStyle: {
    height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain', marginTop: 5
  }, buttonStyle: {
    flexDirection: 'row',
    width: '80%',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 50,
    height: 40,
    backgroundColor: colors.theme_caribpay,
    shadowColor: "#f2f2f2",
    shadowOpacity: 1,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1
    }
  },
  bottomview: { bottom: 0, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

});
