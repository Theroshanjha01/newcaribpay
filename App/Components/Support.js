import React from 'react';
import { SafeAreaView ,View, Text, Image, TouchableOpacity, StyleSheet, Dimensions, BackHandler } from 'react-native';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import email from 'react-native-email';


export default class Support extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    handleEmail = () => {
        const to = ['support@caribpayintl.com']
        email(to, {
            subject: "",
            body: ""
        }
        ).catch(console.error)

    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Support</Text>
                </View>

                <View style={styles.absouluteview}>
                    <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/support.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                            <TouchableOpacity onPress={() => this.handleEmail()}>
                                <Text style={styles.headingg}>Account Support</Text>
                                <Text style={styles.descriiption}>{"Please send us email to \n"}
                                    <Text style={{ ...styles.descriiption, color: colors.carib_pay_blue, textDecorationLine: 'underline' }}>support@caribpayintl.com </Text>

                                    <Text style={styles.descriiption}>will get back to you in next 24 hour time.</Text></Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.horizontalLine} />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }
})
