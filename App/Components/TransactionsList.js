import React from 'react';
import { SafeAreaView, Text, View, FlatList, TouchableOpacity, BackHandler, Image, StyleSheet, Dimensions } from 'react-native';
// import ProgressDialog from '../ProgressDialog.js';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
const moment = require('moment');
const axios = require('axios');
import AsyncStorage from '@react-native-community/async-storage';
import RBSheet from "react-native-raw-bottom-sheet";
import { camelCase } from '../Utils.js';
var Spinner = require('react-native-spinkit');

export default class TransactionsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            transactionData: [],
            subtotalValue: '',
            transaction_id: '',
            type: '',
            payment_method_name: '', user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            visible: false,
            requestLoading: false,
            request_To_Status: "",
            userEndFullName: '',
            endUserEmail: '',
            available_Balance: this.props.navigation.state.params.available_Balance,
            txn_ref_id: '',
            txn_total_fees: '',
            symbol: this.props.navigation.state.params.symbol
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({
            visible: true
        })
        let postData = {
            type: "allTransactions",
            user_id: this.state.user_id
        }
        console.log(postData, this.state.token)
        axios.get('https://sandbox.caribpayintl.com/api/activityall', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            let txnlist = response.data.transactions
            const currencywise = txnlist.filter(roshan => roshan.curr_code == this.props.navigation.state.params.symbol && roshan.status == "Success");
            if (response.data.success.status == 200) {
                this.setState({
                    transactionData: currencywise,
                    visible: false
                })
            }
        }).catch((error) => {
            console.log(error);
        })
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    onRequestMoney = (item) => {
        let postData = { trans_id: item.id }
        console.log(postData)
        axios.get('https://sandbox.caribpayintl.com/api/transaction-details/request-payment/check-creator-status', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            if (response.data.success.status == 200) {
                let user_name = camelCase(item.end_user_f_name) + " " + camelCase(item.end_user_l_name)
                this.setState({
                    userEndFullName: user_name,
                    endUserEmail: item.end_user_email
                })
                this.RBSheetRequestToMoney.open()
            }
        }).catch((error) => {
            console.log(error);
        })
    }



    onPressItems = async (item) => {
        console.log("Data", item)
        console.log("id", item.id)
        let user_token = await AsyncStorage.getItem('token');
        let postData = {
            user_id: item.user_id,
            tr_id: item.id
        }
        await axios.get('https://sandbox.caribpayintl.com/api/transaction-details', { params: postData, headers: { 'Authorization': user_token } }).then((response) => {
            console.log("TXNDTA ---> ", response.data.transaction)
            if (response.data.success.status == 200) {
                var txn_type;
                switch (response.data.transaction.transaction_type_id) {
                    case 1:
                        txn_type = "Reload"
                        break;
                    case 4:
                        txn_type = "Money Receive"
                        break;
                    case 9:
                        txn_type = "Request Sent"
                        break;
                    case 12:
                        txn_type = "Shopping"
                        break;
                    case 3:
                        txn_type = "Money Transfer"
                        break;
                    case 10:
                        txn_type = "Request Received"
                        break;
                    case 11:
                        txn_type = "Shopping"
                        break;
                    case 15:
                        txn_type = "Mobile Top-Up"
                        break;
                }
                this.setState({
                    subtotalValue: response.data.transaction.subtotal,
                    transaction_id: response.data.transaction.transaction_id,
                    type: txn_type,
                    payment_method_name: response.data.transaction.payment_method_name,
                    request_To_Status: response.data.transaction.status,
                    txn_ref_id: response.data.transaction.transaction_reference_id,
                    txn_total_fees: response.data.transaction.totalFees,
                    txn_detail_time: response.data.transaction.t_created_at
                })
                console.log("info coming ,", this.state.subtotalValue, this.state.transaction_id, this.state.type, this.state.payment_method_name)
                { this.state.type == "Request Received" && this.state.request_To_Status == "Pending" ? this.onRequestMoney(item) : this.RBSheetAlert.open() }
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    formatDate = (dateformat, date) => {
        try {
            return moment.utc(date).local().format(dateformat)
        } catch (error) {
            return 'Invalid Date'
        }
    }

    renderItem = (data, index) => {
        console.log("txn data--->", data)
        var txn_type;
        switch (data.transaction_type_id) {
            case 1:
                txn_type = "Reload"
                break;
            case 4:
                txn_type = "Money Receive"
                break;
            case 9:
                txn_type = "Request Sent"
                break;
            case 12:
                txn_type = "Shopping"
                break;
            case 3:
                txn_type = "Money Transfer"
                break;
            case 10:
                txn_type = "Request Received"
                break;
            case 11:
                txn_type = "Shopping"
                break;
            case 15:
                txn_type = "Mobile Top-Up"
                break;
        }
        return (
            <TouchableOpacity onPress={() => this.onPressItems(data)}>
                <View style={styles.cellContainer}>
                    <View style={{
                        flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between', marginBottom: 10,
                        borderWidth: 1,
                        borderRadius: 7,
                        borderColor: '#ddd',
                        borderBottomWidth: 1,
                        shadowColor: 'black',
                        shadowOffset: { width: 1, height: 2 },
                        shadowOpacity: 0.9,
                        shadowRadius: 3,
                        elevation: 5,
                    }}>
                        <View style={{ flexDirection: 'row' }}>
                            {data.user_photo != "" ? <Image style={{ height: 45, width: 45, alignSelf: 'center', borderRadius: 45 / 2, marginLeft: 20 }} source={{ uri: data.user_photo }}></Image> :
                                <Image style={{ height: 45, width: 45, alignSelf: 'center', borderRadius: 45 / 2, marginLeft: 20 }} source={require("../Images/money.png")}></Image>}
                                <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 20 }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_black_text }}>{txn_type}</Text>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{this.formatDate('MMMM DD, YYYY', data.t_created_at)}</Text>
                                </View>
                        </View>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, fontWeight: 'bold', textAlignVertical: 'center', marginRight: 20 }}>{data.curr_symbol + "" + data.subtotal}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    gotoReviewSend = () => {
        this.RBSheetRequestToMoney.close();
        this.props.navigation.navigate("ReviewSend", { sendCurrency: "1", user_id: this.state.user_id, sendAmount: this.state.subtotalValue, balance: this.state.available_Balance, emailPhone: this.state.endUserEmail, fromTxnList: '1', txn_ref_id: this.state.txn_ref_id, totalFees: this.state.txn_total_fees })
    }


    renderRequestMoney = () => {
        return (
            <View style={{ flex: 1 }}>

                <View>
                    <View style={{ height: 60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                        <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', paddingHorizontal: 20, textAlign: 'center' }}>Request Money</Text>
                    </View>
                    <View style={{ margin: metrics.dimen_15, justifyContent: 'center', flexDirection: 'column' }}>
                        <Text style={{ fontSize: metrics.text_large, color: colors.carib_pay_blue, fontWeight: 'bold', textAlign: 'center' }}>{this.state.userEndFullName}</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, fontWeight: 'bold', textAlign: 'center', marginTop: metrics.dimen_5 }}>{"Requesting You for amount $" + this.state.subtotalValue}</Text>

                        <View style={{ flexDirection: 'row', flex: 1, marginTop: metrics.dimen_15 }}>

                            <View style={{ flex: 0.5, height: metrics.dimen_50, justifyContent: 'center', borderRadius: metrics.dimen_7, backgroundColor: 'red' }}>
                                <Text style={{ fontSize: metrics.text_header, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{"Reject"}</Text>
                            </View>


                            <TouchableOpacity style={{ flex: 0.5, height: metrics.dimen_50, justifyContent: 'center', borderRadius: metrics.dimen_7, backgroundColor: colors.carib_pay_blue, marginLeft: metrics.dimen_10 }} onPress={() => this.gotoReviewSend()}>
                                <View style={{ flex: 0.5, height: metrics.dimen_50, justifyContent: 'center', borderRadius: metrics.dimen_7, backgroundColor: colors.carib_pay_blue, marginLeft: metrics.dimen_10 }}>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{"Accept"}</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                    </View>

                </View>

            </View>
        )
    }





    renderTxnDetails = () => {
        return (
            <View>
                <View style={{ height: 60, backgroundColor: colors.theme_caribpay, justifyContent: 'center' }}>
                    <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', paddingHorizontal: 20 }}>Transaction Detail</Text>
                </View>

                <View style={{ flexDirection: 'column', margin: metrics.dimen_10, borderTopLeftRadius: 10, borderTopLeftRadius: 10 }}>
                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>{this.state.type == "Transferred" ? "Fund Transferred" : "Fund Added"}</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{"$ " + this.state.subtotalValue}</Text>
                    </View>


                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>{"Date / Time"}</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.formatDate('DD/MM/YYYY HH:MM:SS', this.state.txn_detail_time)}</Text>
                    </View>




                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Transaction No.</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.transaction_id}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Type</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.type}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>Payment Method</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.payment_method_name == undefined ? "Wallet" : this.state.payment_method_name}</Text>
                    </View>


                </View>
            </View>
        )
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Spinner style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                {this.state.visible == false && this.state.transactionData.length > 0 &&
                    <View>
                        <FlatList
                            style={{ marginTop: metrics.dimen_10 }}
                            data={this.state.transactionData}
                            renderItem={({ item, index }) => this.renderItem(item, index)}
                            keyExtractor={(item, index) => item + index}
                        />
                    </View>
                }
                {this.state.visible == false && this.state.transactionData.length == 0 &&
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <Image style={{ width: 250, height: 150, resizeMode: 'contain', alignSelf: 'center' }}
                            source={require("../Images/notxn.png")}></Image>
                        <Text style={{ fontSize: metrics.text_21, color: colors.theme_caribpay, textAlign: 'center', alignSelf: 'center' }}>Sorry, No Transactions!</Text>
                    </View>
                }
                <RBSheet
                    ref={ref => {
                        this.RBSheetAlert = ref;
                    }}
                    height={360}
                    duration={0}>
                    {this.renderTxnDetails()}
                </RBSheet>
                <RBSheet
                    ref={ref => {
                        this.RBSheetRequestToMoney = ref;
                    }}
                    height={230}
                    duration={0}>
                    {this.renderRequestMoney()}
                </RBSheet>
            </SafeAreaView>


        )
    }
}


const styles = StyleSheet.create({
    listStyle: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: 'transparent'
    },
    cellContainer: {
        width: '100%',
        flexDirection: 'column',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 7

    },
    cellSubViewContainer: {
        padding: 8,
        flexDirection: 'column',
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 5,
        marginHorizontal: 8,
        marginTop: 4,
        borderColor: colors.view_border_color,
    },
    cellSubViewTopContainer: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingBottom: 0

    },
    SectionHeaderStyle: {
        backgroundColor: 'white',
        fontSize: metrics.text_16,
        padding: 10,
        color: colors.heading_color,
    },
    iconStyle: {
        marginTop: 2,
        width: metrics.dimen_27,
        height: metrics.dimen_27,

    },
    payment_iconStyle: {
        width: 18,
        height: 18,
        marginLeft: 10,
        marginRight: 10

    },
    textStyleBold: {
        fontFamily: metrics.proxima_nova,
        color: colors.gray,
        fontSize: metrics.text_normal,
        fontWeight: 'normal',
        // textAlign: 'center'
    },
    textStyleBoldBalance: {
        fontFamily: metrics.proxima_nova,
        // color: colors.gray,
        fontSize: metrics.text_normal,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    textStyleExtBold: {
        fontFamily: metrics.proxima_nova,
        color: colors.gray,
        fontSize: metrics.text_normal,
        fontWeight: '400',
    },
    textStyleNormal: {
        fontFamily: metrics.proxima_nova,
        fontSize: metrics.text_normal,
        color: 'black',
        fontWeight: '400'
    },

    textStyleNormalwhite: {
        fontFamily: metrics.proxima_nova,
        fontSize: metrics.text_normal,
        color: 'white',
        fontWeight: '400'
    },
    textStyleRegular: {
        fontFamily: metrics.proxima_nova_regular,
        fontSize: metrics.text_medium,
        color: colors.white,
        textAlign: 'center',
        marginTop: metrics.dimen_2

    },
    textStyleRegular2: {
        fontFamily: metrics.proxima_nova_regular,
        fontSize: metrics.text_medium,
        textAlign: 'center',
        marginTop: metrics.dimen_2
    },
    seperatorStyle: {
        height: 1,
        width: '100%',
        backgroundColor: colors.view_border_color,
    },
    requestButtonStyle: {
        backgroundColor: colors.app_blueColor,
        height: metrics.dimen_40,
        paddingLeft: metrics.dimen_30,
        paddingRight: metrics.dimen_30,
        borderRadius: metrics.dimen_50 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    bottomViewStyle: {
        position: 'absolute',
        width: '100%',
        paddingTop: 40,
        bottom: 0,
        left: 0,
        alignItems: 'center'
    }
})
