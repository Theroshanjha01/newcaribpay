import React from 'react';
import { ActivityIndicator, View, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';


export default class Splash extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    async componentDidMount() {
        let istoken = await AsyncStorage.getItem('token')
        let passcode = await AsyncStorage.getItem('passcode')
        let touchid = await AsyncStorage.getItem('TouchIDEnable')

        console.log(touchid)


        setTimeout(() => {
            if (istoken == null || undefined) { this.props.navigation.navigate('IntroSlider') }
            else if (istoken != null && (passcode == null || undefined) && touchid != "1") {
                this.props.navigation.navigate('Home')
            }
            else if (istoken != null && (passcode != null || undefined) && touchid != "1") {
                this.props.navigation.navigate('LockScreen')
            }
            else if (istoken != null && touchid == "1") {
                this.props.navigation.navigate('Login')
            }
        }, 1500);

    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignContent: "center", backgroundColor: colors.theme_caribpay }}>
                <Image style={{ width:120, height:120, resizeMode: 'contain', alignSelf: "center" }} source={require('../Images/logo_1024.png')}></Image>

                <View style={{ bottom: 15, position: 'absolute', justifyContent: 'center', alignSelf: 'center', }}>
                    <ActivityIndicator size='large' color={colors.carib_pay_blue} animating={true}></ActivityIndicator>
                </View>

            </View>
        )
    }
}