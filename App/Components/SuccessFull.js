import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, BackHandler } from 'react-native';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
const moment = require('moment');


export default class SuccessFull extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: this.props.navigation.state.params.amount,
            updated_balance: this.props.navigation.state.params.updated_balance,
            remarks: this.props.navigation.state.params.remarks,
            created0n: this.props.navigation.state.params.created0n,
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            payment_mode: this.props.navigation.state.params.payment_mode
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }


    render() {
        let txn_date = moment(this.state.created0n).format('DD-MM-YYYY hh:mm A');
        return (
            <View style={styles.container}>

                <View style={{ height: metrics.dimen_200, backgroundColor: colors.app_dark_green_color, justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_80, resizeMode: 'contain', alignSelf: 'center', marginStart: metrics.dimen_30 }} source={require('../Images/tick_success.png')}></Image>
                        <Text style={{ fontSize: metrics.text_17, color: colors.white, fontWeight: 'bold', marginLeft: metrics.dimen_30, textAlignVertical: 'center' }}>Add Fund SuccessFully</Text>
                    </View>
                </View>


                <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Amount Added</Text>
                        <Text style={styles.subHeading}>{"$ " + this.state.amount}</Text>
                        <View style={styles.horizontal} />
                    </View>

                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Payment Mode</Text>
                        <Text style={styles.subHeading}>{this.state.payment_mode}</Text>
                        <View style={styles.horizontal} />
                    </View>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Your updated Balance</Text>
                        <Text style={styles.subHeading}>{"$ " + this.state.updated_balance}</Text>
                        <View style={styles.horizontal} />
                    </View>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Remarks</Text>
                        <Text style={styles.subHeading}>{this.state.remarks}</Text>
                        <View style={styles.horizontal} />
                    </View>


                    <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                        <Text style={styles.textHeading}>Funds Added On</Text>
                        <Text style={styles.subHeading}>{txn_date}</Text>
                        <View style={styles.horizontal} />
                    </View>



                </View>

                <View style={styles.bottomview}>
                    <View style={{ flexDirection: 'row', flex: 1, height: 50 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                            <View style={{ flex: 0.5, width: metrics.dimen_180, height: metrics.dimen_50, borderRadius: metrics.dimen_10, backgroundColor: colors.app_gray, justifyContent: 'center' }}>
                                <Text style={{ textAlignVertical: 'center', textAlign: 'center', fontSize: metrics.text_description, color: 'white', fontWeight: 'bold' }}>Home</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('TransactionsList', { user_id: this.state.user_id, token: this.state.token })}>
                            <View style={{ flex: 0.5, width: metrics.dimen_180, height: metrics.dimen_50, borderRadius: metrics.dimen_10, backgroundColor: colors.carib_pay_blue, justifyContent: 'center', marginLeft: 10 }}>
                                <Text style={{ textAlignVertical: 'center', textAlign: 'center', fontSize: metrics.text_description, color: 'white', fontWeight: 'bold' }}>View Transaction</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    textHeading: { fontSize: metrics.text_medium, color: colors.black, textAlignVertical: 'center' },
    subHeading: { fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', textAlignVertical: 'center' },
    horizontal: { borderBottomColor: 'black', borderBottomWidth: 0.5, marginTop: metrics.dimen_10 },
    bottomview: { bottom: 10, height: 100, position: 'absolute', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },

})