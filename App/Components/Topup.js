import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, FlatList, BackHandler, ActivityIndicator } from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';
import { Input } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';
var Spinner = require('react-native-spinkit');
import RBSheet from "react-native-raw-bottom-sheet";
import { ScrollView } from 'react-native-gesture-handler';
const axios = require('axios');
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';


export default class Topup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            phone: this.props.navigation.state.params.phone,
            isoName: this.props.navigation.state.params.isoName,
            fromTopup: true,
            fromPlans: false,
            operatorName: '',
            operatorID: '',
            operator_logo_url: '',
            topups: [],
            amount: '',
            destinationCurrencyCode: '',
            operatorData: [],
            operator_visible: false,
            isComingSoon: false
        }
        console.disableYellowBox = true;
    }


    componentDidMount() {
        console.log("id---->", this.state.user_id)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({ visible: true })
        let postData = { iso: this.state.isoName, phone: this.state.phone }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/get-operators-for-phone',
            data: postData,
            headers: { 'Authorization': this.state.token },
        })
            .then((response) => {
                console.log(response.data.data.operatorId)
                if (response.data.status == 200) {
                    let data_top_ups = []
                    var jsonObj = response.data.data.suggestedAmountsMap
                    for (var key in jsonObj) {
                        const feed = { inUsd: key, inLocal: jsonObj[key] }
                        data_top_ups.push(feed)
                    }

                    let arr = data_top_ups.map((item, index) => {
                        // item.toggle = false
                        item.id = ++index
                        return { ...item };
                    })

                    this.setState({
                        operatorName: response.data.data.name,
                        operatorID: response.data.data.operatorId,
                        operator_logo_url: response.data.data.logoUrls[0],
                        destinationCurrencyCode: response.data.data.destinationCurrencyCode,
                        topups: arr,
                        visible: false
                    })

                    console.log("top ups Array  -- > ", this.state.topups)
                }
            }).catch((err) => {
                this.setState({ visible: false })
                alert(err)
            });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }





    onNext = () => {
        if (this.state.amount == "") {
            this.refs.toast.show("please select the top-up");
        } else {
            this.props.navigation.navigate("TopupPayment", { userId: this.state.user_id, token: this.state.token, isoName: this.state.isoName, phone: this.state.phone, amount: this.state.amount, operatorName: this.state.operatorName, operatorID: this.state.operatorID })
        }
    }

    onSelectOperator = () => {
        this.RBSheetAlert.open();
        this.setState({
            operator_visible: true
        })
        let postData = { iso: this.state.isoName }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/get-operators-by-iso',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            if (response.data.status == 200) {
                this.setState({
                    operatorData: response.data.data,
                    // operator_logo_url: response.data.data.logoUrls[0],
                    operator_visible: false
                })
            }
        }).catch((error) => {
            console.log("error ", error)
        })
    }

    renderMoreOperators = () => {
        return (
            this.state.operator_visible == true ?
                <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                    <ActivityIndicator size='large' color={colors.carib_pay_blue} animating={this.state.operator_visible}></ActivityIndicator>
                    <Text style={{ color: 'black', textAlign: 'center', marginTop: 10 }}>Please wait fetching more operators for you.</Text></View>
                : <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.operatorData}
                    numColumns={2}
                    keyExtractor={(item, index) => item.toString()}
                    renderItem={({ item, index }) =>
                        <View style={{ flex: 1, flexDirection: 'column', margin: metrics.dimen_20, borderColor: colors.light_grey_backgroud, borderWidth: 1, borderRadius: 10 }}>
                            <Image style={{ height: 60, width: 60, borderRadius: 30, resizeMode: 'contain', alignSelf: 'center' }}
                                source={{ uri: item.logoUrls[0] }}></Image>
                            <Text style={{ textAlign: 'center', fontSize: metrics.text_normal, color: colors.heading_black_text }}>{item.name}</Text>
                        </View>
                    } />
        )
    }
    handleSelection = (item, id) => {
        var selectedid = this.state.selectedID
        if (selectedid === id) {
            this.setState({
                selectedID: null,
                amount: ''

            })
        } else {
            this.setState({
                selectedID: id,
                amount: item.inUsd
            })
        }
    }



    renderTopups = (item) => {
        console.log("data", item)
        return (
            <TouchableOpacity activeOpacity={0} onPress={() => this.handleSelection(item, item.id)}>
                <View style={{ flexDirection: 'row', margin: 10, width: Dimensions.get('window').width - 40, justifyContent: 'space-between', alignSelf: 'center', marginBottom: 10 }}>
                    {item.id == this.state.selectedID && <View>

                        <Image style={{ height: 25, width: 25, resizeMode: 'contain', alignSelf: 'center' }} source={require("../Images/tick_success.png")}></Image>
                        <Text style={{ fontSize: 8 }}>(Tap again to deselect)</Text>
                    </View>}
                    <View style={{ flexDirection: 'row', marginStart: 20 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, textAlignVertical: 'center', justifyContent: 'center', fontWeight: 'bold' }}>{item.inUsd + " USD"}</Text>
                        <Text style={{ fontSize: metrics.text_medium, color: colors.carib_pay_blue, textAlignVertical: 'center', justifyContent: 'center', marginLeft: 10, fontWeight: 'bold' }}>{"( " + this.state.destinationCurrencyCode + " " + item.inLocal + " )"}</Text>
                    </View>
                    <View style={{ width: metrics.dimen_70, borderRadius: metrics.dimen_7, height: 30, justifyContent: 'center' }}>
                        <LinearGradient style={{ width: metrics.dimen_70, borderRadius: metrics.dimen_7, height: 30, justifyContent: 'center' }} colors={['#283142', '#3b5998', '#283142']}>
                            <Text style={{ fontSize: metrics.text_description, color: colors.white, textAlign: 'center', justifyContent: 'center' }}>{"Select"}</Text>
                        </LinearGradient>
                    </View>
                </View>
            </TouchableOpacity>
        )

    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Top-up</Text>
                </View>
                <View style={styles.absouluteview}>

                    <View style={{ flex: 1 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_20 }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ color: '#323232', fontWeight: '800', fontSize: metrics.text_21 }}>{this.state.phone}</Text>
                                <Text style={{ color: colors.app_gray, fontSize: metrics.text_description, marginTop: metrics.dimen_7 }}>{"Prepaid , " + this.state.operatorName}</Text>
                                <TouchableOpacity onPress={() => this.onSelectOperator()}>
                                    <Text style={{ color: colors.carib_pay_blue, fontWeight: 'bold', fontSize: metrics.text_description, marginTop: metrics.dimen_7 }}>Change Operator</Text>
                                </TouchableOpacity>
                            </View>

                            {this.state.operator_logo_url != "" &&
                                // <View style={{ height: metrics.dimen_70, width: metrics.dimen_70, justifyContent: 'center',borderRadius:metrics.dimen_70/2 }}>
                                <Image style={{ height: 70, width: 70, borderRadius: 35, alignSelf: 'center', resizeMode: 'contain' }}
                                    source={{ uri: this.state.operator_logo_url }}></Image>
                            }
                        </View>

                        <View style={{ marginLeft: metrics.dimen_10 }}>
                            <Input
                                placeholder={'0.00'}
                                containerStyle={{ width: "100%", height: metrics.dimen_45 }}
                                keyboardType="number-pad"
                                value={this.state.amount}
                                inputContainerStyle={{ borderBottomWidth: 0.5 }}
                                inputStyle={{ fontSize: 20, color: colors.black }}
                                onChangeText={(text) => this.setState({ amount: text })}>
                            </Input>
                        </View>

                        <View style={{ backgroundColor: colors.transparent, height: 40, marginTop: metrics.dimen_20, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: metrics.text_description, color: colors.heading_black_text, alignSelf: 'center', marginLeft: 10 }}>Browse Plans</Text>
                            <Text style={{ fontSize: metrics.text_description, color: colors.carib_pay_blue, alignSelf: 'center', marginRight: 10 }}>{"For " + this.state.operatorName}</Text>
                        </View>


                        <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_35, alignSelf: 'center', width: '90%', margin: 2, borderRadius: metrics.dimen_10 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromTopup ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromTopup: true, fromPlans: false })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromTopup ? colors.light_grey_backgroud : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromTopup ? colors.black : colors.white, textAlign: 'center' }}>Top up</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', margin: 2, flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPlans ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromTopup: false, fromPlans: true, isComingSoon: true })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPlans ? colors.light_grey_backgroud : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromPlans ? colors.black : colors.white, textAlign: 'center' }}>Plan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>


                        <Spinner style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />


                        <ScrollView style={{ marginBottom: 180 }}
                            showsVerticalScrollIndicator={false}>
                            <FlatList
                                style={{ flex: 0 }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.topups}
                                extraData={this.state.selectedID}
                                keyExtractor={(item, index) => item.toString()}
                                renderItem={({ item, index }) => this.renderTopups(item)}
                            />
                        </ScrollView>



                    </View>



                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.isComingSoon}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 200, justifyContent: 'center' }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                            source={require("../Images/comingsoon.png")}></Image>
                                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center' }}>Plans</Text>
                                    </View>
                                    <View style={styles.bottomview2}>
                                        <TouchableOpacity onPress={() => this.setState({
                                            isComingSoon: false, fromPostPaid: false, fromPrepaid: true
                                        })}>
                                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>





                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />


                    <RBSheet
                        ref={ref => {
                            this.RBSheetAlert = ref;
                        }}
                        height={Dimensions.get('screen').height}
                        duration={0}>
                        {this.renderMoreOperators()}
                    </RBSheet>
                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.onNext()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed to Top-up</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}




const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }

})
