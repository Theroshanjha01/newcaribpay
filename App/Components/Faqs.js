import React from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Dimensions, Image, StyleSheet, BackHandler, ScrollView } from 'react-native';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";

export default class Faqs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>FAQ</Text>
                </View>

                <View style={styles.absouluteview}>
                    <ScrollView style={{ flex: 1, marginBottom: 150 }}
                        showsVerticalScrollIndicator={false}>
                        <View style={{ margin: metrics.dimen_20 }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Collapse>
                                    <CollapseHeader style={styles.collapseHeader}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={styles.cheadText}>Can i use the Card like a credit card ?</Text>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                        <Text style={styles.cbodyText}>Yes, you may use the card to make purchase at all VISA and Mastercard accepting merchants , online purchases and cash withdrawals.However, you can only spend the amount you have preloaded into the the card Account.</Text>
                                    </CollapseBody>
                                </Collapse>
                                <View style={styles.horizontalLine}></View>
                            </View>

                            <View style={{ flexDirection: 'column' }}>
                                <Collapse style={{ marginTop: 10 }}>
                                    <CollapseHeader style={styles.collapseHeader}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={styles.cheadText}>Can i apply for a supplementary Card ?</Text>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                        <Text style={styles.cbodyText}>We regret that supplementary cards are not available for this prepaid Card.</Text>
                                    </CollapseBody>
                                </Collapse>
                                <View style={styles.horizontalLine}></View>
                            </View>

                            <View style={{ flexDirection: 'column' }}>
                                <Collapse style={{ marginTop: 10 }}>
                                    <CollapseHeader style={styles.collapseHeader}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={styles.cheadText}>Does my Card have an Expiry Date ?</Text>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                        <Text style={styles.cbodyText}>Yes, it is printed on your Card.</Text>
                                    </CollapseBody>
                                </Collapse>
                                <View style={styles.horizontalLine}></View>
                            </View>

                            <View style={{ flexDirection: 'column' }}>
                                <Collapse style={{ marginTop: 10 }}>
                                    <CollapseHeader style={styles.collapseHeader}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={styles.cheadText}>What will happen if my card is inactive ?</Text>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                        <Text style={styles.cbodyText}>Your card will be automatically terminated.</Text>
                                    </CollapseBody>
                                </Collapse>
                                <View style={styles.horizontalLine}></View>
                            </View>

                            <View style={{ flexDirection: 'column' }}>
                                <Collapse style={{ marginTop: 10 }}>
                                    <CollapseHeader style={styles.collapseHeader}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={styles.cheadText}>What are the Fees and Charges I have to Pay ?</Text>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                        <Text style={styles.cbodyText}>1. Monthly - Card Holder Fee $2.5/card</Text>
                                        <Text style={styles.cbodyText}>2. ATM Withdrawals - $3.20</Text>
                                        <Text style={styles.cbodyText}>3. Balance Enquiry ATM (network fee are pass-on) $0.80</Text>
                                    </CollapseBody>
                                </Collapse>
                                <View style={styles.horizontalLine}></View>

                            </View>

                            <View style={{ flexDirection: 'column' }}>
                                <Collapse style={{ marginTop: 10 }}>
                                    <CollapseHeader style={styles.collapseHeader}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={styles.cheadText}>How can i obtain my online portal account ?</Text>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                    </CollapseHeader>
                                    <CollapseBody style={styles.collapseBody}>
                                        <Text style={styles.cbodyText}>1. Proceed to www.caribpayintl.com</Text>
                                        <Text style={styles.cbodyText}>2. Click on "SIGN UP" tab on the Home screen and it will guide you accordingly.</Text>
                                        <Text style={styles.cbodyText}>3. Note** Please ensure the "User ID" is the same email address as regesitered in your card application form.</Text>
                                    </CollapseBody>
                                </Collapse>
                                <View style={styles.horizontalLine}></View>

                            </View>

                            <View style={{ flexDirection: 'column' }}>
                            <Collapse style={{ marginTop: 10 }}>
                                <CollapseHeader style={styles.collapseHeader}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.cheadText}>How do i keep track of my spending ?</Text>
                                    <View style={{ justifyContent: 'center' }}>
                                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                                            </View>
                                        </View>
                                </CollapseHeader>
                                <CollapseBody style={styles.collapseBody}>
                                    <Text style={styles.cbodyText}>1. Your statement of account history is made available when you </Text>
                                    <Text style={styles.cbodyText}>2. Click on "SIGN UP" tab on the Home screen and it will guide you accordingly.</Text>
                                    <Text style={styles.cbodyText}>3. Note** Please ensure the "User ID" is the same email address as regesitered in your card application form.</Text>
                                </CollapseBody>
                            </Collapse>
                            <View style={styles.horizontalLine}></View>
                            </View>

                        </View>
                    </ScrollView>
                </View>

            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.5, width: '90%', alignSelf: 'center', marginTop: 10 },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    collapseHeader: { justifyContent: 'center', height: 40, borderRadius: 5 },
    collapseBody: { justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, padding: 10 },
    cbodyText: { fontSize: metrics.text_medium, color: colors.heading_black_text },
    cheadText: { fontSize: metrics.text_description, color: colors.black, paddingHorizontal: 10 },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },

})