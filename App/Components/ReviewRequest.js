import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, TouchableOpacity, Image, Dimensions, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
var Spinner = require('react-native-spinkit');
import { Input, CheckBox } from 'react-native-elements';
const axios = require('axios');
import Toast, { DURATION } from 'react-native-easy-toast';

export default class ReviewRequest extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sendCurrency: this.props.navigation.state.params.sendCurrency,
            user_id: this.props.navigation.state.params.user_id,
            sendAmount: this.props.navigation.state.params.sendAmount,
            emailPhone: this.props.navigation.state.params.emailPhone,
            token: '',
            checked: false,
            note: '',
            fullname: '',
            symbol: '',
            visible:false

        }
    }


    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let token_value = await AsyncStorage.getItem('token');
        let fname = await AsyncStorage.getItem('first_name');
        let lname = await AsyncStorage.getItem('last_name');
        let symbol = await AsyncStorage.getItem('symbol')
        this.setState({
            token: token_value,
            fullname: fname + " " + lname,
            symbol: symbol,
        });


    }



    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }




    onConfirmBtnClick = () => {
        if (this.state.note == "") {
            this.refs.toast.show("Please add note,before cofirm!")
        } else {
            this.setState({
                visible: true
            })
            let postData = {
                user_id: this.state.user_id,
                emailOrPhone: this.state.emailPhone,
                currency_id: this.state.sendCurrency,
                amount: this.state.sendAmount,
                note: this.state.note,
            }
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/request-money-pay',
                data: postData,
                headers: { 'Authorization': this.state.token },
            }).then((response) => {
                console.log("response send money", response.data)
                if (response.data.status == true) {
                    this.setState({
                        visible: false
                    });
                    this.props.navigation.navigate('RequestMoneySuccessfull', { phone: this.state.emailPhone, amount: this.state.sendAmount, remarks: this.state.note, paymentMode: 'Wallet', name: this.state.fullname })

                }
            }).catch((err) => {
                this.setState({
                    visible: false
                })
                alert(err)
            });
        }

    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
              
                <View style={{ width: '90%', alignSelf: 'center', height: metrics.dimen_170, borderRadius: metrics.dimen_8, marginTop: metrics.dimen_40, borderColor: colors.carib_light_grey, borderWidth: 0.4 }}>
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_35, marginBottom: metrics.dimen_10 }}>
                        <Image style={{ height: metrics.dimen_40, width: metrics.dimen_40, alignSelf: 'center', resizeMode: 'contain' }}
                            source={require('../Images/user.png')}></Image>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginHorizontal: metrics.dimen_20 }}>
                            {/* <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>Ben Wilson</Text> */}
                            <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.emailPhone}</Text>
                        </View>
                    </View>

                    <View style={styles.horizontalLine}></View>


                    <View style={{ width: "80%", height: 50, alignSelf: 'center', marginBottom: 10 }}>
                        <View style={{ margin: metrics.dimen_7, alignSelf: 'center', flexDirection: 'row' }}>
                            <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/addnote.png')}></Image>
                            <Input
                                placeholder={'Add Note'}
                                containerStyle={{ alignSelf: 'center', height: 30, marginBottom: 2 }}
                                value={this.state.note}
                                multiline={false}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                inputStyle={{ fontSize: metrics.text_description }}
                                onChangeText={(text) => this.setState({ note: text })}>
                            </Input>
                        </View>
                    </View>
                    <View style={{ height: 40, width: metrics.dimen_150, backgroundColor: colors.carib_pay_blue, position: 'absolute', top: -20, alignSelf: 'center', justifyContent: 'center', borderRadius: metrics.dimen_8 }}>
                        <Text style={{ fontSize: metrics.text_16, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{this.state.symbol == "USD" ? "$ " + parseFloat(this.state.sendAmount).toFixed(2) : "EC$ " + parseFloat(this.state.sendAmount).toFixed(2)}</Text>
                    </View>

                </View>



                <View style={{ flexDirection: 'column', margin: 5 , marginBottom:50}}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Requested Amount</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.black, textAlign: 'center' }}>{this.state.symbol == "USD" ? "$ " + parseFloat(this.state.sendAmount).toFixed(2) : "EC$ " + parseFloat(this.state.sendAmount).toFixed(2)}</Text>
                    </View>
                    <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>
                </View>

                <Spinner style={{ justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />



                <View style={{ ...styles.bottomview, backgroundColor: colors.transparent, bottom: 50 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <CheckBox
                            checkedIcon={<Image source={require('../Images/tick.png')}
                                style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: 3 }}></Image>}
                            uncheckedIcon={<Image source={require('../Images/unchecked.png')}
                                style={{ height: metrics.dimen_20, width: metrics.dimen_20 }}></Image>}
                            containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, marginLeft: -10 }}
                            checked={this.state.checked}
                            onPress={() => this.setState({ checked: !this.state.checked })}
                        />
                        <Text style={{
                            color: colors.heading_black_text,
                            marginLeft: -20, fontSize: metrics.text_description,
                        }}
                            onPress={() => { console.warn("clicked") }}>Terms {'\u0026'} Conditions applied</Text>
                    </View>
                </View>

                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.state.checked ? this.onConfirmBtnClick() : alert('Please agree to our terms and condition.')}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Confirm</Text>
                    </TouchableOpacity>
                </View>


                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'black' }}
                    position='center'
                    positionValue={200}
                    fadeInDuration={200}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }} />

            </SafeAreaView>
        )

    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f2f2f2', borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    headingg: { fontSize: metrics.text_description, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    container: { flexDirection: 'row', marginTop: metrics.dimen_20, marginLeft: metrics.dimen_20 },


})
