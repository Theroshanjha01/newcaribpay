import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity,BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import LinearGradient from 'react-native-linear-gradient';
import ProgressDialog from '../ProgressDialog.js';
import { Input  } from 'react-native-elements';
import Stripe from 'react-native-stripe-api';
import Modal from 'react-native-modal';
const axios = require('axios');


export default class TopupPayStripe extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: this.props.navigation.state.params.amount, fromCard: true, fromBanking: false,
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            operatorID: this.props.navigation.state.params.operatorID,
            country: this.props.navigation.state.params.country,
            phone: this.props.navigation.state.params.phone,
            cvv: '',
            expiryMonth: '',
            expiryYear: '',
            visible: false,
            cardNumber: '',
            stripeKey: '',
            updated_balance: '',
            isVisible: false,
            isVisiblePayment:false
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let postData = { method_id: "2", currency_id: "1" }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/deposit/get-stripe-info',
            data: postData,
            headers: { 'Authorization': this.state.token },
        })
            .then((response) => {
                if (response.data.success.status == 200) {
                    this.setState({
                        stripeKey: response.data.success.stripe_keys.secret_key
                    })
                    console.log(this.state.stripeKey)
                }
            }).catch((err) => {
                alert(err)
            });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    OnProccede = async () => {
        this.setState({ visible: true })
        const apiKey = this.state.stripeKey;
        const client = new Stripe(apiKey);
        const token = await client.createToken({
            number: this.state.cardNumber,
            exp_month: this.state.expiryMonth,
            exp_year: this.state.expiryYear,
            cvc: this.state.cvv,
            address_zip: '12345'
        }).then((response) => {
            if (response) {
                console.log("response", response.id)
                let postData = {
                    amount: this.state.amount,
                    totalAmount: this.state.amount,
                    currency_id: 1,
                    user_id: this.state.user_id,
                    deposit_payment_id: 2,
                    stripeToken: response.id
                }

                console.log("postdatac--->", postData)
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/deposit/stripe-payment-store',
                    data: postData,
                    headers: { 'Authorization': this.state.token },
                }).then((response) => {
                    console.log("stripe-payment ",response.data)
                    if(response.data.status == 200){
                        let postData = { user_id: this.state.user_id }
                                axios.get('https://sandbox.caribpayintl.com/api/get-default-wallet-balance', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
                                    if (response.data.success.status == 200) {
                                        let balance = response.data.success.defaultWalletBalance
                                        let available_bal = balance.substr(4, balance.length)
                                        console.log("balance is ---", available_bal)
                                        this.setState({ updated_balance:available_bal,visible: false, isVisiblePayment:true })
                                    }
                                }).catch((error) => {
                                    console.log("error updated bal---", error);
                                })
                    }
                }).catch((err) => {
                    alert(err)
                    this.setState({ visible: false })
                });
            } else {
                alert(response.error.message)
                this.setState({ visible: false })
            }
        }).catch((err) => {
            console.log(err)
            this.setState({ visible: false })

        })


    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ProgressDialog visible={this.state.visible} />
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Debit/Credit Card</Text>
                </View>

                <View style={styles.absouluteview}>
                    <LinearGradient colors={['#b4e6f1', '#d7f3f7', '#f9fdff']} style={{ flex: 1 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: colors.light_grey_backgroud, height: metrics.dimen_35 }}>
                            <Text style={{ color: colors.black, fontSize: metrics.text_normal, fontWeight: '600', marginLeft: metrics.dimen_10, textAlignVertical: 'center' }}>Mobile Reload</Text>
                            <Text style={{ color: colors.black, fontSize: metrics.text_normal, fontWeight: '600', marginRight: metrics.dimen_10, textAlignVertical: 'center' }}>{"$ " + this.state.amount}</Text>
                        </View>

                        <View style={{ flex: 1, margin: metrics.dimen_15 }}>
                            <View style={{ flexDirection: 'row', backgroundColor: '#DCDCDC', height: metrics.dimen_30, alignSelf: 'center', width: '100%', margin: 2, borderRadius: metrics.dimen_10 }}>
                                <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromCard ? colors.white : colors.transparent }} onPress={() => this.setState({ fromCard: true, fromBanking: false })}>
                                    <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromCard ? colors.white : colors.transparent }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black, textAlign: 'center' }}>Credit/Debit Card</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromBanking ? colors.white : colors.transparent }} onPress={() => this.setState({ fromCard: false, fromBanking: true })}>
                                    <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromBanking ? colors.white : colors.transparent }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black, textAlign: 'center' }}>Online Banking</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: 'column', margin: metrics.dimen_15 }}>
                                <Input
                                    placeholder={'1245 5634 9980'}
                                    containerStyle={{ width: "100%", height: 50, borderColor: colors.app_gray, borderWidth: 0.5, borderRadius: 10 }}
                                    keyboardType="number-pad"
                                    rightIcon={() => <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/card.png')}></Image>}
                                    value={this.state.cardNumber}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: metrics.dimen_22 }}
                                    onChangeText={(text) => this.setState({ cardNumber: text })}>
                                </Input>

                                <View style={{ flexDirection: 'row', width: '100%', borderColor: colors.app_gray, borderWidth: 0.5, height: 50, marginTop: 10, borderRadius: 10 }}>
                                    <Input
                                        placeholder={'MM'}
                                        containerStyle={{ flex: 0.5, width: '40%', height: 50, borderRadius: 10 }}
                                        keyboardType="number-pad"
                                        maxLength={2}
                                        value={this.state.expiryMonth}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.dimen_22 }}
                                        onChangeText={(text) => this.setState({
                                            expiryMonth: text
                                        })}>
                                    </Input>

                                    <View
                                        style={{
                                            borderLeftWidth: 1,
                                            height: 35,
                                            alignSelf: 'center',
                                            borderLeftColor: colors.app_gray,
                                        }}></View>

                                    <Input
                                        placeholder={'YY'}
                                        containerStyle={{ flex: 0.5, width: '40%', height: 50, borderRadius: 10 }}
                                        keyboardType="number-pad"
                                        maxLength={2}
                                        value={this.state.expiryYear}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.dimen_22 }}
                                        onChangeText={(text) => this.setState({
                                            expiryYear: text
                                        })}>
                                    </Input>

                                    <View
                                        style={{
                                            borderLeftWidth: 1,
                                            height: 35,
                                            alignSelf: 'center',
                                            borderLeftColor: colors.app_gray,
                                        }}></View>

                                    <Input
                                        placeholder={'CVV'}
                                        containerStyle={{ width: '40%', flex: 0.5, height: 50, alignSelf: 'center', borderRadius: 10 }}
                                        keyboardType="number-pad"
                                        value={this.state.cvv}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        inputStyle={{ fontSize: metrics.dimen_22 }}
                                        onChangeText={(text) => this.setState({ cvv: text })}>
                                    </Input>

                                </View>


                            </View>

                            <Modal isVisible={this.state.isVisiblePayment}>
                                <View style={{ width: '90%', backgroundColor: colors.white, borderRadius: metrics.dimen_7 }}>
                                    <Image style={{ height: metrics.dimen_50, width: metrics.dimen_50, alignSelf: 'center', resizeMode: 'contain', marginTop: 10 }}
                                        source={require('../Images/success.png')}></Image>
                                    <Text style={{ textAlign: 'center', color: colors.black, fontWeight: 'bold', fontSize: metrics.text_16, marginTop: 5 }}>{"Recharge Successfully!"}</Text>


                                    <View style={{ flexDirection: 'column', margin: 15 }}>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{this.state.operatorName}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', margin: 15 }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black }}>Recipient Phone </Text>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{this.state.recipientPhone}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', margin: 15 }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black }}>Reacharge Amount</Text>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{"$" + this.state.amount + " USD"}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', margin: 15 }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black }}>Payment Mode</Text>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{"Debit/Credit Card"}</Text>
                                    </View>


                                    <TouchableOpacity onPress={() => this.setState({ isVisible: false }, () => this.props.navigation.navigate('Home'))}>
                                        <View style={{ width: '80%', alignSelf: 'center', backgroundColor: colors.carib_pay_blue, margin: 10, justifyContent: 'center', height: metrics.dimen_30, borderRadius: metrics.dimen_7 }}>
                                            <Text style={{ textAlign: 'center', fontSize: metrics.text_description, color: colors.white, fontWeight: 'bold' }}>Home</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            </Modal>








                        </View>


                    </LinearGradient>
                </View>
                <View style={{ ...styles.bottomview, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => this.OnProccede()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Proceed</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }
})
