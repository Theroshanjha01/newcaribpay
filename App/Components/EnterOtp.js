import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, BackHandler, Image, Dimensions, SafeAreaView } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import Toast, { DURATION } from 'react-native-easy-toast';
const axios = require('axios');
var Spinner = require('react-native-spinkit');
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import Modal from 'react-native-modal';

export default class EnterOtp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            userdata: this.props.navigation.state.params.item,
            otp: this.props.navigation.state.params.otp,
            code: '',
            remainingTime: 60,
            tryAgain: false,
            visible: false
        }
    }



    componentDidMount() {
        console.log("otp :: ", this.state.otp, this.state.userdata[0].user_formattedPhone)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        setInterval(() => {
            this.setState(prevState => {
                if (this.state.remainingTime > 0)
                    return { remainingTime: prevState.remainingTime - 1 }
                else {
                    return null
                }
            });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack()
        return true;
    }


    onOTPVerify = () => {
        if (this.state.code == "") {
            this.refs.toast.show("OTP Required.", DURATION.LENGTH_LONG);
        } else {
            let otp_entered = this.state.code
            if (otp_entered == this.state.otp) {
                this.setState({
                    visible: true
                });
                let data = []
                data = this.state.userdata
                let postData = {
                    first_name: data[0].fname,
                    last_name: data[0].lname,
                    carrierCode: data[0].user_countryCode,
                    email: data[0].user_email,
                    formattedPhone: data[0].user_formattedPhone,
                    password: data[0].user_password,
                    phone: data[0].user_phone_number,
                    type: data[0].user_type
                }

                console.log("postData", postData)
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/registration-with-otp',
                    data: postData,
                })
                    .then((response) => {
                        console.log(response.data.success.status)
                        if (response.data.success.status == 200) {
                            let id = response.data.success.data.id
                            this.setState({ visible: false });
                            this.props.navigation.navigate('RegistrationSuccesfull', { isReset: "0", user_id: id })
                        }
                    })
                    .catch(error => {
                        if (error.response) {
                            console.log("response  errorn-- > ", error.response.status)
                            if (error.response.status == 401) {
                                this.setState({
                                    tryAgain: true,
                                    visible: false
                                })
                            }
                        }

                    });
            }
            else {
                this.refs.toast.show("Something went wrong!", DURATION.LENGTH_LONG);

            }
        }
    }

    onConfirmClick = () => {
        this.setState({ visible: true })
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/send-otp',
            data: { phone: this.state.userdata[0].user_formattedPhone },
        })
            .then((response) => {
                if (response.data.response.status == 200) {
                    console.log(response.data.response.otp)
                    this.setState({ remainingTime: 60, visible: false, otp: response.data.response.otp })
                }
            })
            .catch((err) => {
                this.setState({ visible: false })
            });
    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Enter OTP</Text>
                </View>

                <View style={styles.absouluteview}>
                    <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_normal, textAlign: 'center', marginTop: 20 }}>{"Enter Your 4-digit OTP sent on \n" + this.state.userdata[0].user_formattedPhone}</Text>
                    <View style={{ marginTop: 15, alignSelf: 'center', marginBottom: 15 }}>
                        <SmoothPinCodeInput
                            placeholder=""
                            cellSize={55}
                            cellSpacing={7}
                            keyboardType='phone-pad'
                            cellStyle={{
                                borderWidth: 2,
                                borderRadius: 7,
                                borderColor: colors.theme_caribpay,
                                backgroundColor: 'azure',
                            }}
                            cellStyleFocused={{
                                borderColor: 'lightseagreen',
                                backgroundColor: 'lightcyan',
                            }}
                            textStyle={{
                                fontSize: 24,
                                color: colors.black,
                                fontWeight: 'bold'
                            }}
                            textStyleFocused={{
                                color: 'crimson'
                            }}
                            value={this.state.code}
                            // onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                            onTextChange={code => this.setState({ code })}
                        />
                    </View>

                    <Text style={styles.info}>Once your phone number is verified,your account will be created.</Text>
                    {this.state.remainingTime > 0 && <Text style={{ textAlignVertical: 'center', textAlign: 'center', color: colors.carib_pay_blue, fontWeight: '400', marginBottom: metrics.dimen_40 }}>{this.state.remainingTime + " Seconds Left"}</Text>}

                    <TouchableOpacity onPress={() => this.onOTPVerify()}>
                        <View elevation={1} style={styles.buttonStyle}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Verify</Text>
                        </View>
                    </TouchableOpacity>

                    {this.state.remainingTime == 0 &&
                        <TouchableOpacity onPress={() => this.onConfirmClick()}>
                            <Text style={{ ...styles.info, marginTop: metrics.dimen_20 }}>Not received your code ? <Text style={{ ...styles.info, color: colors.carib_pay_blue }}>Resend Code</Text></Text>
                        </TouchableOpacity>}


                    <Spinner style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 30 }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />


                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position='center'
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }} />

                    <View>
                        <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.tryAgain}>
                            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 350, justifyContent: 'center' }}>
                                <View style={{ flex: 1 }}>

                                    <View>
                                        <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                            source={require("../Images/logo.png")}></Image>
                                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>You already have an existing account</Text>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 15, fontWeight: 'bold', textAlign: 'center' }}>Try again with different Email ID or Mobile No.</Text>
                                    </View>
                                    <View style={styles.bottomview}>
                                        <TouchableOpacity onPress={() => this.setState({ tryAgain: false }, () => this.props.navigation.navigate("Register"))}>
                                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Try Again</Text>
                                        </TouchableOpacity>
                                    </View>


                                </View>
                            </View>
                        </Modal>
                    </View>

                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    info: {
        textAlign: 'center', fontSize: metrics.text_description, fontFamily: metrics.quicksand_bold, color: colors.heading_black_text, marginBottom: metrics.dimen_20, width: "60%", alignSelf: 'center'
    }, buttonStyle: {
        width: '80%',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        height: 40,
        backgroundColor: colors.theme_caribpay,
        shadowColor: "#f2f2f2",
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    bottomview: { bottom: 20, position: 'absolute', height: 50, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, textAlignVertical: 'center' },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' }
});
