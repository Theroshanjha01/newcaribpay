import React from 'react';
import { Text, View, StyleSheet, Dimensions, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
import { Input } from 'react-native-elements';
const axios = require('axios');
import { showAlert } from '../Utils.js';


export default class ResetPassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            newPassword: '',
            confirmNewPassword: '',
            visible: false,
            email: this.props.navigation.state.params.email,
            phone: this.props.navigation.state.params.phone,
            fieldType: this.props.navigation.state.params.fieldType,
            show1: true,
            show2: true
        }
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    onSubmitClick = () => {
        if (this.state.newPassword == "") {
            showAlert("CaribPay", "Required New Password.")
        } else if (this.state.confirmNewPassword == "") {
            showAlert("CaribPay", "Required Confirm Password.")
        } else if (this.state.confirmNewPassword != this.state.newPassword) {
            showAlert("CaribPay", "Oops! Password Mismatchs.")
        } else {
            this.setState({ visible: true })
            let postData = { [this.state.fieldType ? 'email' : 'email']: this.state.phone, password: this.state.confirmNewPassword }
            console.log(postData)
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/reset-password',
                data: postData,
            }).then((response) => {
                console.log(response.data)
                if (response.data.status == 200) {
                    this.setState({ visible: false })
                    this.props.navigation.navigate("RegistrationSuccesfull", { isReset: "1", item: '' })
                } else {
                    this.setState({ visible: false })
                    alert(response.data.message)
                }
            }).catch((err) => {
                this.setState({ visible: false })
                alert(err)
            });
        }

    }


    render() {
        return (
            <View style={{ flex: 1 }}>
               
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Reset Password</Text>
                </View>

                <View style={styles.absouluteview}>
                    <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image>
                    <Text style={{ fontSize: metrics.text_normal, textAlign: 'center', color: colors.carib_pay_blue, }}>Enter Your New Password</Text>


                    <Input
                        placeholder={'New Password'}
                        containerStyle={{ width: '90%', height: metrics.dimen_50, borderColor: colors.app_gray, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10, alignSelf: 'center' }}
                        value={this.state.newPassword}
                        secureTextEntry={this.state.show1?true:false}
                        leftIcon={
                            <Image style={{ height: 20, width: 20 }} source={require('../Images/key.png')}>
                            </Image>
                        }
                        rightIcon={
                            <TouchableOpacity onPress={() => this.setState({ show1: !this.state.show1 })}>
                                <Image style={{ height: 20, width: 20, marginRight: 10 }} source={this.state.show1 ? require('../Images/hide.png') : require('../Images/eye.png')}>
                                </Image>
                            </TouchableOpacity>
                        }
                        inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                        inputStyle={{ fontSize: 13 }}
                        onChangeText={(text) => this.setState({ newPassword: text })}>
                    </Input>


                    <Input
                        placeholder={'Confirm New Password'}
                        containerStyle={{ width: '90%', height: metrics.dimen_50, borderColor: colors.app_gray, borderWidth: 1, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10, alignSelf: 'center' }}
                        value={this.state.confirmNewPassword}
                        secureTextEntry={this.state.show2 ? true : false}
                        leftIcon={
                            <Image style={{ height: 20, width: 20 }} source={require('../Images/key.png')}>
                            </Image>
                        }
                        rightIcon={
                            <TouchableOpacity onPress={() => this.setState({ show2: !this.state.show2 })}>
                                <Image style={{ height: 20, width: 20, marginRight: 10 }} source={this.state.show2 ? require('../Images/hide.png') : require('../Images/eye.png')}>
                                </Image>
                            </TouchableOpacity>
                        }
                        inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center' }}
                        inputStyle={{ fontSize: 13 }}
                        onChangeText={(text) => this.setState({ confirmNewPassword: text })}>
                    </Input>


                    <Spinner style={{ justifyContent: 'center', alignSelf: 'center'}} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />

                </View>



                <View style={{ ...styles.bottomview, backgroundColor: colors.carib_pay_blue }}>
                    <TouchableOpacity onPress={() => this.onSubmitClick()}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>SUBMIT</Text>
                    </TouchableOpacity>
                </View>

            </View>

        )
    }
}




const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    imageStyle: {
        height: metrics.dimen_200, width: metrics.dimen_200, alignSelf: 'center', resizeMode: 'contain', marginTop: 5
    },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }
})
