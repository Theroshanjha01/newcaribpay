import * as React from 'react';
import { View, StyleSheet, Dimensions, Image, Text } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';

const FirstRoute = () => (
    <View style={[styles.scene, { backgroundColor: colors.light_grey_backgroud }]}>
        <Image style={{ height: 100, width: 100, resizeMode: 'contain', alignSelf: 'center' }}
            source={require("../Images/chest.png")}></Image>
        <Text style={{ fontSize: metrics.text_heading, color: colors.app_gray, textAlign: 'center', margin: 20 }}>Item Not Found</Text>
    </View>
);

const SecondRoute = () => (
    <View style={[styles.scene, { backgroundColor: colors.light_grey_backgroud }]}>
        <Image style={{ height: 100, width: 100, resizeMode: 'contain', alignSelf: 'center' }}
            source={require("../Images/chest.png")}></Image>
        <Text style={{ fontSize: metrics.text_heading, color: colors.app_gray, textAlign: 'center', margin: 20 }}>Item Not Found</Text>

    </View>

);

const initialLayout = { width: Dimensions.get('window').width };

export default function TabViewExample() {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'ACTIVE' },
        { key: 'second', title: 'USED/EXPIRED' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });

    return (
        <TabView
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: 'pink' }}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
}

const styles = StyleSheet.create({
    scene: {
        flex: 1,
        justifyContent: 'center'
    },
});