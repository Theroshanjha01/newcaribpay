import React from 'react';
import { SafeAreaView, Text, View, Image, Dimensions, StyleSheet, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';

export default class AccountVerification extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: this.props.navigation.state.params.user_id,
            fromSignup:this.props.navigation.state.params.fromSignup,
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View>
                    <Image
                        source={require("../Images/kychead.png")}
                        style={{ height: 150, width: Dimensions.get('window').width, resizeMode: 'contain', marginTop: -20 }}></Image>

                    <Text style={{ fontSize: metrics.text_large, color: colors.theme_caribpay, fontWeight: 'bold', marginHorizontal: 10 }}>Verify Now to enjoy more features!</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: "#e8f4f8", height: metrics.dimen_150 }}>
                        <View style={{ height: 50, width: 50, borderRadius: 25, justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, marginLeft: 10, marginTop: 10 }}>
                            <Image style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center' }} source={require('../Images/shield.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', marginLeft: 10, marginTop: 10 }}>
                            <Text style={styles.headingg}>Money Back Guarantee</Text>
                            <Text style={styles.descriiption}>Verify your Carib Pay Account and get protected from unauthorised transactions.</Text>
                            <Text style={{ marginTop: 20, marginHorizontal: 70, color: colors.theme_caribpay, fontWeight: 'bold', fontSize: metrics.text_header }}>More Features</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 40, alignSelf: 'center', marginRight: 10 }}>
                        <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', backgroundColor: colors.theme_caribpay, alignSelf: 'center' }}>
                                <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', alignSelf: 'center' }}>1</Text>
                            </View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.theme_caribpay, alignSelf: 'center', textAlign: 'center', marginTop: 5, fontWeight: 'bold' }}>Upload Photo</Text>
                        </View>
                        <View style={{ ...styles.horizontalLine, marginTop: -40 }}></View>
                        <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', backgroundColor: colors.theme_caribpay, alignSelf: 'center' }}>
                                <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', alignSelf: 'center' }}>2</Text>
                            </View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.theme_caribpay, alignSelf: 'center', textAlign: 'center', marginTop: 5, fontWeight: 'bold' }}>Upload ID Proof</Text>
                        </View>
                        <View style={{ ...styles.horizontalLine, marginTop: -40 }}></View>
                        <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', backgroundColor: colors.theme_caribpay, alignSelf: 'center' }}>
                                <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', alignSelf: 'center' }}>3</Text>
                            </View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.theme_caribpay, alignSelf: 'center', textAlign: 'center', marginTop: 5, fontWeight: 'bold' }}>Upload Address Proof</Text>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 50, marginLeft: 20 }}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center' }} source={require('../Images/shield.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                            <Text style={{ fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', }}>Feel Safe</Text>
                            <Text style={{ ...styles.descriiption, width: 300, alignSelf: 'center' }}>Rest assured that your transactions and data are kept secure and confidential</Text>
                        </View>
                    </View>

                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("UpdateKYC", { user_id: this.state.user_id, fromSignup:this.state.fromSignup })}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Verify Account</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView >
        )
    }


}

const styles = StyleSheet.create({
    headingg: { fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', marginBottom: 5 },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_30 },
    horizontalLine: { borderBottomColor: colors.theme_caribpay, borderBottomWidth: 0.4, width: 40, alignSelf: 'center', justifyContent: 'center' },


})