import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, TouchableOpacity, Image, Dimensions, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
var Spinner = require('react-native-spinkit');
import { Input, CheckBox } from 'react-native-elements';
const axios = require('axios');
import Toast, { DURATION } from 'react-native-easy-toast';

export default class ReviewSend extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sendCurrency: this.props.navigation.state.params.sendCurrency,
            user_id: this.props.navigation.state.params.user_id,
            sendAmount: this.props.navigation.state.params.sendAmount,
            available_balance: this.props.navigation.state.params.balance,
            emailPhone: this.props.navigation.state.params.emailPhone,
            token: '',
            total_amount_pay: '',
            txn_fee: '',
            checked: false,
            note: '',
            fullname: '',
            txn_ref_id: this.props.navigation.state.params.txn_ref_id, totalFees: this.props.navigation.state.params.totalFees
        }
    }


    createUniqueCode(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }




    async componentDidMount() {


        let fromTxnList = this.props.navigation.state.params.fromTxnList
        console.log("fromTxn list rs == ", fromTxnList)


        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({
            visible: true
        })
        let token_value = await AsyncStorage.getItem('token');
        let fname = await AsyncStorage.getItem('first_name');
        let lname = await AsyncStorage.getItem('last_name');

        this.setState({
            token: token_value,
            fullname: fname + " " + lname
        });
        console.log("info coming -->", this.state.user_id, this.state.sendCurrency, this.state.sendAmount)
        let postData = { sendCurrency: this.state.sendCurrency, user_id: this.state.user_id, sendAmount: this.state.sendAmount }
        console.log(postData)
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/check-send-money-amount-limit',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            console.log("send money feee response ---> ", response.data)
            this.setState({
                total_amount_pay: response.data.success.totalAmountDisplay,
                txn_fee: response.data.success.totalFeesDisplay,
                visible: false
            })
        }).catch((err) => {
            this.setState({
                visible: false
            })
            alert(err)
        });

    }



    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }




    onConfirmBtnClick = () => {
        let fromTxnList = this.props.navigation.state.params.fromTxnList
        console.log("fromTxn list == ", fromTxnList)
        if (fromTxnList == '0') {
            if (this.state.note == "") {
                this.refs.toast.show("Please add note,before cofirm!")
            } else {
                this.setState({
                    visible: true
                })
                let postData = {
                    user_id: this.state.user_id,
                    emailOrPhone: this.state.emailPhone,
                    currency_id: this.state.sendCurrency,
                    amount: this.state.sendAmount,
                    totalFees: this.state.txn_fee,
                    total_with_fee: this.state.total_amount_pay,
                    note: this.state.note,
                    unique_code: this.createUniqueCode(9)
                }
                console.log("post data money send", postData)
                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/send-money-pay',
                    data: postData,
                    headers: { 'Authorization': this.state.token },
                }).then((response) => {
                    console.log("response send money", response.data)
                    if (response.data.status == true) {
                        this.setState({
                            visible: false
                        });
                        this.props.navigation.navigate('sendMoneySuccessfull', { phone: this.state.emailPhone, amount: this.state.sendAmount, remarks: this.state.note, paymentMode: 'Wallet', name: this.state.fullname })
                    }
                }).catch((err) => {
                    this.setState({
                        visible: false
                    })
                    alert(err)
                });
            }
        } else {
            this.setState({
                visible: true
            })
            let postData = {
                tr_ref_id: this.state.txn_ref_id,
                amount: this.state.sendAmount,
                currency_id: this.state.sendCurrency,
                user_id: this.state.user_id,
                totalFee: this.state.totalFees,
                tr_email_or_phone: this.state.emailPhone
            }
            console.log("txn post data accept payemnt  params ---", postData)
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/accept-request-payment-pay',
                data: postData,
                headers: { 'Authorization': this.state.token },
            }).then((response) => {
                if (response.data.status == true) {
                    this.setState({
                        visible: false
                    })
                    this.props.navigation.navigate('sendMoneySuccessfull', { phone: this.state.emailPhone, amount: this.state.sendAmount, remarks: this.state.note, paymentMode: 'Wallet', name: this.state.fullname })
                }
            }).catch((err) => {
                this.setState({
                    visible: false
                })
                alert(err)
            });
        }


    }


    render() {
        console.log(this.state.total_amount_pay, this.state.txn_fee)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
                <View style={{ width: '90%', alignSelf: 'center', height: metrics.dimen_170, borderRadius: metrics.dimen_8, marginTop: metrics.dimen_40, borderColor: colors.carib_light_grey, borderWidth: 0.4 }}>
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_35, marginBottom: metrics.dimen_10 }}>
                        <Image style={{ height: metrics.dimen_40, width: metrics.dimen_40, alignSelf: 'center', resizeMode: 'contain' }}
                            source={require('../Images/user.png')}></Image>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginHorizontal: metrics.dimen_20 }}>
                            {/* <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>Ben Wilson</Text> */}
                            <Text style={{ fontSize: metrics.text_description, color: colors.app_gray }}>{this.state.emailPhone}</Text>
                        </View>
                    </View>

                    <View style={styles.horizontalLine}></View>


                    <View style={{ width: "80%", height: 50, alignSelf: 'center', marginBottom: 10 }}>
                        <View style={{ margin: metrics.dimen_7, alignSelf: 'center', flexDirection: 'row' }}>
                            <Image style={{ height: metrics.dimen_20, width: metrics.dimen_20, resizeMode: 'contain', alignSelf: 'center' }} source={require('../Images/addnote.png')}></Image>
                            <Input
                                placeholder={'Add Note'}
                                containerStyle={{ alignSelf: 'center', height: 30, marginBottom: 2 }}
                                value={this.state.note}
                                multiline={false}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                inputStyle={{ fontSize: metrics.text_description }}
                                onChangeText={(text) => this.setState({ note: text })}>
                            </Input>
                        </View>
                    </View>
                    <View style={{ height: 40, width: metrics.dimen_150, backgroundColor: colors.carib_pay_blue, position: 'absolute', top: -20, alignSelf: 'center', justifyContent: 'center', borderRadius: metrics.dimen_8 }}>
                        <Text style={{ fontSize: metrics.text_16, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{this.state.sendCurrency == "1" ? "$ " + parseFloat(this.state.sendAmount).toFixed(2) : "EC$ " + parseFloat(this.state.sendAmount).toFixed(2)}</Text>
                    </View>

                </View>


                <View style={styles.container}>
                    <View>
                        <Image style={{ height: metrics.dimen_40, width: metrics.dimen_40, resizeMode: 'contain' }} source={require('../Images/logo.png')}></Image>
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20 }}>
                        <Text style={styles.headingg}>Wallet</Text>
                        <Text style={styles.descriiption}>Available Balance <Text style={{ ...styles.descriiption, color: colors.carib_pay_blue }}>{this.state.sendCurrency == "1" ? "$ " + this.state.available_balance : "EC$ " + this.state.available_balance}</Text></Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'column', margin: 5 }}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Transfer Amount</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.black, textAlign: 'center' }}>{this.state.sendCurrency == "1" ? "$ " + parseFloat(this.state.sendAmount).toFixed(2) : "EC$ " + parseFloat(this.state.sendAmount).toFixed(2)}</Text>
                    </View>
                    <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Fee</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.black, textAlign: 'center' }}>{this.state.sendCurrency == "1" ? "$ " + this.state.txn_fee : "EC$ " + this.state.txn_fee}</Text>
                    </View>
                    <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>


                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_15 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>Total</Text>
                        <Text style={{ fontSize: metrics.text_description, color: colors.carib_pay_blue, textAlign: 'center' }}>{this.state.sendCurrency == "1" ? "$ " + this.state.total_amount_pay : "EC$ " + this.state.total_amount_pay}</Text>
                    </View>
                    <View style={{ borderBottomColor: '#D3D3D3', borderBottomWidth: 1, width: '90%', alignSelf: 'center' }}></View>

                </View>



                <Spinner style={{  justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />




                <View style={{ ...styles.bottomview, backgroundColor: colors.transparent, bottom: 50 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <CheckBox
                            checkedIcon={<Image source={require('../Images/tick.png')}
                                style={{ height: metrics.dimen_20, width: metrics.dimen_20, borderRadius: 3 }}></Image>}
                            uncheckedIcon={<Image source={require('../Images/unchecked.png')}
                                style={{ height: metrics.dimen_20, width: metrics.dimen_20 }}></Image>}
                            containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, marginLeft: -10 }}
                            checked={this.state.checked}
                            onPress={() => this.setState({ checked: !this.state.checked })}
                        />
                        <Text style={{
                            color: colors.heading_black_text,
                            marginLeft: -20, fontSize: metrics.text_description,
                        }}
                            onPress={() => { console.warn("clicked") }}>Terms {'\u0026'} Conditions applied</Text>
                    </View>
                </View>

                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => this.state.checked ? this.onConfirmBtnClick() : alert('Please agree to our terms and condition.')}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Confirm</Text>
                    </TouchableOpacity>
                </View>


                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'black' }}
                    position='center'
                    positionValue={200}
                    fadeInDuration={200}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }} />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f2f2f2', borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.carib_pay_blue, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    headingg: { fontSize: metrics.text_description, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    container: { flexDirection: 'row', marginTop: metrics.dimen_20, marginLeft: metrics.dimen_20 },
})
