import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import ProgressDialog from '../ProgressDialog.js';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
const axios = require('axios');
import Modal from 'react-native-modal';




export default class PasswordOTP extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            otp: this.props.navigation.state.params.otp,
            code: '',
            data: this.props.navigation.state.params.data,
            phone: this.props.navigation.state.params.phone,
            fieldType: this.props.navigation.state.params.fieldType,
            remainingTime: 60,
            invalid: false

        }
    }



    componentDidMount() {
        console.log("data", this.state.data)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        setInterval(() => {
            this.setState(prevState => {
                if (this.state.remainingTime > 0)
                    return { remainingTime: prevState.remainingTime - 1 }
                else {
                    return null
                }
            });
        }, 1000);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        clearInterval();
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    onConfirmClick = () => {
        this.setState({ visible: true })
        let data = this.state.data
        let postData = { email: data }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/forgot-password',
            data: postData,
        }).then((response) => {
            console.log(response.data)
            if (response.data.status == 200) {
                console.log("otp", response.data.otp)
                this.setState({ remainingTime: 60, visible: false })
            } else if (response.data.status == 201) {
                this.setState({ visible: false })
                this.setState({
                    invalid: true
                })
            } else {
                this.setState({ visible: false })
                // showAlert("CaribPay", "Something went wrong")
            }
        }).catch((err) => {
            this.setState({ visible: false })
        });
    }

    onOTPVerify = () => {
        if (this.state.code == "") {
            showAlert("CaribPay", "Proper OTP Required!")
        } else {
            let otp_entered = this.state.code
            if (otp_entered == this.state.otp) {
                this.props.navigation.navigate("ResetPassword", { email: this.state.data, phone: this.state.phone, fieldType: this.state.fieldType });
            } else {
                this.setState({
                    invalid:true
                })
            }
        }

    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ProgressDialog visible={this.state.visible} />
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ margin: metrics.dimen_20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold' }}>Enter OTP</Text>
                    </View>
                </View>

                <View style={styles.curveview}>
                    <Text style={{ textAlignVertical: 'center', textAlign: 'center', color: colors.theme_caribpay, fontWeight: 'bold', marginTop: metrics.dimen_20 }}>{"Enter your 4-Digit OTP sent on \n" + this.state.data}</Text>
                    <View style={{ marginTop: 30, alignSelf: 'center', marginBottom: 30 }}>
                        <SmoothPinCodeInput
                            placeholder=""
                            cellSize={55}
                            cellSpacing={7}
                            keyboardType='phone-pad'
                            cellStyle={{
                                borderWidth: 2,
                                borderRadius: 7,
                                borderColor: colors.theme_caribpay,
                                backgroundColor: colors.light_grey_backgroud,
                            }}
                            cellStyleFocused={{
                                borderColor: 'lightseagreen',
                                backgroundColor: 'lightcyan',
                            }}
                            textStyle={{
                                fontSize: 24,
                                color: colors.black,
                                fontWeight: 'bold'
                            }}
                            textStyleFocused={{
                                color: 'crimson'
                            }}
                            value={this.state.code}
                            // onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                            onTextChange={code => this.setState({ code })}
                        />
                    </View>

                    <Text style={{ textAlignVertical: 'center', textAlign: 'center', color: colors.heading_black_text, width: 300, marginBottom: metrics.dimen_40, alignSelf: 'center' }}>Once your phone number is verified, you can reset your password.</Text>
                    {this.state.remainingTime > 0 && <Text style={{ textAlignVertical: 'center', textAlign: 'center', color: colors.theme_ornage_color, fontWeight: '400', marginBottom: metrics.dimen_40 }}>{this.state.remainingTime + " Seconds Left"}</Text>}

                    <TouchableOpacity onPress={() => this.onOTPVerify()}>
                        <View elevation={1} style={styles.buttonStyle}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Verify</Text>
                        </View>
                    </TouchableOpacity>


                    {this.state.remainingTime == 0 &&
                        <TouchableOpacity onPress={() => this.onConfirmClick()}>
                            <Text style={{ ...styles.info, marginTop: metrics.dimen_20 }}>Not received your code ? <Text style={{ ...styles.info, color: colors.theme_caribpay }}>Resend Code</Text></Text>
                        </TouchableOpacity>}

                </View>






                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid} >
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"INVALID!"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                {/* <Text style={{ color: 'red', fontSize: metrics.text_medium, textAlign: 'center' }}>{"You have made " + this.state.loginAttempts + " unsuccessfull attempts(s)" + "\n Remaining Attempt(s) : " + (3 - parseFloat(this.state.loginAttempts)) + " "}</Text> */}
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"You have entered wrong OTP!"}</Text>
                                <TouchableOpacity onPress={() => this.setState({
                                    invalid: false
                                })}>
                                    <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"Retry"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    imageStyle: {
        height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain', marginTop: 5
    },
    textStyle: {
        color: colors.app_gray, fontSize: metrics.text_description, marginLeft: 15, marginTop: 15
    },
    buttonStyle: {
        width: '80%',
        borderRadius: metrics.dimen_10,
        justifyContent: 'center',
        alignSelf: 'center',
        height: 40,
        backgroundColor: colors.theme_caribpay,
        shadowColor: "#f2f2f2",
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    info: {
        textAlign: 'center', fontSize: metrics.text_description, color: colors.carib_pay_blue, marginBottom: metrics.dimen_20, width: "60%", alignSelf: 'center'
    },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop, width: "100%", backgroundColor: colors.white, borderRadius: 40 }
})
