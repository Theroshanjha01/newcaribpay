import React from 'react';
import { Keyboard, Text, View, StyleSheet, Image, TouchableOpacity, Dimensions, FlatList, BackHandler, SafeAreaView, ScrollView } from 'react-native';
import { Input } from 'react-native-elements';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import Toast, { DURATION } from 'react-native-easy-toast';
import RBSheet from "react-native-raw-bottom-sheet";
import AsyncStorage from '@react-native-community/async-storage';
const axios = require('axios');
import { showAlert } from '../Utils.js';
import CountryData from 'country-data';
import TouchID from 'react-native-touch-id';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import Modal from 'react-native-modal';
import RadioButton from 'react-native-radio-button';
var Spinner = require('react-native-spinkit');

const optionalConfigObject = {
    title: 'Biometric Authentication', // Android
    imageColor: colors.app_dark_green_color, // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'Touch the fingerprint sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Androi
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            currencycode: '',
            email: '',
            phone: '',
            password: '',
            hide: true,
            fromMobilNo: true,
            fromEmail: false,
            countryDataSource: [],
            newData: [], text: '',
            arrayholder: [],
            showAlert: true,
            isoName: '',
            passcode: '',
            code: '',
            wrong: false,
            invalid: false,
            userblocked: false,
            showKeyboard: false,
            loginAttempts: 0
        }
        this._keyboardDidHide = this._keyboardDidHide.bind(this);
        this._keyboardDidShow = this._keyboardDidShow.bind(this);

    }

    async componentDidMount() {
        axios.get('https://topups.reloadly.com/countries').then((response) => {
            AsyncStorage.setItem('countryDataSource', JSON.stringify(response.data))
            this.setState({
                countryDataSource: response.data,
            });
            this.state.arrayholder = response.data
        }).catch((err) => {
            console.warn(err)
        });

        axios.get('https://ipinfo.io/json').then((response) => {
            let country_code = response.data.country;
            let countrycallingcode = JSON.stringify(CountryData.countries[country_code].countryCallingCodes[0]);
            this.setState({
                currencycode: countrycallingcode.replace(/"/g, ""),
                isoName: country_code
            });
        }).catch((error) => {
            console.log(error);
        });

        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.setCredentials();
            });

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }



    setCredentials = async () => {

        let touchIdEnable = await AsyncStorage.getItem("TouchIDEnable")
        let FaceIDEnable = await AsyncStorage.getItem("FaceIDEnable")
        let ispasscode = await AsyncStorage.getItem('passcode')
        if (ispasscode != null && touchIdEnable != "1" && FaceIDEnable != "1") {
            this.setState({
                passcode: ispasscode
            })
            this.RBSheetPasscode.open();
        }
        if (touchIdEnable == "1") {
            TouchID.authenticate('Please authenticate', optionalConfigObject)
                .then(success => {
                    if (success == true) {
                        this.props.navigation.navigate("Home")
                    }
                })
        }
        if (FaceIDEnable == "1") {
            TouchID.authenticate('Please authenticate', optionalConfigObject)
                .then(success => {
                    if (success == true) {
                        this.props.navigation.navigate("Home")
                    }
                })
        }



        // let fname = await AsyncStorage.getItem('first_name')
        // let lname = await AsyncStorage.getItem('last_name');
        // let carrierCode = await AsyncStorage.getItem('carrierCode');
        // let email = await AsyncStorage.getItem('email');
        // let formattedPhone = await AsyncStorage.getItem('formattedPhone');
        // let password = await AsyncStorage.getItem('password');
        // let phone = await AsyncStorage.getItem('phone');
        // let type = await AsyncStorage.getItem('type');
        // let user_id = await AsyncStorage.getItem('id');
        // let token = await AsyncStorage.getItem('token');


        // console.log("********************info coming***************")
        // console.log("fname", fname)
        // console.log("lname", lname)
        // console.log("carrierCode", carrierCode)
        // console.log("formattedPhone", formattedPhone)
        // console.log("email", email)
        // console.log("password", password)
        // console.log("phone", phone)
        // console.log("type", type)
        // console.log("user_id", user_id)
        // console.log("token", token)






    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }


    componentWillUnmount() {
        this.willFocusSubscription.remove()
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }


    _keyboardDidShow() {
        this.setState({
            showKeyboard: true
        })
    }

    _keyboardDidHide() {
        this.setState({
            showKeyboard: false
        })
    }



    handleBackPress = async () => {
        BackHandler.exitApp();

    }

    SearchFilterFunction(text) {
        const newData = this.state.arrayholder.filter(item => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ countryDataSource: newData, text: text });
    }

    renderCountries = () => {
        return (
            <View>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.theme_caribpay, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetAlert.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_16, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Select Country</Text>
                </View>
                <View style={{ marginTop: metrics.dimen_5, width: "95%", alignSelf: 'center' }}>
                    <Input
                        containerStyle={{ alignSelf: 'center', backgroundColor: '#DCDCDC', borderRadius: 10, height: 45 }}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        placeholder='Search'
                        placeholderTextColor="#9D9D9F"
                        inputStyle={{ color: colors.black, fontSize: metrics.text_16, }}
                        value={this.state.text}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        leftIcon={<Image style={{ width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }} source={require('../Images/search.png')}></Image>} />

                </View>

                <FlatList
                    style={{ marginTop: metrics.dimen_7 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.countryDataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => this.renderItemCountries(item)}>
                </FlatList>
            </View>
        )
    }


    getCodesByList = (item) => {
        this.setState({
            currencycode: item.callingCodes[0],
            isoName: item.isoName
        })
        this.RBSheetAlert.close();
    }

    renderItemCountries = (item) => {

        return (
            <View>
                <TouchableOpacity onPress={() => this.getCodesByList(item)} >
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                        <Image style={{ height: 30, width: 30, resizeMode: 'contain' }} source={{ uri: 'https://www.countryflags.io/' + item.isoName.toLocaleLowerCase() + '/flat/64.png' }}></Image>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, paddingHorizontal: 10 }}>{item.name}</Text>
                        <Text style={{ color: colors.app_black_text, fontSize: metrics.text_description }}>{" (" + item.callingCodes[0] + ")"}</Text>
                    </View>
                    <View style={{ borderBottomColor: colors.light_grey_backgroud, borderBottomWidth: 1 }} />
                </TouchableOpacity>
            </View>

        )

    }


    renderPasscode = () => {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }} onPress={() => this.RBSheetPasscode.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }} source={require('../Images/close.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_16, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Passcode</Text>
                </View>

                <Text style={{ fontSize: metrics.text_heading, color: colors.heading_black_text, fontWeight: '700', textAlign: 'center', marginTop: metrics.dimen_10 }}>Enter your Passcode</Text>
                <Image style={{ height: metrics.dimen_100, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: metrics.dimen_5 }} source={require('../Images/logo.png')}></Image>
                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, textAlign: 'center', marginTop: metrics.dimen_5 }}>Passcode Required to check-in the application</Text>
                <View style={{ marginTop: metrics.dimen_40, alignSelf: 'center' }}>
                    <SmoothPinCodeInput
                        placeholder=""
                        cellSize={65}
                        cellSpacing={7}
                        keyboardType='phone-pad'
                        cellStyle={{
                            borderWidth: 2,
                            borderRadius: 7,
                            borderColor: 'mediumturquoise',
                            backgroundColor: 'azure',
                        }}
                        cellStyleFocused={{
                            borderColor: 'lightseagreen',
                            backgroundColor: 'lightcyan',
                        }}
                        textStyle={{
                            fontSize: 24,
                            color: colors.black,
                            fontWeight: 'bold'
                        }}
                        textStyleFocused={{
                            color: 'crimson'
                        }}
                        value={this.state.code}
                        onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                        onTextChange={code => this.setState({ code })}
                    />
                </View>
                <TouchableOpacity onPress={() => console.log("cli")}>
                    <Text style={{ fontSize: 14, color: colors.place_holder_color, fontWeight: 'bold', marginTop: 20, marginHorizontal: 20, textAlign: 'left' }}>Forgot Passcode ?</Text>
                </TouchableOpacity>
            </View>
        )
    }


    onSetPasscodeClick = () => {
        console.log(this.state.code, this.state.passcode)
        if (this.state.code != this.state.passcode) {
            showAlert("CaribPay", "Passcode Invalid!")
        } else {
            this.RBSheetPasscode.close()
            this.props.navigation.navigate("Home");
        }
    }


    OnLogin = () => {

        if (this.state.fromMobilNo) {
            if (this.state.currencycode == '') {
                this.refs.toast.show("Please select country");
            } else if (this.state.phone == '') {
                this.refs.toast.show("Please enter the phone number");
            } else if (this.state.password == "") {
                this.refs.toast.show("Password Required");
            }
            else {
                this.setState({
                    visible: true
                })

                let loginwithPhoneData = {
                    email: this.state.currencycode + this.state.phone,
                    password: this.state.password,

                }
                console.log("loginwithPhoneData ---", loginwithPhoneData)
                axios.post('https://sandbox.caribpayintl.com/api/login', loginwithPhoneData)
                    .then((response) => {
                        console.log("loginwithphone data ---> ", response.data.response)
                        if (response.data.response.status == 200) {
                            AsyncStorage.setItem('first_name', response.data.response.first_name);
                            AsyncStorage.setItem('last_name', response.data.response.last_name);
                            AsyncStorage.setItem('email', response.data.response.email);
                            AsyncStorage.setItem('formattedPhone', response.data.response.formattedPhone);
                            AsyncStorage.setItem('id', JSON.stringify(response.data.response.user_id));
                            AsyncStorage.setItem('token', response.data.response.token);
                            AsyncStorage.setItem('picture', response.data.response.picture);
                            AsyncStorage.setItem('user_status', response.data.response.user_status);
                            AsyncStorage.setItem('password', this.state.password);

                            let postData = { user_id: response.data.response.user_id }
                            console.log(postData, response.data.response.token)
                            axios.get('https://sandbox.caribpayintl.com/api/available-balance', { params: postData, headers: { 'Authorization': response.data.response.token } }).then((response) => {
                                let data = response.data.wallets
                                const filterData = data.filter(roshan => roshan.is_default == "Yes")
                                console.log("filtered result on cureency change", filterData)
                                let currency_symbol = filterData[0].curr_code
                                let balance = filterData[0].balance
                                let available_bal = parseFloat(balance).toFixed(2)
                                this.setState({
                                    available_Balance: available_bal,
                                    currency_symbol: currency_symbol,
                                });
                                console.log("info coming --> ", data, this.state.available_Balance, this.state.currency_symbol)
                                AsyncStorage.setItem('available_balance', this.state.available_Balance)
                                AsyncStorage.setItem('symbol', this.state.currency_symbol)
                                { data.length > 1 && this.setWalletdata(data) }
                                setTimeout(() => {
                                    this.setState({ visible: false, phone: '', password: '', loginAttempts: 0 })
                                }, 200);
                                this.props.navigation.navigate('Home')
                            }).catch(error => {
                                console.log("error in balance getting --- > ", error)
                            })
                        }
                    }).catch(error => {
                        if (error.response) {
                            console.log("response", error.response.data)
                            if (error.response.data.success != undefined) {
                                if (error.response.data.success.status != null && error.response.data.success.status != undefined && error.response.data.success.status == 400) {
                                    this.setState({
                                        visible: false, invalid: true
                                    })
                                }
                            }
                            else (error.response.data.response.status != null && error.response.data.response.status != undefined && error.response.data.response.status == 400)
                            this.setState({
                                visible: false, invalid: error.response.data.response.message == "Invalid email & credentials" ? true : false, loginAttempts: error.response.data.response.failed_attempt, userblocked: error.response.data.response.message != "Invalid email & credentials" ? true : false
                            })
                        }
                    })
            }
        } else {
            var patt = new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");
            if (this.state.email == '') {
                this.refs.toast.show("Email Required!");
            }
            else if (patt.test(this.state.email) == false) {
                this.setState({
                    emailInvalid: true
                })
            }
            else if (this.state.password == "") {
                this.refs.toast.show("Password Required");
            }
            else {
                this.setState({
                    visible: true
                })

                let loginwithEmailData = {
                    email: this.state.email,
                    password: this.state.password,
                }

                axios({
                    method: 'post',
                    url: 'https://sandbox.caribpayintl.com/api/login',
                    data: loginwithEmailData
                }).then((response) => {
                    console.log("response data is ---", response.data.data)
                    if (response.data.response.status == 200) {
                        AsyncStorage.setItem('first_name', response.data.response.first_name);
                        AsyncStorage.setItem('last_name', response.data.response.last_name);
                        AsyncStorage.setItem('email', response.data.response.email);
                        AsyncStorage.setItem('formattedPhone', response.data.response.formattedPhone);
                        AsyncStorage.setItem('id', JSON.stringify(response.data.response.user_id));
                        AsyncStorage.setItem('token', response.data.response.token);
                        AsyncStorage.setItem('picture', response.data.response.picture);
                        AsyncStorage.setItem('user_status', response.data.response.user_status);
                        AsyncStorage.setItem('password', this.state.password);
                        let postData = { user_id: response.data.response.user_id }
                        console.log(postData)
                        axios.get('https://sandbox.caribpayintl.com/api/available-balance', { params: postData, headers: { 'Authorization': response.data.response.token } }).then((response) => {
                            let data = response.data.wallets
                            const filterData = data.filter(roshan => roshan.is_default == "Yes")
                            console.log("filtered result on cureency change", filterData)
                            let currency_symbol = filterData[0].curr_code
                            let balance = filterData[0].balance
                            let available_bal = parseFloat(balance).toFixed(2)
                            this.setState({
                                available_Balance: available_bal,
                                currency_symbol: currency_symbol,
                            });
                            console.log("info coming --> ", data, this.state.available_Balance, this.state.currency_symbol)
                            AsyncStorage.setItem('available_balance', this.state.available_Balance)
                            AsyncStorage.setItem('symbol', this.state.currency_symbol)
                            { data.length > 1 && this.setWalletdata(data) }
                            setTimeout(() => {
                                this.setState({ visible: false, email: '', password: '', loginAttempts: 0 })
                            }, 200);
                            this.props.navigation.navigate('Home')
                        })
                    }

                }).catch(error => {
                    if (error.response) {
                        console.log("response", error.response.data)
                        if (error.response.data.success != undefined) {
                            if (error.response.data.success.status != null && error.response.data.success.status != undefined && error.response.data.success.status == 400) {
                                this.setState({
                                    visible: false, invalid: true
                                })
                            }
                        }
                        else (error.response.data.response.status != null && error.response.data.response.status != undefined && error.response.data.response.status == 400)
                        this.setState({
                            visible: false, invalid: error.response.data.response.message == "Invalid email & credentials" ? true : false, loginAttempts: error.response.data.response.failed_attempt, userblocked: error.response.data.response.message != "Invalid email & credentials" ? true : false
                        })
                    }
                })
            }
        }
    }


    setWalletdata = async (data) => {
        await AsyncStorage.setItem('walletData', JSON.stringify(data))
    }

    render() {
        let name = this.state.isoName
        var inshort = name.toLocaleLowerCase();
        return (
            <SafeAreaView style={styles.mainContainer}>
                <View style={{ flex: 1 }}>
                    <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.dimen_240 }}>
                        <View style={{ margin: metrics.dimen_20 }}>
                            <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image>
                            <Text style={{ fontSize: metrics.text_header, color: colors.app_light_gray, fontWeight: 'bold' }}>Welcome</Text>
                            <Text style={{ fontSize: metrics.text_21, color: colors.white, fontWeight: 'bold' }}>Sign In</Text>
                        </View>
                    </View>

                    <View style={{ position: "absolute", top: metrics.dimen_160, width: "100%", height: Dimensions.get('screen').height - 200, backgroundColor: colors.white, borderRadius: 40 }}>
                        <ScrollView style={{ marginBottom: 20 }}>
                            <View style={{ flexDirection: 'column', marginHorizontal: metrics.dimen_20 }}>
                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                    <View style={{ flexDirection: "row", justifyContent: 'space-evenly' }}>
                                        <RadioButton
                                            size={metrics.dimen_12}
                                            animation={'bounceIn'}
                                            isSelected={this.state.fromMobilNo}
                                            innerColor={colors.theme_caribpay}
                                            outerColor={colors.theme_caribpay}
                                            onPress={() => this.setState({
                                                fromMobilNo: true,
                                                fromEmail: false
                                            })}
                                        />
                                        <Text style={{ fontSize: metrics.text_14, color: colors.heading_black_text, marginLeft: metrics.dimen_10 }}>Mobile No.</Text>
                                    </View>

                                    <View style={{ justifyContent: 'center', marginLeft: metrics.dimen_20 }}>
                                        <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, textAlignVertical: 'center' }}>|</Text>
                                    </View>

                                    <View style={{ flexDirection: "row", marginLeft: metrics.dimen_30 }}>
                                        <RadioButton
                                            size={metrics.dimen_12}
                                            animation={'bounceIn'}
                                            isSelected={this.state.fromEmail}
                                            innerColor={colors.theme_caribpay}
                                            outerColor={colors.theme_caribpay}
                                            onPress={() => this.setState({
                                                fromEmail: true,
                                                fromMobilNo: false
                                            })}
                                        />
                                        <Text style={{ fontSize: metrics.text_14, color: colors.heading_black_text, marginLeft: metrics.dimen_10 }}>Email</Text>
                                    </View>
                                </View>


                                {this.state.fromMobilNo &&
                                    <View style={{ flexDirection: 'column', marginTop: 20, width: '90%' }}>
                                        <Text style={{ fontSize: metrics.text_header, color: colors.black }}>{"Mobile No."}</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                            <Input
                                                placeholderTextColor={colors.place_holder_color}
                                                placeholder={'Code'}
                                                value={this.state.currencycode}
                                                disabled={true}
                                                onChangeText={(text) => this.setState({ currencycode: text })}
                                                leftIcon={
                                                    <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                                        {this.state.isoName == "" ?
                                                            <Image style={{ height: 20, width: 20 }} source={require('../Images/globe.png')}>
                                                            </Image> :
                                                            <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://www.countryflags.io/' + inshort + '/flat/64.png' }}>
                                                            </Image>
                                                        }
                                                    </TouchableOpacity>
                                                }
                                                containerStyle={{ width: "30%", height: 50 }}
                                                inputContainerStyle={{ borderBottomWidth: 0.2 }}
                                                inputStyle={{ fontSize: 14 }}>
                                            </Input>
                                            {/* <Text style={{ fontSize: metrics.text_normal, color: colors.black, marginTop: 10 }}>Mobile Number</Text> */}
                                            <Input
                                                placeholder={'Mobile No.'}
                                                placeholderTextColor={colors.place_holder_color}
                                                containerStyle={{ width: "80%", height: 50 }}
                                                keyboardType='phone-pad'
                                                value={this.state.phone}
                                                inputContainerStyle={{ borderBottomWidth: 0.2 }}
                                                inputStyle={{ fontSize: 14 }}
                                                onChangeText={(text) => this.setState({ phone: text })}>
                                            </Input>
                                        </View>
                                    </View>
                                }
                                {this.state.fromEmail &&

                                    <View>
                                        <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 20 }}>Email</Text>

                                        <Input
                                            placeholder={'Email Id'}
                                            placeholderTextColor={colors.place_holder_color}
                                            value={this.state.email}
                                            onChangeText={(text) => this.setState({ email: text })}
                                            containerStyle={{ width: "100%", height: 50 }}
                                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                                            inputStyle={{ fontSize: 14 }}>
                                        </Input>
                                    </View>
                                }
                                <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Password</Text>
                                <Input
                                    placeholder={'Password'}
                                    value={this.state.password}
                                    placeholderTextColor={colors.place_holder_color}
                                    onChangeText={(text) => this.setState({ password: text })}
                                    secureTextEntry={this.state.hide ? true : false}
                                    rightIcon={
                                        <TouchableOpacity onPress={() => this.setState({ hide: !this.state.hide })}>
                                            <Image style={{ height: 20, width: 20, marginRight: 10 }} source={this.state.hide ? require('../Images/hide.png') : require('../Images/eye.png')}>
                                            </Image>
                                        </TouchableOpacity>
                                    }
                                    containerStyle={{ width: "100%", height: 50 }}
                                    inputContainerStyle={{ borderBottomWidth: 0.2 }}
                                    inputStyle={{ fontSize: 14 }}>
                                </Input>




                            </View>


                            <Spinner style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />



                            <TouchableOpacity onPress={() => this.OnLogin()}>
                                <View elevation={5} style={styles.buttonStyle}>
                                    <Image style={{ height: 15, width: 15, alignSelf: 'center' }} source={require('../Images/checkbox.png')}></Image>
                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>Sign In</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                                {/* <TouchableOpacity onPress={() => this.setState({
                                tryAgain: true
                            })}> */}

                                <Text style={{ fontSize: 14, color: colors.place_holder_color, fontWeight: 'bold', marginTop: 20, marginHorizontal: 20, textAlign: 'right' }}>Forgot Password ?</Text>
                            </TouchableOpacity>


                            <Toast
                                ref="toast"
                                style={{ backgroundColor: 'black' }}
                                position='center'
                                positionValue={200}
                                fadeInDuration={200}
                                fadeOutDuration={1000}
                                opacity={0.8}
                                textStyle={{ color: 'white' }} />


                            <RBSheet
                                ref={ref => {
                                    this.RBSheetAlert = ref;
                                }}
                                height={Dimensions.get('screen').height}
                                duration={0}>
                                {this.renderCountries()}
                            </RBSheet>


                            <RBSheet
                                ref={ref => {
                                    this.RBSheetPasscode = ref;
                                }}
                                height={Dimensions.get('screen').height}
                                duration={0}>
                                {this.renderPasscode()}
                            </RBSheet>



                            {/* <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid} >
                                    <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                        <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"Warning!"}</Text>
                                        </View>
                                        <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                            <Text style={{ color: 'red', fontSize: metrics.text_medium, textAlign: 'center' }}>{"You have made " + this.state.loginAttempts + " unsuccessfull attempts(s)" + "\n Remaining Attempt(s) : " + (3 - parseInt(this.state.loginAttempts)) + " "}</Text>
                                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Invalid Credentials, Try again with correct credentials!"}</Text>
                                            <TouchableOpacity onPress={() => this.setState({
                                                invalid: false
                                            })}>
                                                <View style={{ height: metrics.dimen_30, width: 80, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                                    <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"Try Again"}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Modal>
                            </View> */}


                            <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid}>
                                    <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 350, justifyContent: 'center' }}>
                                        <View style={{ flex: 1 }}>
                                            <View>
                                                <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                                    source={require("../Images/logo.png")}></Image>
                                                {this.state.fromEmail ? <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Your Email ID or password is incorrect.</Text>
                                                    :
                                                    <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Your Mobile No. or password is incorrect.</Text>
                                                }
                                                <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 15, fontWeight: 'bold', textAlign: 'center' }}>Try again or tap on 'Forgot Password' to reset your password</Text>
                                            </View>
                                            <View style={styles.bottomview}>
                                                <TouchableOpacity onPress={() => this.setState({ invalid: false })}>
                                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>

                            <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.emailInvalid}>
                                    <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 200, justifyContent: 'center' }}>
                                        <View style={{ flex: 1 }}>
                                            <View>
                                                <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 10 }}
                                                    source={require("../Images/logo.png")}></Image>
                                                <Text style={{ fontSize: metrics.text_normal, color: colors.black, margin: 15, fontWeight: 'bold', textAlign: 'center' }}>Enter the Email ID in correct Format.</Text>
                                            </View>
                                            <View style={styles.bottomview}>
                                                <TouchableOpacity onPress={() => this.setState({ emailInvalid: false })}>
                                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>


                            {/* 
                            <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.userblocked} >
                                    <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                        <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"Sorry!"}</Text>
                                        </View>
                                        <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Your Account has been suspended due to many Invalid Login attempts.!"}</Text>
                                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Please Contact Support Centre support@caribpayintl.com"}</Text>
                                            <TouchableOpacity onPress={() => this.setState({
                                                userblocked: false
                                            })}>
                                                <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                                    <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Modal>
                            </View> */}


                            <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.userblocked}>
                                    <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 350, justifyContent: 'center' }}>
                                        <View style={{ flex: 1 }}>
                                            <View>
                                                <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                                    source={require("../Images/logo.png")}></Image>
                                                <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}>Your account has been suspended due to many invalid login attempt.</Text>
                                                <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 15, fontWeight: 'bold', textAlign: 'center' }}>Please contact support centre at support@caribpayintl.com</Text>
                                            </View>
                                            <View style={styles.bottomview}>
                                                <TouchableOpacity onPress={() => this.setState({ userblocked: false })}>
                                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>OK</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>






                        </ScrollView>

                    </View>
                    {!this.state.showKeyboard &&
                        <View style={{ position: 'absolute', bottom: 10, alignSelf: 'center' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                                <Text style={{ fontSize: metrics.text_header, color: colors.carib_pay_blue, fontWeight: 'bold', marginTop: 20, marginHorizontal: metrics.dimen_20 }}>Dont Have an Account? <Text style={{ fontSize: metrics.text_large, color: colors.theme_caribpay, fontWeight: 'bold', textAlign: 'center', marginTop: 20 }}>Sign Up</Text></Text>
                            </TouchableOpacity>
                        </View>}
                </View>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    imageStyle: {
        height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain', marginTop: 5
    },
    textStyle: {
        color: colors.app_gray, fontSize: metrics.text_description, marginLeft: 15, marginTop: 15
    },
    buttonStyle: {
        flexDirection: 'row',
        width: '80%',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        height: 50,
        marginTop: metrics.dimen_20,
        backgroundColor: colors.theme_caribpay,
        shadowColor: "#f2f2f2",
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10, marginTop: 10 }

})