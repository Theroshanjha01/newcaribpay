import React from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Dimensions, Image, StyleSheet, BackHandler } from 'react-native';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';

export default class Points extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ margin: metrics.dimen_20 }}>
                        <View style={{ height: metrics.dimen_40, width: 300, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white', marginTop: 1 }} source={require('../Images/leftarrow.png')}></Image>
                            </TouchableOpacity>
                            <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginLeft: 80, textAlign: 'center' }}>MY Carib Points</Text>
                        </View>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>100 CP</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>Points will be reflected soon</Text>
                    </View>
                </View>
                <View style={styles.curveview}>
                    <View style={{ margin: metrics.dimen_15 }}>
                        <View style={{ flexDirection: 'column' }}>


                        <View style={{flexDirection:'row', justifyContent:'center',marginBottom:15}}> 
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_16 }}>Membership Status </Text>
                                <View style={{borderRadius:10, borderBottomColor:'red',borderWidth:1.5 , width:50, backgroundColor:colors.light_grey_backgroud, justifyContent:'center'}}>
                                <Text style={{ color: colors.black,fontSize: metrics.text_normal , textAlign:'center' }}>Red</Text>
                                </View>
                                </View>


                            <Text style={{ color: colors.lightgrey }}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>CPANEL INC</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+20 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>

                        <View style={{ flexDirection: 'column' , marginTop:10}}>
                            <Text style={{ color: colors.lightgrey}}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>CPANEL INC</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+20 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>


                        <View style={{ flexDirection: 'column' , marginTop:10}}>
                            <Text style={{ color: colors.lightgrey}}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>CPANEL INC</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+20 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>



                        <View style={{ flexDirection: 'column' , marginTop:10}}>
                            <Text style={{ color: colors.lightgrey}}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>UPCLOUD</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+20 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>


                        <View style={{ flexDirection: 'column' , marginTop:10}}>
                            <Text style={{ color: colors.lightgrey}}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>CPANEL INC</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+5 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>


                        
                        <View style={{ flexDirection: 'column' , marginTop:10}}>
                            <Text style={{ color: colors.lightgrey }}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>UPCLOUD</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+10 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>



                        
                        <View style={{ flexDirection: 'column' , marginTop:10}}>
                            <Text style={{ color: colors.lightgrey }}>10 August 2020</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                <View style={{flexDirection:'row', justifyContent:'space-between'}}> 
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25}} source={require('../Images/coins.png')}></Image>
                                <Text style={{ color: colors.black, marginLeft: 10, fontSize: metrics.text_normal }}>UPCLOUD</Text>
                                </View>
                                <Text style={{ color: colors.lightgrey, marginRight: 10,fontSize: metrics.text_header }}>+5 pts</Text>
                            </View>
                            <View style={styles.horizontalLine}></View>
                        </View>


                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        flexDirection: 'row', paddingHorizontal: metrics.dimen_20,
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_16,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', paddingHorizontal: metrics.dimen_20
    },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop - 20, width: "100%", backgroundColor: colors.white, borderRadius: 40 },
    container: { flexDirection: 'row', margin: metrics.dimen_20 },
    image_style: { height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.5, width: '100%', alignSelf: 'center', marginTop: 10 },
    bottomview: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },

})