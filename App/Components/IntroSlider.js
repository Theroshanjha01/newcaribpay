import React from 'react';
import { Text , View , Image , TouchableOpacity} from 'react-native';
import { slides } from '../constant.js';
import AppIntroSlider from 'react-native-app-intro-slider';
import Metrics from '../Themes/Metrics.js';
import metrics from '../Themes/Metrics.js';
// import Colors from '../Themes/Colors.js';

export default class IntroSlider extends React.Component{
    constructor(props){
        super(props)
        this.state={}
    }

    _renderItem = ({ item }) => {
        return (
            <View style={{flex:1}}>
        {item.skip && 
         <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
        <View style={{ flexDirection: 'row', justifyContent:'flex-end', height:40 , margin:10}}>
        <Text style={{color:'#0E95FF', alignSelf:'center', fontSize:14 ,margin:5}}>SKIP</Text>
            <Image style= {{height:20,width:15, alignSelf:'center', resizeMode:'contain',alignSelf:'center' ,margin:5}}source={require('../Images/arrow.png')}/>
         </View>
         </TouchableOpacity>
       }
           <View style={{flex:1, justifyContent:'center', alignContent:'center'}}>
             <Image style= {{height:Metrics.dimen_220,width:Metrics.dimen_220,alignSelf:'center', resizeMode:'contain'}}source={item.image}/>
                    <Text style={{color:'#232429', fontSize:metrics.text_22, textAlign:'center',marginTop:20, fontWeight:'400'}}>{item.title}</Text>
                    <Text style={{ width:'80%', alignSelf:'center', color:'#808080', fontSize:18, textAlign:'center', marginTop:20 , fontFamily:Metrics.quicksand_bold}}>{item.text}</Text>
                    {!item.skip && 
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('Login')}>
                    <View style={{backgroundColor:'#FFB229', justifyContent:'center' , alignSelf:'center' , width:'50%' , borderRadius:20 , height:40 ,marginTop:20}}>
                        <Text style={{textAlign:'center' , fontSize:15 , color:'white' , fontWeight:'bold'}}>GET STARTED</Text>
                    </View>
                    </TouchableOpacity>
                    }
                </View>
                </View>
        );
    }

    _onDone = () => {}

    render(){
        return(
            <AppIntroSlider renderItem={this._renderItem} data={slides} onDone={this._onDone}
            activeDotStyle={{backgroundColor:'blue'}}/>
        )
    }
}
