import React from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, Share, BackHandler } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import ToggleSwitch from 'toggle-switch-react-native'
import TouchID from 'react-native-touch-id'
import { showAlert } from '../Utils.js';
import AsyncStorage from '@react-native-community/async-storage';


const optionalConfigObject = {
    title: 'Authentication Required', // Android
    imageColor: colors.app_dark_green_color, // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'Touch the fingerprint sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};


export default class Settings extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isPasscodeEnabled: false,
            isTouchIdEnabled: false,
            // isEnabledFaceID: false,
            token: this.props.navigation.state.params.token
        }
    }

    async componentDidMount() {

        let touchid = await AsyncStorage.getItem('TouchIDEnable');
        // let faceid = await AsyncStorage.getItem('FaceIDEnable');
        let passcode = await AsyncStorage.getItem('passcode')

        if (touchid == "1") {
            this.setState({
                isTouchIdEnabled: true
            })
        }
        // if (faceid == "1") {
        //     this.setState({
        //         isEnabledFaceID: true
        //     })
        // }
        if (passcode != null) {
            this.setState({
                isPasscodeEnabled: true
            })
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }


    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    _pressHandler() {

        const isSupportedObject = {
            unifiedErrors: false, // use unified error messages (default false)
            passcodeFallback: false // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
        }

        TouchID.isSupported(isSupportedObject)
            .then(biometryType => {
                if (biometryType === 'FaceID') {
                    console.log('FaceID is supported.');
                    TouchID.authenticate('Please enable your Face ID', optionalConfigObject)
                        .then(success => {
                            console.log(success)
                            AsyncStorage.setItem("FaceIDEnable", "1");
                            showAlert("CaribPay", 'Authenticated Successfully');
                        })
                        .catch(error => {
                            showAlert("CaribPay", 'Authentication Failed');
                        });
                } else {
                    console.log('TouchID is supported.');
                    TouchID.authenticate('Please Enable your Touch ID', optionalConfigObject)
                        .then(success => {
                            console.log(success)
                            AsyncStorage.setItem("TouchIDEnable", "1");
                            showAlert("CaribPay", 'Authenticated Successfully');
                        })
                        .catch(error => {
                            showAlert("CaribPay", 'Authentication Failed');
                        });
                }
            })
            .catch(error => {
                // Failure code
                console.log(error);
                showAlert("CaribPay", "Not Supported");
            });

    }

    _onFaceID() {
        const isSupportedObject = {
            unifiedErrors: false, // use unified error messages (default false)
            passcodeFallback: false // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
        }

        TouchID.isSupported(isSupportedObject)
            .then(biometryType => {
                if (biometryType === 'FaceID') {
                    console.log('FaceID is supported.');
                } else {
                    showAlert("CaribPay", 'FaceID is not Supported');
                }
            })
    }


    referPeoples = async () => {

        if (Platform.OS === 'ios') {
            try {
                const result = await Share.share({
                    message: 'Hey! Download CaribPay for Fast & Easy Payment  https://share.caribpayintl.com',
                    title: 'CaribPay',
                });
                if (result.action === Share.sharedAction) {
                    if (result.activityType) {
                    } else {
                    }
                } else if (result.action === Share.dismissedAction) {
                }
            } catch (error) {
                alert(error.message);
            }
        }
        else {
            try {
                const result = await Share.share({
                    message: 'Hey! Download CaribPay for Fast & Easy Payment  https://share.caribpayintl.com',
                    title: 'CaribPay',
                });
                if (result.action === Share.sharedAction) {
                    if (result.activityType) {
                    } else {
                    }
                } else if (result.action === Share.dismissedAction) {
                }
            } catch (error) {
                alert(error.message);
            }
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>Settings</Text>
                </View>

                <View style={styles.absouluteview}>

                    <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/dial.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                            <Text style={styles.headingg}>Enable Passcode</Text>
                        </View>
                        <ToggleSwitch
                            isOn={this.state.isPasscodeEnabled}
                            onColor={colors.carib_pay_blue}
                            offColor={colors.light_grey_backgroud}
                            // label="Example label"
                            // labelStyle={{ color: "black", fontWeight: "900" }}
                            size="medium"
                            onToggle={isOn => this.setState({
                                isPasscodeEnabled: !this.state.isPasscodeEnabled
                            }, () => this.state.isPasscodeEnabled ? AsyncStorage.setItem("DeviceId", null) : AsyncStorage.setItem("DeviceId", "1"))}
                        />
                    </View>


                    <View style={styles.horizontalLine} />

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ChangePasscode")}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/dial.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: "center" }}>
                                <Text style={styles.headingg}>Change Passcode</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />

                    <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/fingerprint.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                            <Text style={styles.headingg}>Enable Touch ID</Text>
                        </View>
                        <ToggleSwitch
                            isOn={this.state.isTouchIdEnabled}
                            onColor={colors.carib_pay_blue}
                            offColor={colors.light_grey_backgroud}
                            size="medium"
                            onToggle={isOn => this.setState({
                                isTouchIdEnabled: !this.state.isTouchIdEnabled
                            }, () => this.state.isTouchIdEnabled ? this._pressHandler() : AsyncStorage.setItem("TouchIDEnable", "0"))}
                        />
                    </View>

                    <View style={styles.horizontalLine} />

                    {/* <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/identity.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                            <Text style={styles.headingg}>Enable Face ID</Text>
                        </View>
                        <ToggleSwitch
                            isOn={this.state.isEnabledFaceID}
                            onColor={colors.carib_pay_blue}
                            offColor={colors.light_grey_backgroud}
                            // label="Example label"
                            // labelStyle={{ color: "black", fontWeight: "900" }}
                            size="medium"
                            onToggle={isOn => this.setState({
                                isEnabledFaceID: !this.state.isEnabledFaceID
                            }, () => this.state.isEnabledFaceID ? this._onFaceID() : AsyncStorage.setItem("FaceIDEnable", "0"))}/>
                    </View>


                    <View style={styles.horizontalLine} /> */}

                    <TouchableOpacity onPress={() => this.referPeoples()}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/share.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                                <Text style={styles.headingg}>{"Share & Earn"}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />



                    <TouchableOpacity onPress={() => this.props.navigation.navigate('TermsNConditions', { token: this.state.token, fromPrivacy: "0" })}>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/paper.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                                <Text style={styles.headingg}>{"Terms & Conditions"}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />


                    <TouchableOpacity onPress={() => this.props.navigation.navigate('TermsNConditions', { token: this.state.token, fromPrivacy: "1" })}>

                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.image_style} source={require('../Images/policy.png')}></Image>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                                <Text style={styles.headingg}>{"Privacy Notice"}</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.horizontalLine} />



                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, textAlignVertical: 'center' },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' }
})