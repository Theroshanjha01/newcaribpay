import React from 'react'
import { ProgressDialog as Progress} from 'react-native-simple-dialogs';
import colors from './Themes/Colors.js';



export default class ProgressDialog extends React.PureComponent{
    render(){
        return(
            <Progress visible = {this.props.visible}
                 dialogStyle = {{backgroundColor:colors.white , alignSelf:'center',height:'10%',width:'65%',
                 alignItems:'center',justifyContent:'center'}}
                 overlayStyle = {{backgroundColor:colors.transparent}}
                 messageStyle = {{color:colors.heading_black_text,fontSize:14}}
                 title = ""
                 message="Please wait"
                 activityIndicatorColor = {colors.carib_pay_blue}
                 activityIndicatorSize = 'large'
            />
        )
    }
}