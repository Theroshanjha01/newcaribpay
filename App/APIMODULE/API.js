
const method = {
    post: 'POST',
    get: 'GET',
    delete: 'DELETE',
}



const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
}


//stagingAPi
baseUrl = 'https://sandbox.caribpayintl.com/api'

const signup_url = baseUrl + 'registration-with-otp';
const login_url = baseUrl +'login';
const send_otp_url = baseUrl+'send-otp';
// export const homepage_banner_url =  baseUrl+'banner';
// export const homepage_shopbyCategory = baseUrl + 'category';
// export const newarrival_product = baseUrl + 'newarrival';
// export const toptrends_product = baseUrl + 'toptrend';
// export const clearance_product = baseUrl + 'clearance';
// export const getproduct_Details = baseUrl + 'productdetail/';
// export const browserListing_url  = baseUrl + 'showallnewarrival';
// const addToWishlist_url = baseUrl + 'addToWishlist';
// export const fav_product_list = baseUrl +'favouriteProductList'










export const callWebService = (header, body, url) => {

    console.log('requestbody  ' + JSON.stringify(body) + '\n' + url)
    console.log('header =  ' + JSON.stringify(header) + '\n' + url)
    //url = 'https://facebook.github.io/react-native/movies.json'
    //console.warn("URL"  +url);
    return fetch(url, {

        method: method.post,

        headers: { ...headers, ...header },

        body: JSON.stringify(body)
    })
        .then(response => {
            // console.log("service response = ",response)
            return response.json()

        }).then(responseJson => {

            //console.log("response ",JSON.stringify(responseJson)) 
            return responseJson

        })
        .catch(error => {

            //console.log("error  "+JSON.stringify(error))
            return error
        });

}


export const calldeleteWebService = (header, body, url) => {

    console.log('requestbody  ' + JSON.stringify(body) + '\n' + url)
    //url = 'https://facebook.github.io/react-native/movies.json'
    //console.warn("URL"  +url);
    return fetch(url, {

        method: method.delete,

        headers: { ...headers, ...header },

        body: JSON.stringify(body)
    })
        .then(response => {
            //console.warn("response ",JSON.stringify(response))
            return response.json()

        }).then(responseJson => {

            //console.warn("response ",JSON.stringify(responseJson)) 
            return responseJson

        })
        .catch(error => {

            //console.warn("error  "+JSON.stringify(error))
            return error
        });

}

export const callWebServiceGet = (header, url) => {

    //console.warn('requestbody  '+JSON.stringify(body)+'\n'+url)
    //url = 'https://facebook.github.io/react-native/movies.json'
    //console.warn("URL"  +url);
    return fetch(url, {

        method: method.get,

        headers: { ...headers, ...header },

    })
        .then(response => {
            //console.warn("response ",JSON.stringify(response))
            return response.json()

        }).then(responseJson => {

            //console.warn("response ",JSON.stringify(responseJson)) 
            return responseJson

        })
        .catch(error => {

            //console.warn("error  "+JSON.stringify(error))
            return error
        });
}





export const doSignupURL = async (email, password) => {
    try {
        const response = await callWebService({}, { email: email, password: password}, signup_url);
        return response;
    }
    catch (error) {
        return error;
    }

}

export const doLoginURL = async (email, password , phone , first_name, last_name , type , carrierCode , formattedPhone ) => {
    try {
        const response = await callWebService({}, { email: email, password: password , phone:phone , first_name:first_name, last_name:last_name , type:type , carrierCode:carrierCode , formattedPhone:formattedPhone }, login_url);
        return response;
    }
    catch (error) {
        return error;
    }

}



export const addToWishlist = async (product_id, product_sku , product_qty ) => {
    try {
        const response = await callWebService({}, { product_id:product_id, product_sku:product_sku , product_qty:product_qty }, addToWishlist_url);
        return response;
    }
    catch (error) {
        return error;
    }

}


export const sentOtpUrl = async (phone) => {
    try {
        const response = await callWebService({}, { phone:phone }, send_otp_url );
        return response;
    }
    catch (error) {
        return error;
    }
}

// export const gettenantPropertyIssueList = (property_id, job_id,easerid) => {
//     return AuthToken()
//         .then(val => callWebService(val, { property_id: property_id, job_id: job_id,easer_id:easerid }, tenantPropertyIssueList))
//         .then(response => response)
//         .catch(error => error)
// }





// export const addIssueComment = (issue_id, comment, media, type,easerid) => {
//     return AuthToken()
//         .then(val => callWebService(val, { issue_id: issue_id, comment: comment, media: media, type: type,easer_id:easerid }, add_issue_comment_url))
//         .then(response => response)
//         .catch(error => error)
// }




// export const sendReminderIssue = (property_id , issue_id,easerid) => {
//     return AuthToken()
//         .then(val => callWebService(val, { property_id: property_id , issue_id:issue_id,easer_id:easerid }, sendReminder_url))
//         .then(response => response)
//         .catch(error => error)
// }

// export const logoutFromApp = (apptype) => {
//     return AuthToken().then(token =>
//         EaserId().then(user_id =>
//             callWebService(token, { user_id, apptype }, getLogout)
//                 .then(response => response)
//                 .catch(error => error)
//         )
//     )
// }
