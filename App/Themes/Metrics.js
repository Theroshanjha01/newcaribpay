import { Dimensions, Platform, PixelRatio } from 'react-native'
import NormalizeSize from '../Themes/NormalizeSize';

const { width, height } = Dimensions.get('window')
const font_ratio = PixelRatio.getFontScale()
// used via Metrics.baseMargin

const metrics = {

    banner_height: (NormalizeSize.getWPercent('100%') * 4 / 7) + (Platform.OS === 'android' ? NormalizeSize.getH(30) : NormalizeSize.getH(20)),
    development_banner_height: (NormalizeSize.getWPercent('60%') * 4 / 7) + (Platform.OS === 'android' ? NormalizeSize.getH(30) : NormalizeSize.getH(20)),

    text_small: NormalizeSize.getFontSize(10),//10/font_ratio,
    text_medium: NormalizeSize.getFontSize(12),//12/font_ratio,
    text_normal: NormalizeSize.getFontSize(14),//10,14/font_ratio,
    text_16: NormalizeSize.getFontSize(16),//16/font_ratio,c
    text_large: NormalizeSize.getFontSize(18),//18/font_ratio,
    text_heading: NormalizeSize.getFontSize(20),//20/font_ratio,
    text_description: NormalizeSize.getFontSize(13),//13/font_ratio,
    text_header: NormalizeSize.getFontSize(15),//20/font_ratio,
    text_size_button: NormalizeSize.getFontSize(16),//16/font_ratio,
    text_17: NormalizeSize.getFontSize(17),
    text_21: NormalizeSize.getFontSize(21),
    text_22: NormalizeSize.getFontSize(22),
    text_23: NormalizeSize.getFontSize(23),
    text_24: NormalizeSize.getFontSize(24),
    text_25: NormalizeSize.getFontSize(25),
    text_26: NormalizeSize.getFontSize(26),
    text_36: NormalizeSize.getFontSize(36),


    font_weight_button: '500',
    quicksand_bold: 'Quicksand-Bold',
    quicksand_light: 'Quicksand-Light',
    quicksand_medium: 'Quicksand-Medium',
    quicksand_regular: 'Quicksand-Regular',
    quicksand_semibold: 'Quicksand-SemiBold',

    proxima_nova: 'Proxima Nova',
    proxima_nova_regular: 'ProximaNova-Regular',
    proxima_nova_Semibold: 'ProximaNova-Semibold',
    Proxima_Nova_Extrabold: 'Proxima Nova Extrabold',


    width: width,
    height: height,
    marginHorizontal: 10,
    marginVertical: 10,
    section: 25,
    baseMargin: 10,
    doubleBaseMargin: 20,
    smallMargin: 5,
    doubleSection: 50,
    horizontalLineHeight: 1,
    screenWidth: width < height ? width : height,
    screenHeight: width < height ? height : width,
    navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
    buttonRadius: 5,
    borderWidth: 2,
    dimen_5: NormalizeSize.getH(5),
    dimen_6: NormalizeSize.getH(6),
    dimen_7: NormalizeSize.getH(7),
    dimen_1: NormalizeSize.getH(1),
    dimen_tiny: NormalizeSize.getH(0.5),
    dimen_2: NormalizeSize.getH(2),
    dimen_3: NormalizeSize.getH(3),
    dimen_4: NormalizeSize.getH(4),
    dimen_8: NormalizeSize.getH(8),
    dimen_9: NormalizeSize.getH(9),
    dimen_10: NormalizeSize.getH(10),
    dimen_12: NormalizeSize.getH(12),
    dimen_14: NormalizeSize.getH(14),
    dimen_15: NormalizeSize.getH(15),
    dimen_16: NormalizeSize.getH(16),
    dimen_18: NormalizeSize.getH(18),
    dimen_20: NormalizeSize.getH(20),
    dimen_22: NormalizeSize.getH(22),
    dimen_24: NormalizeSize.getH(24),
    dimen_25: NormalizeSize.getH(25),
    dimen_27: NormalizeSize.getH(27),
    dimen_30: NormalizeSize.getH(30),
    dimen_35: NormalizeSize.getH(35),
    dimen_36: NormalizeSize.getH(36),
    dimen_40: NormalizeSize.getH(40),
    dimen_42: NormalizeSize.getH(42),
    dimen_44: NormalizeSize.getH(44),
    dimen_45: NormalizeSize.getH(45),
    dimen_48: NormalizeSize.getH(48),
    dimen_50: NormalizeSize.getH(50),
    dimen_55: NormalizeSize.getH(55),
    dimen_56: NormalizeSize.getH(56),
    dimen_60: NormalizeSize.getH(60),
    dimen_64: NormalizeSize.getH(64),
    dimen_65: NormalizeSize.getH(65),
    dimen_70: NormalizeSize.getH(70),
    dimen_72: NormalizeSize.getH(72),
    dimen_75: NormalizeSize.getH(75),
    dimen_80: NormalizeSize.getH(80),
    dimen_90: NormalizeSize.getH(90),
    dimen_92: NormalizeSize.getH(92),
    dimen_96: NormalizeSize.getH(96),
    dimen_100: NormalizeSize.getH(100),
    dimen_120: NormalizeSize.getH(120),
    dimen_130: NormalizeSize.getH(130),
    dimen_140: NormalizeSize.getH(140),
    dimen_150: NormalizeSize.getH(150),
    dimen_160: NormalizeSize.getH(160),
    dimen_180: NormalizeSize.getH(180),
    dimen_190: NormalizeSize.getH(190),
    dimen_200: NormalizeSize.getH(200),
    dimen_220: NormalizeSize.getH(220),
    dimen_230: NormalizeSize.getH(230),
    dimen_240: NormalizeSize.getH(240),
    dimen_250: NormalizeSize.getH(250),
    dimen_300: NormalizeSize.getH(300),
    getH: (size) => NormalizeSize.getH(size),
    getHPercent: (percent) => NormalizeSize.getHPercent(percent),
    getW: (size) => NormalizeSize.getW(size),
    getWPercent: (percent) => NormalizeSize.getWPercent(percent),
    getFontSize: (size) => NormalizeSize.getFontSize(size),

    icons: {

        tiny: 15,
        small: 20,
        medium: 30,
        large: 45,
        xl: 50
    },

    images: {

        small: 20,
        medium: 40,
        large: 60,
        logo: 200
    },

    newView: {
        upperview: Dimensions.get('window').height / 4 + 10,
        curveview: Dimensions.get('window').height * 3 / 4,
        curvetop: Dimensions.get('window').height / 4 - 30
    }

}

export default metrics