export const slides = [
  {
    key: 1,
    title: 'Scan & Pay',
    text: 'Go cashless in our partner merchant stores by scanning CaribPay QR code to make payment.',
    image: require('./Images/slide1.jpg'),
    backgroundColor: '#59b2ab',
    skip: true
  },
  {
    key: 2,
    title: 'Attractive Deals',
    text: 'More saving on every deal you buy on CaribPay - You have thousands to choose from.',
    image: require('./Images/slide2.png'),
    backgroundColor: '#febe29',
    skip: true
  },
  {
    key: 3,
    title: 'Amenities',
    text: 'Your Favourite brands are all here resturants,spas,gym,hotels and more.',
    image: require('./Images/slide3.png'),
    backgroundColor: '#22bcb5',
    skip: true
  },
  {
    key: 4,
    title: 'Various Services',
    text: 'Consumer Loan & Insurance Payment Pay Bills and many other services',
    image: require('./Images/slide4.png'),
    backgroundColor: '#22bcb5',
    skip: true
  },
  {
    key: 5,
    title: 'Mobile Airtime - Topup',
    text: 'The best way to connected , Deliver mobile topup worldwide',
    image: require('./Images/slide5.png'),
    backgroundColor: '#22bcb5',
    skip: false
  }
];


export const Facilities_data = [
  {
    id: 1,
    image: require('./Images/addfund.png'),
    name: "Add Fund",
  },
  {
    id: 2,
    image: require('./Images/topup.png'),
    name: "Mobile Topup",
  },
  {
    id: 3,
    image: require('./Images/bill.png'),
    name: "Bills Pay",
  },
  {
    id: 4,
    image: require('./Images/cards.png'),
    name: "Card",
  },
  {
    id: 5,
    image: require('./Images/remittance.png'),
    name: "Remittance",
  },
  {
    id: 6,
    image: require('./Images/mall.png'),
    name: "Carib Mall",
  }
]
