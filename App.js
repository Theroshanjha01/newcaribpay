import React from 'react';
import AppContainer from './App/routes.js';

export default class App extends React.Component {
  render() {
    return <AppContainer/>;
  }
}
